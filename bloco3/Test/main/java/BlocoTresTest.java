package main.java;

import org.junit.jupiter.api.Test;

import static main.java.BlocoTres.*;
import static org.junit.jupiter.api.Assertions.*;

class BlocoTresTest {

    @Test
    void ExOneGetResResultOne() {
        // arrange
        int res = 1;
        int num = 1;
        int expected = 1;

        // act
        int result = ExOneGetRes(res, num);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExOneGetResResultZero() {
        // arrange
        int res = 0;
        int num = 1;
        int expected = 0;

        // act
        int result = ExOneGetRes(res, num);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExOneGetResResultOneWithNegativeInput() {
        // arrange
        int res = 1;
        int num = -1;
        int expected = 1;

        // act
        int result = ExOneGetRes(res, num);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExOneGetResResultTwentyFour() {
        // arrange
        int res = 1;
        int num = 4;
        int expected = 24;

        // act
        int result = ExOneGetRes(res, num);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFourAGetNumberMultiplesResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 2;
        int MULTIPLE = 3;
        int expected = 0;

        // act
        int result = ExFourAGetNumberMultiples(minimum, maximum, MULTIPLE);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourAGetNumberMultiplesResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 3;
        int MULTIPLE = 3;
        int expected = 1;

        // act
        int result = ExFourAGetNumberMultiples(minimum, maximum, MULTIPLE);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourAGetNumberMultiplesResultTenWithNegativeInput() {
        // arrange
        int minimum = -15;
        int maximum = 15;
        int MULTIPLE = 3;
        int expected = 10;

        // act
        int result = ExFourAGetNumberMultiples(minimum, maximum, MULTIPLE);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFourBGetNumberMultiplesResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int multiple = 4;
        int expected = 0;

        // act
        int result = ExFourBGetNumberMultiples(minimum, maximum, multiple);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourBGetNumberMultiplesResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int multiple = 1;
        int expected = 1;

        // act
        int result = ExFourBGetNumberMultiples(minimum, maximum, multiple);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourBGetNumberMultiplesResultTenWithNegativeInput() {
        // arrange
        int minimum = -2;
        int maximum = 8;
        int multiple = 1;
        int expected = 10;

        // act
        int result = ExFourBGetNumberMultiples(minimum, maximum, multiple);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFourCGetNumberMultiplesResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int MULTIPLE_ONE = 3;
        int MULTIPLE_TWO = 5;
        int expected = 0;

        // act
        int result = ExFourCGetNumberMultiples(minimum, maximum, MULTIPLE_ONE, MULTIPLE_TWO);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourCGetNumberMultiplesResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 4;
        int MULTIPLE_ONE = 3;
        int MULTIPLE_TWO = 5;
        int expected = 1;

        // act
        int result = ExFourCGetNumberMultiples(minimum, maximum, MULTIPLE_ONE, MULTIPLE_TWO);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourCGetNumberMultiplesResultTenWithNegativeInput() {
        // arrange
        int minimum = -10;
        int maximum = 10;
        int MULTIPLE_ONE = 3;
        int MULTIPLE_TWO = 5;
        int expected = 10;

        // act
        int result = ExFourCGetNumberMultiples(minimum, maximum, MULTIPLE_ONE, MULTIPLE_TWO);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFourDGetNumberMultiplesResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int multipleOne = 2;
        int multipleTwo = 3;
        int expected = 0;

        // act
        int result = ExFourDGetNumberMultiples(minimum, maximum, multipleOne, multipleTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourDGetNumberMultiplesResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 3;
        int multipleOne = 2;
        int multipleTwo = 6;
        int expected = 1;

        // act
        int result = ExFourDGetNumberMultiples(minimum, maximum, multipleOne, multipleTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourDGetNumberMultiplesResultTenWithNegativeInput() {
        // arrange
        int minimum = -8;
        int maximum = 8;
        int multipleOne = 2;
        int multipleTwo = 3;
        int expected = 10;

        // act
        int result = ExFourDGetNumberMultiples(minimum, maximum, multipleOne, multipleTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFourEGetSumMultiplesResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int multipleOne = 2;
        int multipleTwo = 3;
        int expected = 0;

        // act
        int result = ExFourEGetSumMultiples(minimum, maximum, multipleOne, multipleTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourEGetSumMultiplesResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int multipleOne = 1;
        int multipleTwo = 6;
        int expected = 1;

        // act
        int result = ExFourEGetSumMultiples(minimum, maximum, multipleOne, multipleTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourEGetSumMultiplesResultTenWithNegativeInput() {
        // arrange
        int minimum = -3;
        int maximum = 6;
        int multipleOne = 2;
        int multipleTwo = 3;
        int expected = 10;

        // act
        int result = ExFourEGetSumMultiples(minimum, maximum, multipleOne, multipleTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFourEGetSumMultiplesResultZeroWithNegativeInput() {
        // arrange
        int minimum = -3;
        int maximum = 3;
        int multipleOne = 2;
        int multipleTwo = 3;
        int expected = 0;

        // act
        int result = ExFourEGetSumMultiples(minimum, maximum, multipleOne, multipleTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveAGetSumEvensResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int expected = 0;

        // act
        int result = ExFiveAGetSumEvens(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveAGetSumEvensResultTwo() {
        // arrange
        int minimum = 0;
        int maximum = 2;
        int expected = 2;

        // act
        int result = ExFiveAGetSumEvens(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveAGetSumEvensResultSixWithNegativeInput() {
        // arrange
        int minimum = -4;
        int maximum = 6;
        int expected = 6;

        // act
        int result = ExFiveAGetSumEvens(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveBGetNumberEvensResultZero() {
        // arrange
        int minimum = 1;
        int maximum = 1;
        int expected = 0;

        // act
        int result = ExFiveBGetNumberEvens(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveBGetNumberEvensResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int expected = 1;

        // act
        int result = ExFiveBGetNumberEvens(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveBGetNumberEvensResultTenWithNegativeInput() {
        // arrange
        int minimum = -8;
        int maximum = 10;
        int expected = 10;

        // act
        int result = ExFiveBGetNumberEvens(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveCGetSumOddsResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 0;
        int expected = 0;

        // act
        int result = ExFiveCGetSumOdds(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveCGetSumOddsResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 2;
        int expected = 1;

        // act
        int result = ExFiveCGetSumOdds(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveCGetSumOddsResultTwelveWithNegativeInput() {
        // arrange
        int minimum = -3;
        int maximum = 7;
        int expected = 12;

        // act
        int result = ExFiveCGetSumOdds(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveDGetNumberOddsResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 0;
        int expected = 0;

        // act
        int result = ExFiveDGetNumberOdds(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveDGetNumberOddsResultOne() {
        // arrange
        int minimum = 0;
        int maximum = 1;
        int expected = 1;

        // act
        int result = ExFiveDGetNumberOdds(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveDGetNumberOddsResultTenWithNegativeInput() {
        // arrange
        int minimum = -9;
        int maximum = 9;
        int expected = 10;

        // act
        int result = ExFiveDGetNumberOdds(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveEGetSumMultiplesResultZero() {
        // arrange
        int limitOne = 0;
        int limitTwo = 1;
        int number = 3;
        int expected = 0;

        // act
        int result = ExFiveEGetSumMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveEGetSumMultiplesResultOne() {
        // arrange
        int limitOne = 0;
        int limitTwo = 1;
        int number = 1;
        int expected = 1;

        // act
        int result = ExFiveEGetSumMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveEGetSumMultiplesResultTenWithNegativeInputWithLimitsReversed() {
        // arrange
        int limitOne = 10;
        int limitTwo = -8;
        int number = 2;
        int expected = 10;

        // act
        int result = ExFiveEGetSumMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveFGetProductMultiplesResultZero() {
        // arrange
        int limitOne = 0;
        int limitTwo = 1;
        int number = 3;
        int expected = 0;

        // act
        int result = ExFiveFGetProductMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveFGetProductMultiplesResultOne() {
        // arrange
        int limitOne = 0;
        int limitTwo = 1;
        int number = 1;
        int expected = 1;

        // act
        int result = ExFiveFGetProductMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveFGetProductMultiplesResultSixtyFourWithNegativeInput() {
        // arrange
        int limitOne = -4;
        int limitTwo = 4;
        int number = 2;
        int expected = 64;

        // act
        int result = ExFiveFGetProductMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveGGetAverageMultiplesResultZero() {
        // arrange
        int limitOne = -3;
        int limitTwo = 3;
        int number = 3;
        double expected = 0;

        // act
        double result = ExFiveGGetAverageMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveGGetAverageMultiplesResultOne() {
        // arrange
        int limitOne = 0;
        int limitTwo = 1;
        int number = 1;
        double expected = 1;

        // act
        double result = ExFiveGGetAverageMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveGGetAverageMultiplesResultSixPointSevenFiveWithNegativeInput() {
        // arrange
        int limitOne = -3;
        int limitTwo = 14;
        int number = 2;
        double expected = 6.75;

        // act
        double result = ExFiveGGetAverageMultiples(limitOne, limitTwo, number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExFiveHGetAverageMultiplesResultZeroWithNegativeInput() {
        // arrange
        int limitOne = -3;
        int limitTwo = 3;
        int numberOne = 3;
        int numberTwo = 5;
        double expected = 0;

        // act
        double result = ExFiveHGetAverageMultiples(limitOne, limitTwo, numberOne, numberTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveHGetAverageMultiplesResultOne() {
        // arrange
        int limitOne = 0;
        int limitTwo = 1;
        int numberOne = 1;
        int numberTwo = 2;
        double expected = 1;

        // act
        double result = ExFiveHGetAverageMultiples(limitOne, limitTwo, numberOne, numberTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExFiveHGetAverageMultiplesResultFourPointSixTwoFiveWithNegativeInput() {
        // arrange
        int limitOne = -5;
        int limitTwo = 12;
        int numberOne = 5;
        int numberTwo = 3;
        double expected = 4.625;

        // act
        double result = ExFiveHGetAverageMultiples(limitOne, limitTwo, numberOne, numberTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixAGetDigitsResultOne() {
        // arrange
        long number = 1;
        int expected = 1;

        // act
        int result = ExSixAGetDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixAGetDigitsResultThree() {
        // arrange
        long number = 123;
        int expected = 3;

        // act
        int result = ExSixAGetDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixAGetDigitsResultTenWithNegativeInput() {
        // arrange
        long number = -1234567892;
        int expected = 10;

        // act
        int result = ExSixAGetDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixBGetNumberEvenDigitsResultZero() {
        // arrange
        long number = 13579;
        int expected = 0;

        // act
        int result = ExSixBGetNumberEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixBGetNumberEvenDigitsResultOne() {
        // arrange
        long number = 213579;
        int expected = 1;

        // act
        int result = ExSixBGetNumberEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixBGetNumberEvenDigitsResultTwo() {
        // arrange
        long number = 2135792;
        int expected = 2;

        // act
        int result = ExSixBGetNumberEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixBGetNumberEvenDigitsResultNineWithNegativeInput() {
        // arrange
        long number = -224682482;
        int expected = 9;

        // act
        int result = ExSixBGetNumberEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixCGetNumberOddDigitsResultZero() {
        // arrange
        long number = 24682;
        int expected = 0;

        // act
        int result = ExSixCGetNumberOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixCGetNumberOddDigitsResultOne() {
        // arrange
        long number = 124682;
        int expected = 1;

        // act
        int result = ExSixCGetNumberOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixCGetNumberOddDigitsResultNineWithNegativeInput() {
        // arrange
        long number = -135797531;
        int expected = 9;

        // act
        int result = ExSixCGetNumberOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixDGetSumDigitsResultZero() {
        // arrange
        long number = 0;
        double expected = 0;

        // act
        double result = ExSixDGetSumDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixDGetSumDigitsResultTwo() {
        // arrange
        long number = 11;
        double expected = 2;

        // act
        double result = ExSixDGetSumDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixDGetSumDigitsResultNegativeFourteenWithNegativeInput() {
        // arrange
        long number = -123242;
        double expected = -14;

        // act
        double result = ExSixDGetSumDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixDGetSumDigitsResultFourteen() {
        // arrange
        long number = 123242;
        double expected = 14;

        // act
        double result = ExSixDGetSumDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixEGetSumEvenDigitsResultZero() {
        // arrange
        long number = 1135;
        double expected = 0;

        // act
        double result = ExSixEGetSumEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixEGetSumEvenDigitsResultTwo() {
        // arrange
        long number = 10215;
        double expected = 2;

        // act
        double result = ExSixEGetSumEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixEGetSumEvenDigitsResultNegativeFourteenWithNegativeInput() {
        // arrange
        long number = -4578372;
        double expected = -14;

        // act
        double result = ExSixEGetSumEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixEGetSumEvenDigitsResultFourteen() {
        // arrange
        long number = 8435732;
        double expected = 14;

        // act
        double result = ExSixEGetSumEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixFGetSumOddDigitsResultZero() {
        // arrange
        long number = 22468;
        double expected = 0;

        // act
        double result = ExSixFGetSumOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixFGetSumOddDigitsResultTwo() {
        // arrange
        long number = 124281;
        double expected = 2;

        // act
        double result = ExSixFGetSumOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixFGetSumOddDigitsResultNegativeFourteenWithNegativeInput() {
        // arrange
        long number = -1257281;
        double expected = -14;

        // act
        double result = ExSixFGetSumOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixFGetSumOddDigitsResultFourteen() {
        // arrange
        long number = 1257281;
        double expected = 14;

        // act
        double result = ExSixFGetSumOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixGGetAverageDigitsResultOne() {
        // arrange
        long number = 20;
        double expected = 1;

        // act
        double result = ExSixGGetAverageDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixGGetAverageDigitsResultTwo() {
        // arrange
        long number = 113313;
        double expected = 2;

        // act
        double result = ExSixGGetAverageDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixGGetAverageDigitsResultNegativeFiveWithNegativeInput() {
        // arrange
        long number = -2837456;
        double expected = -5;

        // act
        double result = ExSixGGetAverageDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixGGetAverageDigitsResultFive() {
        // arrange
        long number = 2837465;
        double expected = 5;

        // act
        double result = ExSixGGetAverageDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixHGetAverageEvenDigitsResultOne() {
        // arrange
        long number = 20;
        double expected = 1;

        // act
        double result = ExSixHGetAverageEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixHGetAverageEvenDigitsResultTwo() {
        // arrange
        long number = 11331304;
        double expected = 2;

        // act
        double result = ExSixHGetAverageEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixHGetAverageEvenDigitsResultNegativeFiveWithNegativeInput() {
        // arrange
        long number = -2837456;
        double expected = -5;

        // act
        double result = ExSixHGetAverageEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixHGetAverageEvenDigitsResultFive() {
        // arrange
        long number = 2837465;
        double expected = 5;

        // act
        double result = ExSixHGetAverageEvenDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixIGetAverageOddDigitsResultOne() {
        // arrange
        long number = 201;
        double expected = 1;

        // act
        double result = ExSixIGetAverageOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixIGetAverageOddDigitsResultTwo() {
        // arrange
        long number = 11331304;
        double expected = 2;

        // act
        double result = ExSixIGetAverageOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixIGetAverageOddDigitsResultNegativeFiveWithNegativeInput() {
        // arrange
        long number = -2837456;
        double expected = -5;

        // act
        double result = ExSixIGetAverageOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixIGetAverageOddDigitsResultFive() {
        // arrange
        long number = 2837465;
        double expected = 5;

        // act
        double result = ExSixIGetAverageOddDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSixJReverseDigitsResultFourThreeTwoOne() {
        // arrange
        long number = 1234;
        double expected = 4321;

        // act
        double result = ExSixJReverseDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixJReverseDigitsResultTwoOne() {
        // arrange
        long number = 12;
        double expected = 21;

        // act
        double result = ExSixJReverseDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSixJReverseDigitsResultOneTwoThree() {
        // arrange
        long number = 321;
        double expected = 123;

        // act
        double result = ExSixJReverseDigits(number);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSevenACheckIfNumeralPalindromeResultTrue() {
        // arrange
        int number = 101;
        boolean expected = true;

        // act
        boolean result = ExSevenACheckIfNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void ExSevenACheckIfNumeralPalindromeResultFalse() {
        // arrange
        int number = 100;
        boolean expected = false;

        // act
        boolean result = ExSevenACheckIfNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }


    @Test
    void ExSevenBCheckIfArmstrongNumberResultFalse() {
        // arrange
        int number = 100;
        boolean expected = false;

        // act
        boolean result = ExSevenBCheckIfArmstrongNumber(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void ExSevenBCheckIfArmstrongNumberResultTrue() {
        // arrange
        int number = 153;
        boolean expected = true;

        // act
        boolean result = ExSevenBCheckIfArmstrongNumber(number);

        // assert
        assertEquals(expected, result);
    }


    @Test
    void ExSevenCCheckIntervalNumbersResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 2;
        int expected = 0;

        // act
        int result = ExSevenCCheckIntervalNumbersFirstPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenCCheckIntervalNumbersResultOne() {
        // arrange
        int minimum = 1;
        int maximum = 2;
        int expected = 1;

        // act
        int result = ExSevenCCheckIntervalNumbersFirstPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenCCheckIntervalNumbersResultEleven() {
        // arrange
        int minimum = 10;
        int maximum = 20;
        int expected = 11;

        // act
        int result = ExSevenCCheckIntervalNumbersFirstPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSevenDCheckIntervalNumbersLastPalindromeResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 0;
        int expected = 0;

        // act
        int result = ExSevenDCheckIntervalNumbersLastPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenDCheckIntervalNumbersLastPalindromeResultEleven() {
        // arrange
        int minimum = 0;
        int maximum = 12;
        int expected = 11;

        // act
        int result = ExSevenDCheckIntervalNumbersLastPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSevenECheckIntervalNumbersCountPalindromeResultZero() {
        // arrange
        int minimum = 12;
        int maximum = 13;
        int expected = 0;

        // act
        int result = ExSevenECheckIntervalNumbersCountPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenECheckIntervalNumbersCountPalindromeResultOne() {
        // arrange
        int minimum = 10;
        int maximum = 12;
        int expected = 1;

        // act
        int result = ExSevenECheckIntervalNumbersCountPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenECheckIntervalNumbersCountPalindromeResultTen() {
        // arrange
        int minimum = 10;
        int maximum = 102;
        int expected = 10;

        // act
        int result = ExSevenECheckIntervalNumbersCountPalindrome(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSevenFCheckIntervalNumbersFirstArmstrongResultZero() {
        // arrange
        int minimum = 0;
        int maximum = 100;
        int expected = 0;

        // act
        int result = ExSevenFCheckIntervalNumbersFirstArmstrong(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenFCheckIntervalNumbersFirstArmstrongResultOne() {
        // arrange
        int minimum = 1;
        int maximum = 100;
        int expected = 1;

        // act
        int result = ExSevenFCheckIntervalNumbersFirstArmstrong(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenFCheckIntervalNumbersFirstArmstrongResultOneFiftyThree() {
        // arrange
        int minimum = 3;
        int maximum = 155;
        int expected = 153;

        // act
        int result = ExSevenFCheckIntervalNumbersFirstArmstrong(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }


    @Test
    void ExSevenGCheckIntervalNumbersCountArmstrongResultZero() {
        // arrange
        int minimum = 3;
        int maximum = 4;
        int expected = 0;

        // act
        int result = ExSevenGCheckIntervalNumbersCountArmstrong(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenGCheckIntervalNumbersCountArmstrongResultOne() {
        // arrange
        int minimum = 1;
        int maximum = 4;
        int expected = 1;

        // act
        int result = ExSevenGCheckIntervalNumbersCountArmstrong(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExSevenGCheckIntervalNumbersCountArmstrongResultThree() {
        // arrange
        int minimum = 0;
        int maximum = 155;
        int expected = 3;

        // act
        int result = ExSevenGCheckIntervalNumbersCountArmstrong(minimum, maximum);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void GetWeightedSumResultOne() {
        // arrange
        int number = 1;
        int expected = 1;

        // act
        int result = GetWeightedSum(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void GetWeightedSumResultZero() {
        // arrange
        int number = 0;
        int expected = 0;

        // act
        int result = GetWeightedSum(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void GetWeightedSumResultOneSixtyFive() {
        // arrange
        int number = 123456789;
        int expected = 165;

        // act
        int result = GetWeightedSum(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void GetWeightedSumResultOneEleven() {
        // arrange
        int number = 143060520;
        int expected = 111;

        // act
        int result = GetWeightedSum(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void GetWeightedSumResultNegativeOneSixtyFive() {
        // arrange
        int number = -123456789;
        int expected = -165;

        // act
        int result = GetWeightedSum(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void IsValidIdNumberResultFalse() {
        // arrange
        int weightedSum = 1;

        // act
        boolean result = IsValidIdNumber(weightedSum);

        // assert
        assertFalse(result);
    }

    @Test
    void IsValidIdNumberResultTrue() {
        // arrange
        int weightedSum = 165;

        // act
        boolean result = IsValidIdNumber(weightedSum);

        // assert
        assertTrue(result);
    }


    @Test
    void ExNineteenResultOneOneTwoTwo() {
        // arrange
        int number = 2121;
        int expected = 1122;

        // act
        int result = ExNineteen(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExNineteenResultOneTwo() {
        // arrange
        int number = 21;
        int expected = 12;

        // act
        int result = ExNineteen(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ExNineteenResultFiveThreeSevenNineFour() {
        // arrange
        int number = 97435;
        int expected = 53794;

        // act
        int result = ExNineteen(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void GetOddsOnlyResultOne() {
        // arrange
        int number = 12468;
        int expected = 1;

        // act
        int result = GetOddsOnly(number);

        // assert
        assertEquals(expected, result ,0.01);
    }

    @Test
    void GetOddsOnlyResultZero() {
        // arrange
        int number = 2468;
        int expected = 0;

        // act
        int result = GetOddsOnly(number);

        // assert
        assertEquals(expected, result ,0.01);
    }

    @Test
    void GetOddsOnlyResultSevenFive() {
        // arrange
        int number = 524687;
        int expected = 75;

        // act
        int result = GetOddsOnly(number);

        // assert
        assertEquals(expected, result ,0.01);
    }

    @Test
    void isEvenResultTrue() {
        // arrange
        int number = 2;

        // act
        boolean result = isEven(number);

        // assert
        assertTrue(result);
    }

    @Test
    void isEvenResultFalse() {
        // arrange
        int number = 2;

        // act
        boolean result = isEven(number);

        // assert
        assertTrue(result);
    }


    @Test
    void GetSumDivisorsResultOne() {
        // arrange
        int number = 2;
        int expected = 1;

        // act
        int result = GetSumDivisors(number);

        // assert
        assertEquals(expected, result, 0.01);
    }
}