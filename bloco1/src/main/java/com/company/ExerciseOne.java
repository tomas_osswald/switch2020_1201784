package main.java.com.company;

import java.util.Scanner;

public class ExerciseOne {

    public static void main(String[] args) {

        // Cálculo da percentagem de rapazes e raparigas numa turma
        // introdução das variáveis

        double rapazes, raparigas, total, percentagemRapazes, percentagemRaparigas;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de rapazes");
        rapazes = ler.nextDouble();
        System.out.println("Introduza o número de raparigas");
        raparigas = ler.nextDouble();

        // cálculo

        total = rapazes + raparigas;
        percentagemRapazes = getPercentagem(rapazes, total);
        percentagemRaparigas = getPercentagem(raparigas, total);

        // apresentação do resultado

        System.out.println("A percentagem de rapazes é de " + percentagemRapazes + ", a percentagem de raparigas é de " + percentagemRaparigas);

    }

    public static double getPercentagem(double parcial, double total) {
        return (parcial /total) * 100;
    }

}
