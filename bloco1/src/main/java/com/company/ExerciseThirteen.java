package main.java.com.company;

import java.util.Scanner;

public class ExerciseThirteen {

    public static void main(String[] args) {

        // Calcular minutos passados desde hora zero
        // introducao de variaveis

        int horas, minutos, minutosDecorridos;

        // leitura de variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de horas decorridas");
        horas = ler.nextInt();
        System.out.println("Introduza o número de minutos decorridos");
        minutos = ler.nextInt();

        // processamento

        minutosDecorridos = getMinutosDecorridos(horas, minutos);

        // apresentacao do resultado

        System.out.println("Desde o início, decorreram " + minutosDecorridos + " minutos");

    }

    public static int getMinutosDecorridos(int horas, int minutos) {
        return (horas * 60) + minutos;
    }

}
