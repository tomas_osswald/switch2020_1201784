package main.java.com.company;

public class TESTES {

    public static void main(String[] args) {
        zero();
        //one();
        two();

    }

    public static void zero() {

        int hora;

        for (hora=0; hora<=5; hora++)
            System.out.println(hora);

    }

    public static void one() {

        int hora = 6;

        while (hora<=5) {
            System.out.println(hora);
            hora++;
        }
    }

    public static void two() {

        int hora = 6;

        do {
            System.out.println(hora);
            hora++;
        } while (hora<=5);
    }
}
