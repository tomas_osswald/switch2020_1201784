package main.java.com.company;

import java.util.Scanner;

public class ExerciseSeven {

    public static void main(String[] args) {

        // Cálculo da distância percorrida pelo Zé em metros
        // introdução das variáveis

        int horasManel, minutosManel, segundosManel, horasZe, minutosZe, segundosZe;
        double distanciaZe, distanciaManel;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a distância em metros percorrida pelo Manel");
        distanciaManel = ler.nextDouble();
        System.out.println("Introduza as horas do tempo do Manel");
        horasManel = ler.nextInt();
        System.out.println("Introduza os minutos do tempo do Manel");
        minutosManel = ler.nextInt();
        System.out.println("Introduza os segundos do tempo do Manel");
        segundosManel = ler.nextInt();
        System.out.println("Introduza as horas do tempo do Zé");
        horasZe = ler.nextInt();
        System.out.println("Introduza os minutos do tempo do Zé");
        minutosZe = ler.nextInt();
        System.out.println("Introduza os segundos do tempo do Zé");
        segundosZe = ler.nextInt();

        // cálculo de acordo com a fórmula velocidade média é a razão entre distância e intervalo de tempo

        distanciaZe = getDistanciaZe(horasManel, minutosManel, segundosManel, horasZe, minutosZe, segundosZe, distanciaManel);

        // apresentação do resultado

        System.out.println("O Zé percorreu uma distância de " + String.format("%.2f", distanciaZe) + " metros");

    }

    public static double getDistanciaZe(int horasManel, int minutosManel, int segundosManel, int horasZe, int minutosZe, int segundosZe, double distanciaManel) {
        return (distanciaManel / (3600 * horasManel + 60 * minutosManel + segundosManel)) * (3600 * horasZe + 60 * minutosZe + segundosZe);
    }


}
