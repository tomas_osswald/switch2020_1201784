package main.java.com.company;

import java.util.Scanner;

public class ExerciseNine {

    public static void main(String[] args) {

        // Calcular o perimetro de um rectangulo
        // introducao das variaveis

        double A, B, perimetro;

        // leitura das variaveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor de A");
        A = ler.nextDouble();
        System.out.println("Introduza o valor de B");
        B = ler.nextDouble();

        // processamento

        perimetro = getPerimetro(A, B);

        // apresentacao do resultado

        System.out.println("O perímetro do rectângulo é " + perimetro);

    }

    public static double getPerimetro(double a, double b) {
        return (2 * a) + (2 * b);
    }

}
