package main.java.com.company;

import java.util.Scanner;

public class ExerciseEleven {

    public static void main(String[] args) {

        // calcular x^2 - 3x + 1
        // introducao das variaveis

        double variavel, resultado;

        // leitura das variaveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor da variável");
        variavel = ler.nextDouble();

        // processamento

        resultado = getResultado(variavel);

        // apresentacao do resultado

        System.out.println("O resultado é " + resultado);

    }

    public static double getResultado(double variavel) {
        return Math.pow(variavel, 2) - (3 * variavel) + 1;
    }

}
