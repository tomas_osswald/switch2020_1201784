package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseNineTest {

    @Test
    public void getPerimetroTestOne() {
        // arrange
        double A = 2;
        double B = 4;
        double expected = 12;

        // act
        double result = ExerciseNine.getPerimetro(A, B);

        // assert
        assertEquals (expected, result, 0.01);
    }

    @Test
    public void getPerimetroTestTwo() {
        // arrange
        double A = 0;
        double B = 0;
        double expected = 0;

        // act
        double result = ExerciseNine.getPerimetro(A, B);

        // assert
        assertEquals (expected, result, 0.01);
    }

    @Test
    public void getPerimetroTestThree() {
        // arrange
        double A = 2.8;
        double B = 31.3;
        double expected = 68.2;

        // act
        double result = ExerciseNine.getPerimetro(A, B);

        // assert
        assertEquals (expected, result, 0.01);
    }
}