package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseFiveTest {

    @Test
    public void getAlturaTestOne() {

        double expected = 490, tempo = 10;
        double result = ExerciseFive.getAltura(tempo);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getAlturaTestTwo() {

        double expected = 23.72, tempo = 2.2;
        double result = ExerciseFive.getAltura(tempo);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getAlturaTestThree() {

        double expected = 0, tempo = 0;
        double result = ExerciseFive.getAltura(tempo);
        assertEquals(expected, result, 0.01);
    }
}