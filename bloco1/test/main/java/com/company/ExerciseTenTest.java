package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseTenTest {

    @Test
    public void getHipotenusaTestOne() {
        // arrange
        double c1 = 3;
        double c2 = 4;
        double expected = 5;

        // act
        double result = ExerciseTen.getHipotenusa(c1, c2);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getHipotenusaTestTwo() {
        // arrange
        double c1 = 0;
        double c2 = 4;
        double expected = 4;

        // act
        double result = ExerciseTen.getHipotenusa(c1, c2);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getHipotenusaTestThree() {
        // arrange
        double c1 = 32.5;
        double c2 = 23.6;
        double expected = 40.16;

        // act
        double result = ExerciseTen.getHipotenusa(c1, c2);

        // assert
        assertEquals(result, expected, 0.01);
    }
}