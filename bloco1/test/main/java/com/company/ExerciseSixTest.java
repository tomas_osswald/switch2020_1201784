package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseSixTest {

    @Test
    public void getAlturaEdificioTestOne() {
        //arrange

        double alturaPessoa = 2;
        double sombraPessoa = 4;
        double sombraEdificio = 40;
        double expected = 20;

        //act
        double result = ExerciseSix.getAlturaEdificio(sombraPessoa, sombraEdificio, alturaPessoa);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void getAlturaEdificioTestTwo() {
        //arrange

        double alturaPessoa = 1.83;
        double sombraPessoa = 3;
        double sombraEdificio = 9;
        double expected = 5.49;

        //act
        double result = ExerciseSix.getAlturaEdificio(sombraPessoa, sombraEdificio, alturaPessoa);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void getAlturaEdificioTestThree() {
        //arrange

        double alturaPessoa = 0;
        double sombraPessoa = 2;
        double sombraEdificio = 500;
        double expected = 0;

        //act
        double result = ExerciseSix.getAlturaEdificio(sombraPessoa, sombraEdificio, alturaPessoa);

        //assert
        assertEquals(expected, result, 0.01);

    }
}