package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseElevenTest {

    @Test
    public void getResultadoTestOne() {
        // arrange
        double variavel = 5;
        double expected = 11;

        // act
        double result = ExerciseEleven.getResultado(variavel);

        // assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void getResultadoTestTwo() {
        // arrange
        double variavel = 0;
        double expected = 1;

        // act
        double result = ExerciseEleven.getResultado(variavel);

        // assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void getResultadoTestThree() {
        // arrange
        double variavel = 8.2;
        double expected = 43.64;

        // act
        double result = ExerciseEleven.getResultado(variavel);

        // assert
        assertEquals(expected, result, 0.01);

    }
}