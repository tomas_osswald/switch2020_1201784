package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseThreeTest {

    @Test
    public void getVolumeTestOne() {

        double expected = 0;
        double result = ExerciseThree.getVolume(0, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getVolumeTestTwo() {

        double expected = 37699.11;
        double result = ExerciseThree.getVolume(2, 3);
        assertEquals(expected, result, 0.01);
    }

}