package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseSevenTest {

    @Test
    public void getDistanciaZeTestOne() {
        //arrange
        int horasManel = 4, minutosManel= 2, segundosManel = 10;
        int horasZe = 1, minutosZe = 5, segundosZe = 0;
        double distanciaManel = 42195;
        double expected = 11325.57;

        //act
        double result = ExerciseSeven.getDistanciaZe(horasManel,minutosManel,segundosManel,horasZe,minutosZe,segundosZe,distanciaManel);

        //assert
        assertEquals(result, expected, 0.01);

    }

    @Test
    public void getDistanciaZeTestTwo() {
        //arrange
        int horasManel = 2, minutosManel= 2, segundosManel = 2;
        int horasZe = 3, minutosZe = 4, segundosZe = 5;
        double distanciaManel = 10000;
        double expected = 15084.68;

        //act
        double result = ExerciseSeven.getDistanciaZe(horasManel,minutosManel,segundosManel,horasZe,minutosZe,segundosZe,distanciaManel);

        //assert
        assertEquals(result, expected, 0.01);

    }

    @Test
    public void getDistanciaZeTestThree() {
        //arrange
        int horasManel = 4, minutosManel= 2, segundosManel = 10;
        int horasZe = 0, minutosZe = 0, segundosZe = 0;
        double distanciaManel = 4227;
        double expected = 0;

        //act
        double result = ExerciseSeven.getDistanciaZe(horasManel,minutosManel,segundosManel,horasZe,minutosZe,segundosZe,distanciaManel);

        //assert
        assertEquals(result, expected, 0.01);

    }
}