import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioNoveTeste {

    @Test
    public void isInvalidoTesteUm() {
        // arrange
        int numero = 100;
        boolean expected = false;

        // act
        boolean result = ExercicioNove.isInvalido(numero);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isInvalidoTesteDois() {
        // arrange
        int numero = 99;
        boolean expected = true;

        // act
        boolean result = ExercicioNove.isInvalido(numero);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isInvalidoTesteTres() {
        // arrange
        int numero = 1000;
        boolean expected = true;

        // act
        boolean result = ExercicioNove.isInvalido(numero);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getDigito3Teste() {
        // arrange
        int numero = 456;
        double expected = 6;

        // act
        double result = ExercicioNove.getDigito3(numero);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDigito2Teste() {
        // arrange
        int numero = 456;
        double expected = 5;

        // act
        double result = ExercicioNove.getDigito2(numero);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDigito1Teste() {
        // arrange
        int numero = 456;
        double expected = 4;

        // act
        double result = ExercicioNove.getDigito1(numero);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void isCrescenteTesteUm() {
        // arrange
        int digito1 = 1;
        int digito2 = 2;
        int digito3 = 3;
        String expected = "A sequência de algarismos é crescente";

        // act
        String result = ExercicioNove.isCrescente(digito1, digito2, digito3);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isCrescenteTesteDois() {
        // arrange
        int digito1 = 2;
        int digito2 = 2;
        int digito3 = 3;
        String expected = "A sequência de algarismos não é crescente";

        // act
        String result = ExercicioNove.isCrescente(digito1, digito2, digito3);

        // assert
        assertEquals(expected, result);
    }
}