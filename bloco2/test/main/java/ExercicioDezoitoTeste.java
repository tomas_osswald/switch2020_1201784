import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioDezoitoTeste {

    @Test
    public void isValidoTesteUm() {
        // arrange
        int segundosDuracao = 0;
        boolean expected = false;

        // act
        boolean result = ExercicioDezoito.isValido(segundosDuracao);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isValidoTesteDois() {
        // arrange
        int segundosDuracao = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioDezoito.isValido(segundosDuracao);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isValidoTesteTres() {
        // arrange
        int segundosDuracao = -1;
        boolean expected = false;

        // act
        boolean result = ExercicioDezoito.isValido(segundosDuracao);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getTotalSegundosInicioTesteUm() {
        // arrange
        int horasInicio = 1;
        int minutosInicio = 1;
        int segundosInicio = 1;
        int expected = 3661;

        // act
        int result = ExercicioDezoito.getTotalSegundosInicio(horasInicio, minutosInicio, segundosInicio);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getTotalSegundosInicioTesteDois() {
        // arrange
        int horasInicio = 0;
        int minutosInicio = 0;
        int segundosInicio = 1;
        int expected = 1;

        // act
        int result = ExercicioDezoito.getTotalSegundosInicio(horasInicio, minutosInicio, segundosInicio);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getTotalSegundosInicioTesteTres() {
        // arrange
        int horasInicio = 0;
        int minutosInicio = 0;
        int segundosInicio = 0;
        int expected = 0;

        // act
        int result = ExercicioDezoito.getTotalSegundosInicio(horasInicio, minutosInicio, segundosInicio);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getHorasFimTesteUm() {
        // arrange
        int totalSegundosInicio = 1;
        int duracaoSegundos = 1;
        int expected = 0;

        // act
        int result = ExercicioDezoito.getHorasFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getHorasFimTesteDois() {
        // arrange
        int totalSegundosInicio = 3600;
        int duracaoSegundos = 10800;
        int expected = 4;

        // act
        int result = ExercicioDezoito.getHorasFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getHorasFimTesteTres() {
        // arrange
        int totalSegundosInicio = 82800;
        int duracaoSegundos = 7200;
        int expected = 1;

        // act
        int result = ExercicioDezoito.getHorasFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getMinutosFimTesteUm() {
        // arrange
        int totalSegundosInicio = 0;
        int duracaoSegundos = 60;
        int expected = 1;

        // act
        int result = ExercicioDezoito.getMinutosFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getMinutosFimTesteDois() {
        // arrange
        int totalSegundosInicio = 0;
        int duracaoSegundos = 3600;
        int expected = 0;

        // act
        int result = ExercicioDezoito.getMinutosFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getMinutosFimTesteTres() {
        // arrange
        int totalSegundosInicio = 0;
        int duracaoSegundos = 0;
        int expected = 0;

        // act
        int result = ExercicioDezoito.getMinutosFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSegundosFimTesteUm() {
        // arrange
        int totalSegundosInicio = 0;
        int duracaoSegundos = 0;
        int expected = 0;

        // act
        int result = ExercicioDezoito.getSegundosFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSegundosFimTesteDois() {
        // arrange
        int totalSegundosInicio = 0;
        int duracaoSegundos = 1;
        int expected = 1;

        // act
        int result = ExercicioDezoito.getSegundosFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSegundosFimTesteTres() {
        // arrange
        int totalSegundosInicio = 0;
        int duracaoSegundos = 3600;
        int expected = 0;

        // act
        int result = ExercicioDezoito.getSegundosFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSegundosFimTesteQuatro() {
        // arrange
        int totalSegundosInicio = 0;
        int duracaoSegundos = 3662;
        int expected = 2;

        // act
        int result = ExercicioDezoito.getSegundosFim(totalSegundosInicio, duracaoSegundos);

        // assert
        assertEquals(expected, result, 0.01);
    }
}