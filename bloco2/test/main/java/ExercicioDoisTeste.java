import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioDoisTeste {

    @Test
    public void getDigito3Teste() {
        // arrange
        int numero = 456;
        double expected = 6;

        // act
        double result = ExercicioDois.getDigito3(numero);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDigito2Teste() {
        // arrange
        int numero = 456;
        double expected = 5;

        // act
        double result = ExercicioDois.getDigito2(numero);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDigito1Teste() {
        // arrange
        int numero = 456;
        double expected = 4;

        // act
        double result = ExercicioDois.getDigito1(numero);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void isParOuImparTesteUm() {
        // arrange
        int numero = 456;
        boolean expected = false;

        // act
        boolean result = ExercicioDois.isParOuImpar(numero);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isParOuImparTesteDois() {
        // arrange
        int numero = 123;
        boolean expected = true;

        // act
        boolean result = ExercicioDois.isParOuImpar(numero);

        // assert
        assertEquals(expected, result);
    }
}