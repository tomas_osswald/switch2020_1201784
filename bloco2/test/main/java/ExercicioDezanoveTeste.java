import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioDezanoveTeste {

    @Test
    public void isValidoTesteUm() {
        // arrange
        double horasTrabalho = 0;
        boolean expected = false;

        // act
        boolean result = ExercicioDezanove.isValido(horasTrabalho);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isValidoTesteDois() {
        // arrange
        double horasTrabalho = -1;
        boolean expected = false;

        // act
        boolean result = ExercicioDezanove.isValido(horasTrabalho);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isValidoTesteTres() {
        // arrange
        double horasTrabalho = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioDezanove.isValido(horasTrabalho);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getSalarioSemanalTesteUm() {
        // arrange
        double horasTrabalho = 1;
        double expected = 7.5;

        // act
        double result = ExercicioDezanove.getSalarioSemanal(horasTrabalho);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSalarioSemanalTesteDois() {
        // arrange
        double horasTrabalho = 36;
        double expected = 270;

        // act
        double result = ExercicioDezanove.getSalarioSemanal(horasTrabalho);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSalarioSemanalTesteTres() {
        // arrange
        double horasTrabalho = 37;
        double expected = 280;

        // act
        double result = ExercicioDezanove.getSalarioSemanal(horasTrabalho);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSalarioSemanalTesteQuatro() {
        // arrange
        double horasTrabalho = 41;
        double expected = 320;

        // act
        double result = ExercicioDezanove.getSalarioSemanal(horasTrabalho);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getSalarioSemanalTesteCinco() {
        // arrange
        double horasTrabalho = 42;
        double expected = 335;

        // act
        double result = ExercicioDezanove.getSalarioSemanal(horasTrabalho);

        // assert
        assertEquals(expected, result, 0.01);
    }
}