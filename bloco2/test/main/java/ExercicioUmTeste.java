import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioUmTeste {

    @org.junit.Test
    public void getMediaPesadaTesteUm() {
        // arrange
        int nota1 = 10;
        int nota2 = 15;
        int nota3 = 20;
        int peso1 = 5;
        int peso2 = 5;
        int peso3 = 5;
        double expected = 15;

        // act
        double result = ExercicioUm.getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        // assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.Test
    public void getMediaPesadaTesteDois() {
        // arrange
        int nota1 = 0;
        int nota2 = 15;
        int nota3 = 20;
        int peso1 = 20;
        int peso2 = 0;
        int peso3 = 0;
        double expected = 0;

        // act
        double result = ExercicioUm.getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        // assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.Test
    public void getMediaPesadaTesteTres() {
        // arrange
        int nota1 = 12;
        int nota2 = 17;
        int nota3 = 1;
        int peso1 = 2;
        int peso2 = 5;
        int peso3 = 17;
        double expected = 5.25;

        // act
        double result = ExercicioUm.getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void isNotaMinimaTesteUm() {
        // arrange
        double mediaPesada = 8;
        String expected = "O aluno cumpre a nota mínima exigida.";

        // act
        String result = ExercicioUm.isNotaMinima(mediaPesada);

        // assert
        assertEquals(result, expected);

    }

    @Test
    public void isNotaMinimaTesteDois() {
        // arrange
        double mediaPesada = 7.9;
        String expected = "O aluno não cumpre a nota mínima exigida.";

        // act
        String result = ExercicioUm.isNotaMinima(mediaPesada);

        // assert
        assertEquals(result, expected);

    }

}