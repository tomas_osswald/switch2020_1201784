import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioCatorzeTeste {

    @Test
    public void isInvalidoTesteUm() {
        // arrange
        double distanciaDia1 = -1;
        double distanciaDia2 = 1;
        double distanciaDia3 = 1;
        double distanciaDia4 = 1;
        double distanciaDia5 = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioCatorze.isInvalido(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void isInvalidoTesteDois() {
        // arrange
        double distanciaDia1 = 1;
        double distanciaDia2 = 1;
        double distanciaDia3 = 1;
        double distanciaDia4 = 1;
        double distanciaDia5 = 1;
        boolean expected = false;

        // act
        boolean result = ExercicioCatorze.isInvalido(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getMediaDiariaKmTesteUm() {
        // arrange
        double distanciaDia1 = 10;
        double distanciaDia2 = 12;
        double distanciaDia3 = 8;
        double distanciaDia4 = 14;
        double distanciaDia5 = 6;
        double expected = 16.09;

        // act
        double result = ExercicioCatorze.getMediaDiariaKm(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getMediaDiariaKmTesteDois() {
        // arrange
        double distanciaDia1 = 0;
        double distanciaDia2 = 0;
        double distanciaDia3 = 0;
        double distanciaDia4 = 0;
        double distanciaDia5 = 0;
        double expected = 0;

        // act
        double result = ExercicioCatorze.getMediaDiariaKm(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5);

        // assert
        assertEquals(expected, result, 0.01);
    }
}