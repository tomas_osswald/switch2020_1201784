import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioDezasseteTeste {

    @Test
    public void isValidoTesteUm() {
        // arrange
        int horasDuracao = 0;
        int minutosDuracao = 0;
        boolean expected = false;

        // act
        boolean result = ExercicioDezassete.isValido(horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isValidoTesteDois() {
        // arrange
        int horasDuracao = 0;
        int minutosDuracao = 1;
        boolean expected = true;

        // act
        boolean result = ExercicioDezassete.isValido(horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isValidoTesteTres() {
        // arrange
        int horasDuracao = -1;
        int minutosDuracao = 1;
        boolean expected = false;

        // act
        boolean result = ExercicioDezassete.isValido(horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isValidoTesteQuatro() {
        // arrange
        int horasDuracao = 24;
        int minutosDuracao = 1;
        boolean expected = false;

        // act
        boolean result = ExercicioDezassete.isValido(horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void isValidoTesteCinco() {
        // arrange
        int horasDuracao = 24;
        int minutosDuracao = 0;
        boolean expected = true;

        // act
        boolean result = ExercicioDezassete.isValido(horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void getHorasChegadaTesteUm() {
        // arrange
        int horasPartida = 0;
        int minutosPartida = 0;
        int horasDuracao = 1;
        int minutosDuracao = 0;
        int expected = 1;

        // act
        int result = ExercicioDezassete.getHorasChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getHorasChegadaTesteDois() {
        // arrange
        int horasPartida = 0;
        int minutosPartida = 0;
        int horasDuracao = 0;
        int minutosDuracao = 59;
        int expected = 0;

        // act
        int result = ExercicioDezassete.getHorasChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getHorasChegadaTesteTres() {
        // arrange
        int horasPartida = 23;
        int minutosPartida = 40;
        int horasDuracao = 0;
        int minutosDuracao = 30;
        int expected = 0;

        // act
        int result = ExercicioDezassete.getHorasChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getMinutosChegadaTesteUm() {
        // arrange
        int horasPartida = 0;
        int minutosPartida = 0;
        int horasDuracao = 0;
        int minutosDuracao = 1;
        int expected = 1;

        // act
        int result = ExercicioDezassete.getMinutosChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getMinutosChegadaTesteDois() {
        // arrange
        int horasPartida = 0;
        int minutosPartida = 0;
        int horasDuracao = 1;
        int minutosDuracao = 0;
        int expected = 0;

        // act
        int result = ExercicioDezassete.getMinutosChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getMinutosChegadaTesteTres() {
        // arrange
        int horasPartida = 0;
        int minutosPartida = 20;
        int horasDuracao = 0;
        int minutosDuracao = 50;
        int expected = 10;

        // act
        int result = ExercicioDezassete.getMinutosChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getDiaChegadaTesteUm() {
        // arrange
        int horasPartida = 0;
        int minutosPartida = 0;
        int horasDuracao = 0;
        int minutosDuracao = 1;
        String expected = "O comboio chega no mesmo dia em que parte";

        // act
        String result = ExercicioDezassete.getDiaChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected);
    }

    @Test
    public void getDiaChegadaTesteDois() {
        // arrange
        int horasPartida = 23;
        int minutosPartida = 59;
        int horasDuracao = 0;
        int minutosDuracao = 1;
        String expected = "O comboio chega no dia seguinte ao de partida";

        // act
        String result = ExercicioDezassete.getDiaChegada(horasPartida, minutosPartida, horasDuracao, minutosDuracao);

        // assert
        assertEquals(result, expected);
    }
}