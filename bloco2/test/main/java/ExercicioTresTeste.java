import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioTresTeste {

    @Test
    public void getDistanciaTesteUm() {
        // arrange
        int xPonto1 = 2;
        int yPonto1 = 8;
        int xPonto2 = 10;
        int yPonto2 = 8;
        double expected = 8;

        // act
        double result = ExercicioTres.getDistancia(xPonto1, yPonto1, xPonto2, yPonto2);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getDistanciaTesteDois() {
        // arrange
        int xPonto1 = 1;
        int yPonto1 = 1;
        int xPonto2 = 10;
        int yPonto2 = 10;
        double expected = 12.73;

        // act
        double result = ExercicioTres.getDistancia(xPonto1, yPonto1, xPonto2, yPonto2);

        // assert
        assertEquals(result, expected, 0.01);
    }

    @Test
    public void getDistanciaTesteTres() {
        // arrange
        int xPonto1 = 2;
        int yPonto1 = 4;
        int xPonto2 = 1;
        int yPonto2 = 43;
        double expected = 39.01;

        // act
        double result = ExercicioTres.getDistancia(xPonto1, yPonto1, xPonto2, yPonto2);

        // assert
        assertEquals(result, expected, 0.01);
    }
}