import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioDozeTeste {

    @Test
    public void getNotificacaoTesteUm() {
        // arrange
        double indicePoluicao = 0.2;
        String expected = "Impecável, bom trabalho";

        // act
        String result = ExercicioDoze.getNotificacao(indicePoluicao);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getNotificacaoTesteDois() {
        // arrange
        double indicePoluicao = 0.3;
        String expected = "As indústrias do 1º grupo devem suspender actividade";

        // act
        String result = ExercicioDoze.getNotificacao(indicePoluicao);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getNotificacaoTesteTres() {
        // arrange
        double indicePoluicao = 0.4;
        String expected = "As indústrias do 1º e 2º grupo devem suspender actividade";

        // act
        String result = ExercicioDoze.getNotificacao(indicePoluicao);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getNotificacaoTesteQuatro() {
        // arrange
        double indicePoluicao = 0.5;
        String expected = "ALERTA VERMELHO";

        // act
        String result = ExercicioDoze.getNotificacao(indicePoluicao);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void getNotificacaoTesteCinco() {
        // arrange
        double indicePoluicao = -2;
        String expected = "Valor inválido";

        // act
        String result = ExercicioDoze.getNotificacao(indicePoluicao);

        // assert
        assertEquals(expected, result);
    }
}