import org.junit.Test;

import static org.junit.Assert.*;

public class CuboTeste {

    @Test
    public void construtorErroTamanhoNegativo(){
        // arrange
        double areaFace = -1;

        // act

        // assert
        assertThrows("A área da face não pode ter valor negativo", IllegalArgumentException.class, () -> {
            Cubo cuboUm = new Cubo(areaFace);
        });
    }

    @Test
    public void obterVolumeAreaFaceUm(){
        // arrange
        double areaFace = 1;
        Cubo cuboUm = new Cubo(areaFace);
        double expected = 0.06804138174397717;
        double result;

        // act
        result = cuboUm.obterVolume();

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void obterClassificacaoAreaFaceUm(){
        // arrange
        double areaFace = 1;
        Cubo cuboUm = new Cubo(areaFace);
        String expected = "O cubo é pequeno";
        String result;

        // act
        result = cuboUm.obterClassificacao();

        // assert
        assertEquals(expected, result);
    }
}
