import java.util.Scanner;

public class ExercicioDez {

    /*
    public static void main(String[] args) {

        // Função para calcular desconto e preço final de um artigo através de valor inicial
        // Estrutura de dados
        double precoBase, desconto, precoFinal;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o preço inicial do artigo");
        precoBase = ler.nextDouble();

        // Processamento
        if ( isValido(precoBase) == 1 ) {
            desconto = getDesconto(precoBase);
            precoFinal = getPrecoFinal(precoBase, desconto);

            System.out.println("O preço do artigo com desconto é " + String.format("%.2f", precoFinal) );
        } else {
            System.out.println("O preço não é válido");
        }
    }

     */

    public static int isValido(double precoBase) {

        int result;

        if (precoBase > 0) {
            result = 1;
        } else {
            result = 0;
        }

        return result;
    }

    public static double getDesconto(double precoBase) {

        double desconto;

        if (precoBase <= 50) {
            desconto = 0.2;
        } else if (precoBase <= 100) {
            desconto = 0.3;
        } else if (precoBase <= 200) {
            desconto = 0.4;
        } else {
            desconto = 0.6;
        }

        return desconto;
    }

    public static double getPrecoFinal(double precoBase, double desconto) {
        return precoBase * (1.0 - desconto);
    }

}
