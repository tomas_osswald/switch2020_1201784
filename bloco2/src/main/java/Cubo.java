public class Cubo {

    private double areaFace = -1;

    private Cubo() {

    }

    public Cubo(double areaFace) {
        if (areaFace < 0) {
            throw new IllegalArgumentException("A área da face não pode ter valor negativo");
        }
        this.areaFace = areaFace;
    }

    public double obterVolume() {
        return Math.pow(Math.sqrt(areaFace / 6), 3);
    }

    public String obterClassificacao() {
        String classificacao;

        if (obterVolume() <= 1000) {
            classificacao = "O cubo é pequeno";

        } else if (obterVolume() > 2000) {
            classificacao = "O cubo é grande";

        } else {
            classificacao = "O cubo é médio";

        }

        return classificacao;
    }

}
