import java.util.Scanner;

public class ExercicioCinco {

 /*
    public static void main(String[] args) {

        // Cálculo do volume dum cubo através da área duma face.
        // Estrutura de dados
        double area, volume;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a área em cm2");
        area = ler.nextDouble();

        // Processamento
        if (isValido(area) == 1) {
            volume = getVolume(area);
            System.out.println("Volume do cubo = " + String.format("%.2f", volume));
            System.out.println(isTamanho(volume));

        } else {
            System.out.println("Valor da área incorrecto");
        }

    }*/



    public static int isValido(double area) {

        // retorna 0 se a área não for válida
        if (area > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public static double getVolume(double area) {
        return Math.pow(Math.sqrt(area / 6), 3);
    }

    public static String isTamanho(double volume) {

        String result;

        if (volume <= 1000) {
            result = "O cubo é pequeno";

        } else if (volume > 2000) {
            result = "O cubo é grande";

        } else {
            result = "O cubo é médio";

        }

        return result;
    }
}
