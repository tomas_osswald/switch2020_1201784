import java.util.Scanner;

public class ExercicioVinte {


    /*
    public static void main(String[] args) {

        // Calcular valor de aluguer de equipamento de jardinagem em função do dia da semana ou feriado
        // Estrutura de dados
        int tipoDeKit, diaDaSemana;
        double valorAluguer, distancia, valorTransporte, valorTotal;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o tipo de Kit que pretende \n 1 - Elementar \n 2 - Semi-completo \n 3 - Completo");
        tipoDeKit = ler.nextInt();
        System.out.println("Introduza o dia da semana ou feriado \n 1 - Segunda a Sexta \n 2 - Sábado, Domingo ou Feriado");
        diaDaSemana = ler.nextInt();
        System.out.println("Introduza a distância da sua casa à empresa em km");
        distancia = ler.nextDouble();

        // Processamento

        valorAluguer = getValorAluguer(tipoDeKit, diaDaSemana);
        valorTransporte = getValorTransporte(distancia);
        valorTotal = getValorTotal(valorAluguer, valorTransporte);

        // Escritura de dados
        System.out.println("O valor do aluguer do equipamento é " + valorAluguer + " Euros.");
        System.out.println("O valor do transporte é " + valorTransporte + " Euros.");
        System.out.println("O valor total é " + valorTotal + " Euros.");

    }

     */
    public static double getValorAluguer(int tipoDeKit, int diaDaSemana) {

        double valorAluguer = -1;

        switch (tipoDeKit) {
            case 1:
                switch (diaDaSemana) {
                    case 1:
                        valorAluguer = 30;
                        break;
                    case 2:
                        valorAluguer = 40;
                        break;

                }
                break;
            case 2:
                switch (diaDaSemana) {
                    case 1:
                        valorAluguer = 50;
                        break;
                    case 2:
                        valorAluguer = 70;
                        break;
                }
                break;
            case 3:
                switch (diaDaSemana) {
                    case 1:
                        valorAluguer = 100;
                        break;
                    case 2:
                        valorAluguer = 140;
                        break;
                }
                break;
        }

        return valorAluguer;
    }

    public static double getValorTransporte(double distancia) {
        return distancia * 2;
    }

    public static double getValorTotal(double valorAluguer, double valorTransporte) {
        return valorTransporte + valorAluguer;
    }

}
