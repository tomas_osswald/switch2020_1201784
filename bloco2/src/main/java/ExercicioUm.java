import java.util.Scanner;

public class ExercicioUm {

    /*
    public static void main(String[] args) {

        // Função para calcular média.
        // Estrutura de dados
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPesada;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a nota 1");
        nota1 = ler.nextInt();
        System.out.println("Introduza a nota 2");
        nota2 = ler.nextInt();
        System.out.println("Introduza a nota 3");
        nota3 = ler.nextInt();
        System.out.println("Introduza o peso 1");
        peso1 = ler.nextInt();
        System.out.println("Introduza o peso 2");
        peso2 = ler.nextInt();
        System.out.println("Introduza o peso 3");
        peso3 = ler.nextInt();

        // Processamento
        mediaPesada = getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        // Escrita do resultado
        System.out.println("A média é " + String.format("%.2f", mediaPesada));

        System.out.println( isNotaMinima(mediaPesada) );

    } */

    public static double getMediaPesada(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        return ((double)(nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);
    }

    public static String isNotaMinima(double mediaPesada) {

        String result;

        if (mediaPesada >= 8){
            result = "O aluno cumpre a nota mínima exigida.";
        } else {
            result = "O aluno não cumpre a nota mínima exigida.";
        }

        return result;
    }
}
