import java.util.Scanner;

public class ExercicioDoze {

    /*
    public static void main(String[] args) {

        // Função para classificar índice de poluição e notificar indústria
        // Estrutura de dados
        double indicePoluicao;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o índice de poluição");
        indicePoluicao = ler.nextDouble();

        // Processamento e escritura de dados
        System.out.println(getNotificacao(indicePoluicao) );
    }

     */

    public static String getNotificacao(double indicePoluicao) {

        String result;

        if (indicePoluicao < 0) {
            result = "Valor inválido";
        } else if (indicePoluicao < 0.3) {
            result = "Impecável, bom trabalho";
        } else if (indicePoluicao < 0.4) {
            result = "As indústrias do 1º grupo devem suspender actividade";
        } else if (indicePoluicao < 0.5) {
            result = "As indústrias do 1º e 2º grupo devem suspender actividade";
        } else {
            result = "ALERTA VERMELHO";
        }

        return result;
    }
}
