import java.util.Scanner;

public class ExercicioNove {

    /*
    public static void main(String[] args) {

        // Função para separar os 3 dígitos de um número em inteiros e verificar sequência
        // Estrutura de dados
        int numero, digito1, digito2,digito3;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza um número com 3 dígitos");
        numero = ler.nextInt();

        // Verificação - 'numero' tem 3 dígitos
        if ( isInvalido(numero) ) {
            System.out.println("Número não tem 3 dígitos");

        } else {

            // Processamento
            digito3 = getDigito3(numero);
            digito2 = getDigito2(numero);
            digito1 = getDigito1(numero);

            System.out.println(isCrescente(digito1, digito2, digito3));

        }

    }

     */

    public static boolean isInvalido(int numero) {

        boolean result;

        result = numero < 100 || numero > 999;

        return result;
    }

    public static int getDigito3(int numero) {
        return numero % 10;
    }

    public static int getDigito2(int numero) {
        return (numero / 10) % 10;
    }

    public static int getDigito1(int numero) {
        return (numero / 100) % 10;
    }

    public static String isCrescente(int digito1, int digito2, int digito3) {

        String result;

        if ( digito1 < digito2 && digito2 < digito3 ) {
            result = "A sequência de algarismos é crescente";
        } else {
            result = "A sequência de algarismos não é crescente";
        }

        return result;

    }

}
