import java.util.Scanner;

public class ExercicioCatorze {

    /*
    public static void main(String[] args) {

        // Calcular média de km percorridos por um estafeta durante uma semana de trabalho
        // Estrutura de dados
        double distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5, mediaDiariaKm;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a distância percorrida no dia 1");
        distanciaDia1 = ler.nextDouble();
        System.out.println("Introduza a distância percorrida no dia 2");
        distanciaDia2 = ler.nextDouble();
        System.out.println("Introduza a distância percorrida no dia 3");
        distanciaDia3 = ler.nextDouble();
        System.out.println("Introduza a distância percorrida no dia 4");
        distanciaDia4 = ler.nextDouble();
        System.out.println("Introduza a distância percorrida no dia 5");
        distanciaDia5 = ler.nextDouble();

        // Processamento
        if (isInvalido(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5)) {
            System.out.println("Valores inválidos");
        } else {
            mediaDiariaKm = getMediaDiariaKm(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4, distanciaDia5);
            System.out.println("A distância média diária percorrida é de " + String.format("%.2f", mediaDiariaKm) + " km");
        }

    }

     */

    public static boolean isInvalido(double distanciaDia1, double distanciaDia2, double distanciaDia3, double distanciaDia4, double distanciaDia5) {
        return distanciaDia1 < 0 || distanciaDia2 < 0 || distanciaDia3 < 0 || distanciaDia4 < 0 || distanciaDia5 < 0;
    }

    public static double getMediaDiariaKm(double distanciaDia1, double distanciaDia2, double distanciaDia3, double distanciaDia4, double distanciaDia5) {
        return ( (distanciaDia1 + distanciaDia2 + distanciaDia3 + distanciaDia4 + distanciaDia5) / 5 ) * 1.609;
    }

}
