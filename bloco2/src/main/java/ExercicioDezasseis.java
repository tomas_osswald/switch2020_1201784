import java.util.Scanner;

public class ExercicioDezasseis {

    /*
    public static void main(String[] args) {

        // Classificar um triângulo e verificar se ele é possível através dos seus ângulos
        // Estrutura de dados
        double anguloA, anguloB, anguloC;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor do lado A do triângulo");
        anguloA = ler.nextDouble();
        System.out.println("Introduza o valor do lado B do triângulo");
        anguloB = ler.nextDouble();
        System.out.println("Introduza o valor do lado C do triângulo");
        anguloC = ler.nextDouble();

        // Processamento
        if ( !isTrianguloPossivel(anguloA, anguloB, anguloC) ) {
            System.out.println("Triângulo não é possível");
        } else {
            if ( isTrianguloRectangulo (anguloA, anguloB, anguloC) ) {
                System.out.println("Triângulo é rectângulo");
            } else if ( isTrianguloObtusangulo (anguloA, anguloB, anguloC) ) {
                System.out.println("Triângulo é obtusângulo");
            } else {
                System.out.println("Triângulo é acutângulo");
            }
        }

    }

     */

    public static boolean isTrianguloPossivel(double anguloA, double anguloB, double anguloC) {
        return anguloA > 0 && anguloB > 0 && anguloC > 0 && (anguloA + anguloB + anguloC) == 180;
    }

    public static boolean isTrianguloRectangulo(double anguloA, double anguloB, double anguloC) {
        return anguloA == 90 || anguloB == 90 || anguloC == 90;
    }

    public static boolean isTrianguloObtusangulo(double anguloA, double anguloB, double anguloC) {
        return anguloA > 90 || anguloB > 90 || anguloC > 90;
    }

}
