import java.util.Scanner;

public class ExercicioSeis {

    /*
    public static void main(String[] args) {

        // Conversão de segundos em horas do dia e saudação.
        // Estrutura de dados
        int segundosTotal, horas, minutos, segundos;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a hora do dia em segundos");
        segundosTotal = ler.nextInt();

        // Processamento
        horas = getHoras(segundosTotal);
        minutos = getMinutos(segundosTotal);
        segundos = getSegundos(segundosTotal);

        // Escritura de dados
        System.out.println(horas + ":" + minutos + ":" + segundos);

        System.out.println( isDiaTardeNoite(horas, minutos, segundos) );


    }

     */

    public static int getHoras(int segundosTotal) {
        return segundosTotal / 3600;
    }

    public static int getMinutos(int segundosTotal) {
        return (segundosTotal % 3600) / 60;
    }

    public static int getSegundos(int segundosTotal) {
        return (segundosTotal % 3600) % 60;
    }

    public static String isDiaTardeNoite(int horas, int minutos, int segundos) {

        String result;

        if (horas >= 6 && horas < 12 || (horas == 12 && minutos == 0 && segundos == 0)) {
            result = "Bom dia";
        } else if (horas >= 12 && horas < 20 || (horas == 20 && minutos == 0 && segundos == 0)) {
            result = "Boa tarde";
        } else {
            result = "Boa noite";
        }

        return result;
    }

}
