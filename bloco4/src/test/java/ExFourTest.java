import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExFourTest {

    @Test
    void isEvenResultTrue() {
        // arrange
        int number = 0;
        boolean expected = true;

        // act
        boolean result = ExFour.isEven(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isEvenResultFalse() {
        // arrange
        int number = 1;
        boolean expected = false;

        // act
        boolean result = ExFour.isEven(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getEvensFromArrayResultTwo() {
        // arrange
        int[] array = {1,2,3};
        int expected = 2;

        // act
        int result = ExFour.getEvensFromArray(array);
        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getEvensFromArrayResultSixEight() {
        // arrange
        int[] array = {3,6,7,8,1};
        int expected = 68;

        // act
        int result = ExFour.getEvensFromArray(array);
        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getEvensFromArrayResultZero() {
        // arrange
        int[] array = {0,1};
        int expected = 0;

        // act
        int result = ExFour.getEvensFromArray(array);
        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getArrayEvensFromArrayResultZero() {
        // arrange
        int[] array = {0};
        int[] expected = {0};

        // act
        int[] result = ExFour.getArrayEvensFromArray(array);
        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getArrayEvensFromArrayResultNul() { // FIXME: 02/11/2020
        // arrange
        int[] array = {1};

        // act
        int[] result = ExFour.getArrayEvensFromArray(array);
        // assert
        assertNull(result);
    }

    @Test
    void getArrayEvensFromArrayResultSixEight() {
        // arrange
        int[] array = {3,6,7,8,1};
        int[] expected = {6,8};

        // act
        int[] result = ExFour.getArrayEvensFromArray(array);
        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getArrayEvensFromArrayResultTwoFour() {
        // arrange
        int[] array = {1,2,3,4};
        int[] expected = {2,4};

        // act
        int[] result = ExFour.getArrayEvensFromArray(array);
        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getOddsFromArrayResultOne() {
        // arrange
        int[] array = {1,2};
        int expected = 1;

        // act
        int result = ExFour.getOddsFromArray(array);
        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getOddsFromArrayResultThreeSevenOne() {
        // arrange
        int[] array = {3,6,7,8,1};
        int expected = 371;

        // act
        int result = ExFour.getOddsFromArray(array);
        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getOddsFromArrayResultOneInputZeroOne() {
        // arrange
        int[] array = {0,1};
        int expected = 1;

        // act
        int result = ExFour.getOddsFromArray(array);
        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getArrayOddsFromArrayResultOne() {
        // arrange
        int[] array = {1};
        int[] expected = {1};

        // act
        int[] result = ExFour.getArrayOddsFromArray(array);
        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getArrayOddsFromArrayResultNul() { // FIXME: 02/11/2020
        // arrange
        int[] array = {0};

        // act
        int[] result = ExFour.getArrayOddsFromArray(array);
        // assert
        assertNull(result);
    }

    @Test
    void getArrayOddsFromArrayResultThreeSevenOne() {
        // arrange
        int[] array = {3,6,7,8,1};
        int[] expected = {3,7,1};

        // act
        int[] result = ExFour.getArrayOddsFromArray(array);
        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getArrayOddsFromArrayResultOneThree() {
        // arrange
        int[] array = {1,2,3,4};
        int[] expected = {1,3};

        // act
        int[] result = ExFour.getArrayOddsFromArray(array);
        // assert
        assertArrayEquals(expected, result);
    }
}