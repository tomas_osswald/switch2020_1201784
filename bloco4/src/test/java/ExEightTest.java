import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExEightTest {

    @Test
    void countMultiplesFromArrayInIntervalResultTwo() {
        // arrange
        int[] array = {2,3};
        int intervalBegin = 4;
        int intervalEnd = 12;
        int expected = 2;

        // act
        int result = ExEight.countMultiplesFromArrayInInterval(array, intervalBegin, intervalEnd);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void countMultiplesFromArrayInIntervalResultOne() {
        // arrange
        int[] array = {3,6};
        int intervalBegin = 5;
        int intervalEnd = 6;
        int expected = 1;

        // act
        int result = ExEight.countMultiplesFromArrayInInterval(array, intervalBegin, intervalEnd);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalResultSixTwelve() {
        // arrange
        int[] array = {1,2,3};
        int intervalBegin = 4;
        int intervalEnd = 12;
        int[] expected = {6,12};

        // act
        int[] result = ExEight.getCommonMultiplesFromArrayInInterval(array, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalResultTwo() {
        // arrange
        int[] array = {1,2};
        int intervalBegin = 1;
        int intervalEnd = 2;
        int[] expected = {2};

        // act
        int[] result = ExEight.getCommonMultiplesFromArrayInInterval(array, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalResultZeroTen() {
        // arrange
        int[] array = {5,2};
        int intervalBegin = 0;
        int intervalEnd = 10;
        int[] expected = {0,10};

        // act
        int[] result = ExEight.getCommonMultiplesFromArrayInInterval(array, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalVersionTwo() {
        // arrange
        int[] array = {1,2,3};
        int intervalBegin = 4;
        int intervalEnd = 12;
        int[] expected = {6,12};

        // act
        int[] result = ExEight.getCommonMultiplesFromArrayInIntervalVersionTwo(array, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalVersionTwoResultTwentyForty() {
        // arrange
        int[] array = {2,4,5,10};
        int intervalBegin = 2;
        int intervalEnd = 40;
        int[] expected = {20,40};

        // act
        int[] result = ExEight.getCommonMultiplesFromArrayInIntervalVersionTwo(array, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalVersionTwoResultNoMultiples() {
        // arrange
        int[] array = {2,5,10,20};
        int intervalBegin = 2;
        int intervalEnd = 10;
        int[] expected = null;

        // act
        int[] result = ExEight.getCommonMultiplesFromArrayInIntervalVersionTwo(array, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalVersionTwoResult420_840() {
        // arrange
        int[] array = {2,5,10,20,21};
        int intervalBegin = 2;
        int intervalEnd = 1000;
        int[] expected = {420,840};

        // act
        int[] result = ExEight.getCommonMultiplesFromArrayInIntervalVersionTwo(array, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }
}