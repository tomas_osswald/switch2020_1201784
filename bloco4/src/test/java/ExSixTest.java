import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExSixTest {

    @Test
    void getArrayWithFirstXIntsFromArrayResultOneTwoThreeFour() {
        // arrange
        int[] array = {1,2,3,4,5};
        int x = 4;
        int[] expected = {1,2,3,4};

        // act
        int[] result = ExSix.truncateArray(array, x);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getArrayWithFirstXIntsFromArrayResultOne() {
        // arrange
        int[] array = {1,2,3,4,5};
        int x = 1;
        int[] expected = {1};

        // act
        int[] result = ExSix.truncateArray(array, x);

        // assert
        assertArrayEquals(expected, result);
        assertEquals(expected.length, result.length);
        assertNotSame(array, result);
    }

    @Test
    void getArrayWithFirstXIntsFromArrayResultZero() {
        // arrange
        int[] array = {0};
        int x = 2;
        int[] expected = {0};

        // act
        int[] result = ExSix.truncateArray(array, x);

        // assert
        assertArrayEquals(expected, result);
    }
}