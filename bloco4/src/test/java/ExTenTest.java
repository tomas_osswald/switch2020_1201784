import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExTenTest {

    @Test
    void getSmallestIntFromArrayResultTwo() {
        // arrange
        int[] array = {3,17,91,2};
        int expected = 2;

        // act
        int result = ExTen.getSmallestIntFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSmallestIntFromArrayResultZero() {
        // arrange
        int[] array = {0};
        int expected = 0;

        // act
        int result = ExTen.getSmallestIntFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSmallestIntFromArrayResultOne() {
        // arrange
        int[] array = {3,6,7,8,1};
        int expected = 1;

        // act
        int result = ExTen.getSmallestIntFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getLargestIntFromArrayResultNinetyOne() {
        // arrange
        int[] array = {3,17,91,2};
        int expected = 91;

        // act
        int result = ExTen.getLargestIntFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getLargestIntFromArrayResultZero() {
        // arrange
        int[] array = {0};
        int expected = 0;

        // act
        int result = ExTen.getLargestIntFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getLargestIntFromArrayResultOne() {
        // arrange
        int[] array = {0,1};
        int expected = 1;

        // act
        int result = ExTen.getLargestIntFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getLargestIntFromArrayResultEight() {
        // arrange
        int[] array = {3,6,7,8,1};
        int expected = 8;

        // act
        int result = ExTen.getLargestIntFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAverageFromArrayResultZero() {
        // arrange
        int[] array = {0};
        double expected = 0;

        // act
        double result = ExTen.getAverageFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAverageFromArrayResultZeroPointFive() {
        // arrange
        int[] array = {0,1};
        double expected = 0.5;

        // act
        double result = ExTen.getAverageFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAverageFromArrayResultFive() {
        // arrange
        int[] array = {3,6,7,8,1};
        double expected = 5;

        // act
        double result = ExTen.getAverageFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getProductFromArrayResultZero() {
        // arrange
        int[] array = {0,6,7,8,1};
        double expected = 0;

        // act
        double result = ExTen.getProductFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getProductFromArrayResultOne() {
        // arrange
        int[] array = {1};
        double expected = 1;

        // act
        double result = ExTen.getProductFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getProductFromArrayResultOneThousandEight() {
        // arrange
        int[] array = {3,6,7,8,1};
        double expected = 1008;

        // act
        double result = ExTen.getProductFromArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNonRepeatedElementsFromArrayResultOne() {
        // arrange
        int[] array = {0,0,1};
        int[] expected = {1};

        // act
        int[] result = ExTen.getNonRepeatedElementsFromArray(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getNonRepeatedElementsFromArrayResultZeroOne() {
        // arrange
        int[] array = {0,1};
        int[] expected = {0,1};

        // act
        int[] result = ExTen.getNonRepeatedElementsFromArray(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getNonRepeatedElementsFromArrayResultSixSevenEightOne() {
        // arrange
        int[] array = {3,6,7,8,1,3};
        int[] expected = {6,7,8,1};

        // act
        int[] result = ExTen.getNonRepeatedElementsFromArray(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getReversedElementsFromArrayResultZero() {
        // arrange
        int[] array = {0};
        int[] expected = {0};

        // act
        int[] result = ExTen.getReversedElementsFromArray(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getReversedElementsFromArrayResultOneZero() {
        // arrange
        int[] array = {0,1};
        int[] expected = {1,0};

        // act
        int[] result = ExTen.getReversedElementsFromArray(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getReversedElementsFromArrayResultThreeTwoOne() {
        // arrange
        int[] array = {1,2,3};
        int[] expected = {3,2,1};

        // act
        int[] result = ExTen.getReversedElementsFromArray(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getReversedElementsFromArrayResultOneEightSevenSixThree() {
        // arrange
        int[] array = {3,6,7,8,1};
        int[] expected = {1,8,7,6,3};

        // act
        int[] result = ExTen.getReversedElementsFromArray(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getPrimeElementsFromArrayResultThreeFiveSevenElevenThirteen() {
        // arrange
        int[] array = {3,5,7,11,13,14};
        int[] expected = {3,5,7,11,13};

        // act
        int[] result = ExTen.getPrimeElementsFromArray(array);

        // assert
        assertArrayEquals(expected,result);
    }

    @Test
    void getPrimeElementsFromArrayResultTwoThree() {
        // arrange
        int[] array = {1,2,3};
        int[] expected = {2,3};

        // act
        int[] result = ExTen.getPrimeElementsFromArray(array);

        // assert
        assertArrayEquals(expected,result);
    }

    @Test
    void getPrimeElementsFromArrayResultTwo() {
        // arrange
        int[] array = {0,1,2};
        int[] expected = {2};

        // act
        int[] result = ExTen.getPrimeElementsFromArray(array);

        // assert
        assertArrayEquals(expected,result);
    }
}