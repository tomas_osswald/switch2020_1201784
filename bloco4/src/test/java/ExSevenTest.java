import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExSevenTest {

    @Test
    void isNumberOneMultipleOfNumberTwoResultTrue() {
        // arrange
        int numberOne = 4;
        int numberTwo = 2;
        boolean expected = true;

        // act
        boolean result = ExSeven.isNumberOneMultipleOfNumberTwo(numberOne, numberTwo);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isNumberOneMultipleOfNumberTwoResultFalse() {
        // arrange
        int numberOne = 3;
        int numberTwo = 2;
        boolean expected = false;

        // act
        boolean result = ExSeven.isNumberOneMultipleOfNumberTwo(numberOne, numberTwo);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getMultiplesOfNumberInIntervalResultSixNine() {
        // arrange
        int number = 3;
        int intervalBegin = 4;
        int intervalEnd = 10;
        int[] expected = {6,9};

        // act
        int[] result = ExSeven.getMultiplesOfNumberInInterval(number, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMultiplesOfNumberInIntervalResultFourSixEightTen() {
        // arrange
        int number = 2;
        int intervalBegin = 4;
        int intervalEnd = 10;
        int[] expected = {4,6,8,10};

        // act
        int[] result = ExSeven.getMultiplesOfNumberInInterval(number, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMultiplesOfNumberInIntervalResultTwo() {
        // arrange
        int number = 2;
        int intervalBegin = 1;
        int intervalEnd = 3;
        int[] expected = {2};

        // act
        int[] result = ExSeven.getMultiplesOfNumberInInterval(number, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMultiplesOfNumberInIntervalResultOne() {
        // arrange
        int number = 1;
        int intervalBegin = 1;
        int intervalEnd = 1;
        int[] expected = {1};

        // act
        int[] result = ExSeven.getMultiplesOfNumberInInterval(number, intervalBegin, intervalEnd);

        // assert
        assertArrayEquals(expected, result);
    }
}