import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExTwelveTest {

    @Test
    void checkIfNumberOfColumnsIsEqualResultThree() {
        // arrange
        double[][] array = {{0,1,2},{0,1,2}};
        int expected = 3;

        // act
        int result = ExTwelve.checkIfNumberOfColumnsIsEqualDoublesArray(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void checkIfNumberOfColumnsIsEqualResultTwo() {
        // arrange
        double[][] array = {{0,1},{3,4},{1,2}};
        int expected = 2;

        // act
        int result = ExTwelve.checkIfNumberOfColumnsIsEqualDoublesArray(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void checkIfNumberOfColumnsIsEqualResultNegative() {
        // arrange
        double[][] array = {{0,1,2},{0,1}};
        int expected = -1;

        // act
        int result = ExTwelve.checkIfNumberOfColumnsIsEqualDoublesArray(array);

        // assert
        assertEquals(expected, result);
    }
}