import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExSeventeenTest {

    @Test
    void getProductMatrixWithConstantInputConstantTwo() {
        // arrange
        double[][] array = {{1,2,3},{4,5,6}};
        int constant = 2;
        double[][] expected = {{2,4,6},{8,10,12}};

        // act
        double[][] result = ExSeventeen.getProductMatrixWithConstant(array, constant);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getProductMatrixWithConstantInputConstantZero() {
        // arrange
        double[][] array = {{1,2,3},{4,5,6}};
        int constant = 0;
        double[][] expected = {{0,0,0},{0,0,0}};

        // act
        double[][] result = ExSeventeen.getProductMatrixWithConstant(array, constant);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getProductMatrixWithConstantInputConstantFive() {
        // arrange
        double[][] array = {{1,2,3,7,8,9},{4,5,6}};
        int constant = 5;
        double[][] expected = {{5,10,15,35,40,45},{20,25,30}};

        // act
        double[][] result = ExSeventeen.getProductMatrixWithConstant(array, constant);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getSumTwoMatricesInputEqualMatrices() {
        // arrange
        double[][] arrayOne = {{1,2,3},{4,5,6}};
        double[][] arrayTwo = {{1,2,3},{4,5,6}};
        double[][] expected = {{2,4,6},{8,10,12}};

        // act
        double[][] result = ExSeventeen.getSumTwoMatrices(arrayOne, arrayTwo);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getSumTwoMatricesInputDifferentMatrices() {
        // arrange
        double[][] arrayOne = {{1,2,3},{4,5,6}};
        double[][] arrayTwo = {{-1,-2,-3},{4,-5,6}};
        double[][] expected = {{0,0,0},{8,0,12}};

        // act
        double[][] result = ExSeventeen.getSumTwoMatrices(arrayOne, arrayTwo);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getProductTwoMatricesInputEqualMatrices() {
        // arrange
        double[][] arrayOne = {{1,2,3},{4,5,6}};
        double[][] arrayTwo = {{1,2,3},{4,5,6}};
        double[][] expected = {{1,4,9},{16,25,36}};

        // act
        double[][] result = ExSeventeen.getProductTwoMatrices(arrayOne, arrayTwo);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getProductTwoMatricesInputDifferentMatrices() {
        // arrange
        double[][] arrayOne = {{1,2,3},{4,5,6}};
        double[][] arrayTwo = {{3,2,1},{0,-1,-2}};
        double[][] expected = {{3,4,3},{0,-5,-12}};

        // act
        double[][] result = ExSeventeen.getProductTwoMatrices(arrayOne, arrayTwo);

        // assert
        assertArrayEquals(expected, result);
    }
}