import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExFiveTest {

    @Test
    void getEvensFromIntResultTwo() {
        // arrange
        int number = 12;
        int expected = 2;

        // act
        int result = ExFive.getEvensFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getEvensFromIntResultSixEight() {
        // arrange
        int number = 36781;
        int expected = 86;

        // act
        int result = ExFive.getEvensFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getOddsFromIntResultOne() {
        // arrange
        int number = 12;
        int expected = 1;

        // act
        int result = ExFive.getOddsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getOddsFromIntResultThreeSevenOne() {
        // arrange
        int number = 36781;
        int expected = 173;

        // act
        int result = ExFive.getOddsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumDigitsFromIntResultOne() {
        // arrange
        int number = 1;
        int expected = 1;

        // act
        int result = ExFive.getSumDigitsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumDigitsFromIntResultTwo() {
        // arrange
        int number = 11;
        int expected = 2;

        // act
        int result = ExFive.getSumDigitsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumDigitsFromIntResultTwentyFive() {
        // arrange
        int number = 36781;
        int expected = 25;

        // act
        int result = ExFive.getSumDigitsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumEvensFromIntResultFourteen() {
        // arrange
        int number = 36781;
        int expected = 14;

        // act
        int result = ExFive.getSumEvensFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumEvensFromIntResultZero() {
        // arrange
        int number = 13;
        int expected = 0;

        // act
        int result = ExFive.getSumEvensFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumEvensFromIntResultTwo() {
        // arrange
        int number = 12;
        int expected = 2;

        // act
        int result = ExFive.getSumEvensFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumEvensFromIntResultSix() {
        // arrange
        int number = 1234;
        int expected = 6;

        // act
        int result = ExFive.getSumEvensFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumOddsFromIntResultFour() {
        // arrange
        int number = 1234;
        int expected = 4;

        // act
        int result = ExFive.getSumOddsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumOddsFromIntResultOne() {
        // arrange
        int number = 12;
        int expected = 1;

        // act
        int result = ExFive.getSumOddsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumOddsFromIntResultEleven() {
        // arrange
        int number = 36781;
        int expected = 11;

        // act
        int result = ExFive.getSumOddsFromInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }
}