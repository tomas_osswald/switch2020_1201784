import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExNineteenTest {

    @Test
    void createCopyOfSquareMatrixNotSquareResultNull() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,1},
                {8,5,1},
                {4,7,0},};

        // act
        int[][] result = ExNineteen.createCopyOfSquareMatrix(arraySudoku);

        // assert
        assertNull(result);
    }

    @Test
    void createCopyOfSquareMatrixSizeThreeSquare() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int[][] expected = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};

        // act
        int[][] result = ExNineteen.createCopyOfSquareMatrix(arraySudoku);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void createCopyOfSquareMatrixSizeNineSquare() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int[][] expected = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};

        // act
        int[][] result = ExNineteen.createCopyOfSquareMatrix(arraySudoku);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMaskMatrixNumberOne() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int number = 1;
        int[][] expected = {
                {0,0,0,0,1,0,0,0,0},
                {0,0,1,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,1,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,1,0},
                {0,0,0,0,0,0,0,0,0},
                {0,1,0,0,0,0,0,0,0}};

        // act
        int[][] result = ExNineteen.getMaskMatrix(number);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMaskMatrixNumberTwo() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int number = 2;
        int[][] expected = {
                {0,1,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,1,0,0},
                {0,0,0,1,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,1,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0}};

        // act
        int[][] result = ExNineteen.getMaskMatrix(number);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void verifyIfEmptySpacesExistInArraySizeNineGridResultTrue() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};

        // act
        boolean result = ExNineteen.existEmptySpacesInArray(arraySudoku);

        // assert
        assertTrue(result);
    }

    @Test
    void verifyIfEmptySpacesExistInArraySizeThreeGridResultTrue() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};

        // act
        boolean result = ExNineteen.existEmptySpacesInArray(arraySudoku);

        // assert
        assertTrue(result);
    }

    @Test
    void verifyIfEmptySpacesExistInArraySizeThreeGridResultFalse() {
        // arrange
        int[][] arraySudoku = {
                {9,2,6},
                {8,5,1},
                {4,7,3},};

        // act
        boolean result = ExNineteen.existEmptySpacesInArray(arraySudoku);

        // assert
        assertFalse(result);
    }

    @Test
    void writeNumberInputNumberNineInEmptySpace() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int number = 9;
        int row = 0;
        int column = 0;
        int[][] expected = {
                {9,2,0},
                {8,5,1},
                {4,7,0},};

        // act
        int[][] result = ExNineteen.writeNumber(arraySudoku, number, row, column);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void writeNumberInputNumberEightInEmptySpaceSizeNineGrid() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int number = 8;
        int row = 8;
        int column = 8;
        int[][] expected = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,8}};

        // act
        int[][] result = ExNineteen.writeNumber(arraySudoku, number, row, column);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void writeNumberInputNumberNineInExistingBaseNumberResultSameArray() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int number = 9;
        int row = 0;
        int column = 1;
        int[][] expected = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};

        // act
        int[][] result = ExNineteen.writeNumber(arraySudoku, number, row, column);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void writeNumberInputNumberOneInInvalidSpaceResultSameArray() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int number = 1;
        int row = 0;
        int column = 0;
        int[][] expected = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};

        // act
        int[][] result = ExNineteen.writeNumber(arraySudoku, number, row, column);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void checkIfWritingNumberInRowIsValidResultFalse() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int number = 2;
        int row = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInRowValid(arraySudoku, number, row);

        // assert
        assertFalse(result);
    }

    @Test
    void checkIfWritingNumberInRowIsValidResultTrue() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int number = 1;
        int row = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInRowValid(arraySudoku, number, row);

        // assert
        assertTrue(result);
    }

    @Test
    void checkIfWritingNumberInRowIsValidSizeNineGridResultTrue() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int number = 4;
        int row = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInRowValid(arraySudoku, number, row);

        // assert
        assertTrue(result);
    }

    @Test
    void checkIfWritingNumberInColumnIsValidResultFalse() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int number = 8;
        int column = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInColumnValid(arraySudoku, number, column);

        // assert
        assertFalse(result);
    }

    @Test
    void checkIfWritingNumberInColumnIsValidResultTrue() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0},
                {8,5,1},
                {4,7,0},};
        int number = 1;
        int column = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInColumnValid(arraySudoku, number, column);

        // assert
        assertTrue(result);
    }

    @Test
    void checkIfWritingNumberInColumnIsValidSizeNineGridResultTrue() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int number = 1;
        int column = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInColumnValid(arraySudoku, number, column);

        // assert
        assertTrue(result);
    }

    @Test
    void checkIfWritingNumberInGridIsValidSizeNineGridResultFalse() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int number = 1;
        int row = 0;
        int column = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInGridValid(arraySudoku, number, row, column);

        // assert
        assertFalse(result);
    }

    @Test
    void checkIfWritingNumberInGridIsValidSizeNineGridResultTrue() {
        // arrange
        int[][] arraySudoku = {
                {0,2,0,0,1,7,0,3,0},
                {8,5,1,0,9,4,2,0,0},
                {4,7,0,2,6,0,9,0,0},
                {3,4,0,0,5,0,0,0,7},
                {0,0,8,0,0,2,1,0,3},
                {0,0,0,6,0,8,5,0,0},
                {0,0,4,0,0,9,6,1,0},
                {6,0,0,0,0,3,7,0,9},
                {0,1,0,0,4,6,0,5,0}};
        int number = 9;
        int row = 0;
        int column = 0;

        // act
        boolean result = ExNineteen.isWritingNumberInGridValid(arraySudoku, number, row, column);

        // assert
        assertTrue(result);
    }

}