import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExOneTest {

    @Test
    void getNumberDigitsInIntResultOne() {
        // arrange
        int number = 8;
        int expected = 1;

        // act
        int result = ExOne.getNumberDigitsInInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNumberDigitsInIntResultOneInputZero() {
        // arrange
        int number = 0;
        int expected = 1;

        // act
        int result = ExOne.getNumberDigitsInInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getNumberDigitsInIntResultFive() {
        // arrange
        int number = 12345;
        int expected = 5;

        // act
        int result = ExOne.getNumberDigitsInInt(number);

        // assert
        assertEquals(expected, result, 0.01);
    }
}