import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExThreeTest {

    @Test
    void getSumArrayResultZero() {
        // arrange
        int[] array = {0};
        int expected = 0;

        // act
        int result = ExThree.getSumArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumArrayResultOne() {
        // arrange
        int[] array = {1};
        int expected = 1;

        // act
        int result = ExThree.getSumArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumArrayResultTwentyFive() {
        // arrange
        int[] array = {3,6,7,8,1};
        int expected = 25;

        // act
        int result = ExThree.getSumArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumArrayResultSix() {
        // arrange
        int[] array = {1,2,3};
        int expected = 6;

        // act
        int result = ExThree.getSumArray(array);

        // assert
        assertEquals(expected, result, 0.01);
    }
}