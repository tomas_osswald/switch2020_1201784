import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExTwoTest {

    @Test
    void writeDigitsIntoArrayResultThreeSixSevenEightOne() {
        // arrange
        int number = 36781;
        int[] expected = {3,6,7,8,1};

        // act
        int[] result = ExTwo.writeDigitsIntoArray(number);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void writeDigitsIntoArrayResultOne() {
        // arrange
        int number = 1;
        int[] expected = {1};

        // act
        int[] result = ExTwo.writeDigitsIntoArray(number);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void writeDigitsIntoArrayResultOneTwoThree() {
        // arrange
        int number = 123;
        int[] expected = {1,2,3};

        // act
        int[] result = ExTwo.writeDigitsIntoArray(number);

        // assert
        assertArrayEquals(expected, result);
    }
}