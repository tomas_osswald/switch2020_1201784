import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExNineTest {

    @Test
    void isNumeralPalindromeResultTrueInputZero() {
        // arrange
        int number = 0;
        boolean expected = true;

        // act
        boolean result = ExNine.isNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isNumeralPalindromeResultTrueInputOne() {
        // arrange
        int number = 1;
        boolean expected = true;

        // act
        boolean result = ExNine.isNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isNumeralPalindromeResultTrueInputEleven() {
        // arrange
        int number = 11;
        boolean expected = true;

        // act
        boolean result = ExNine.isNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isNumeralPalindromeResultFalseInputTwelve() {
        // arrange
        int number = 12;
        boolean expected = false;

        // act
        boolean result = ExNine.isNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isNumeralPalindromeResultFalseInputOneHundred() {
        // arrange
        int number = 100;
        boolean expected = false;

        // act
        boolean result = ExNine.isNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isNumeralPalindromeResultTrueInputOneHundredOne() {
        // arrange
        int number = 101;
        boolean expected = true;

        // act
        boolean result = ExNine.isNumeralPalindrome(number);

        // assert
        assertEquals(expected, result);
    }
}