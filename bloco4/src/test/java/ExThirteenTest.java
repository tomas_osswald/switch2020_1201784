import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExThirteenTest {

    @Test
    void isMatrixSquareInputTwoLinesOneColumnResultFalse() {
        // arrange
        double[][] array = {{0},{0}};
        boolean expected = false;

        // act
        boolean result = ExThirteen.isDoublesMatrixSquare(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputTwoLinesTwoColumnsResultTrue() {
        // arrange
        double[][] array = {{0,1},{4,2}};
        boolean expected = true;

        // act
        boolean result = ExThirteen.isDoublesMatrixSquare(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputFourLinesFourColumnsResultTrue() {
        // arrange
        double[][] array = {{1,2,3,4},{10,21,31,3},{-1,-2,-3,-4},{4,3,2,1}};
        boolean expected = true;

        // act
        boolean result = ExThirteen.isDoublesMatrixSquare(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputDifferentLineLengthResultFalse() {
        // arrange
        double[][] array = {{0,1,2},{0,1},{2,1,2}};
        boolean expected = false;

        // act
        boolean result = ExThirteen.isDoublesMatrixSquare(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputMoreLinesThanColumnsResultFalse() {
        // arrange
        double[][] array = {{0,1},{0,1},{2,1},{2,6}};
        boolean expected = false;

        // act
        boolean result = ExThirteen.isDoublesMatrixSquare(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputMoreColumnsThanLinesResultFalse() {
        // arrange
        double[][] array = {{0,1,2,4},{0,1,2,1}};
        boolean expected = false;

        // act
        boolean result = ExThirteen.isDoublesMatrixSquare(array);

        // assert
        assertEquals(expected, result);
    }
}