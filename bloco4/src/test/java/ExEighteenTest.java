import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExEighteenTest {

    @Test
    void getMaskArraySizeTwoSquareFindA() {
        // arrange
        char[][] arrayLetters = {{'A','B'},{'C','D'}};
        char letter = 'A';
        int[][] expected = {{1,0},{0,0}};

        // act
        int[][] result = ExEighteen.getMaskMatrix(arrayLetters, letter);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMaskArraySizeTwoSquareFindLowerCaseA() {
        // arrange
        char[][] arrayLetters = {{'A','B'},{'C','D'}};
        char letter = 'a';
        int[][] expected = {{1,0},{0,0}};

        // act
        int[][] result = ExEighteen.getMaskMatrix(arrayLetters, letter);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMaskArraySizeSevenSquareFindA() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char letter = 'A';
        int[][] expected = {
                {0,0,0,0,0,0,0},
                {0,1,1,0,0,0,0},
                {0,1,0,0,0,0,0},
                {0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0},
                {0,1,0,0,0,0,0},
                {0,0,0,1,0,1,0}};

        // act
        int[][] result = ExEighteen.getMaskMatrix(arrayLetters, letter);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void isWordInMatrixInputJavaResultTrue() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'J','A','V','A'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertTrue(result);
    }

    @Test
    void isWordInMatrixInputStringResultTrue() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'S','T','R','I','N','G'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertTrue(result);
    }

    @Test
    void isWordInMatrixInputReturnResultTrue() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'R','E','T','U','R','N'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertTrue(result);
    }

    @Test
    void isWordInMatrixInputArrayResultTrue() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'A','R','R','A','Y'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertTrue(result);
    }

    @Test
    void isWordInMatrixInputDoubleResultTrue() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'D','O','U','B','L','E'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertTrue(result);
    }

    @Test
    void isWordInMatrixInputInteiroResultTrue() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'I','N','T','E','I','R','O'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertTrue(result);
    }

    @Test
    void isWordInMatrixInputSwitchResultFalse() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'S','W','I','T','C','H'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertFalse(result);
    }

    @Test
    void isWordInMatrixInputThrowResultFalse() {
        // arrange
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};
        char[] word = {'T','H','R','O','W'};

        // act
        boolean result = ExEighteen.isWordInMatrix(arrayLetters, word);

        // assert
        assertFalse(result);
    }
}