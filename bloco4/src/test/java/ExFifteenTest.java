import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExFifteenTest {

    @Test
    void getSmallestIntFromMatrixResultOne() {
        // arrange
        int[][] array = new int[][]{{1, 2, 3, 4, 5, 6}, {1, 3}};
        int expected = 1;

        // act
        int result = ExFifteen.getSmallestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getSmallestIntFromMatrixResultZero() {
        // arrange
        int[][] array = new int[][]{{1, 2, 3, 4, 5, 6}, {1, 3}, {0}};
        int expected = 0;

        // act
        int result = ExFifteen.getSmallestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getSmallestIntFromMatrixResultNegativeOne() {
        // arrange
        int[][] array = new int[][]{{1, 2, 3, -1, 5, 6}, {92, 12, 1}, {0, 0, 1}};
        int expected = -1;

        // act
        int result = ExFifteen.getSmallestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getSmallestIntFromMatrixResultTen() {
        // arrange
        int[][] array = new int[][]{{10, 20, 30, 40, 50, 60}, {10, 30}, {23, 23, 4215}, {2136}};
        int expected = 10;

        // act
        int result = ExFifteen.getSmallestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getLargestIntFromMatrixResultTen() {
        // arrange
        int[][] array = new int[][]{{10, 2, 3, 0, 5, 6}, {1, 3}, {2, 2, 4}, {2}};
        int expected = 10;

        // act
        int result = ExFifteen.getLargestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getLargestIntFromMatrixResultOne() {
        // arrange
        int[][] array = new int[][]{{0, 0, 1, 0, 0, 0}, {0, 0}, {0, 0, -4}, {0}};
        int expected = 1;

        // act
        int result = ExFifteen.getLargestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getLargestIntFromMatrixResultZero() {
        // arrange
        int[][] array = new int[][]{{-1, -2, 0, -30, -40}, {0}, {0, 0, -4}, {0}};
        int expected = 0;

        // act
        int result = ExFifteen.getLargestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getLargestIntFromMatrixResultNegativeOne() {
        // arrange
        int[][] array = new int[][]{{-1, -2, -30, -40}, {-3, -4}};
        int expected = -1;

        // act
        int result = ExFifteen.getLargestIntFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getAverageFromMatrixResultZero() {
        // arrange
        int[][] array = new int[][]{{-1, 0, 1, 2}, {-2}};
        double expected = 0.0D;

        // act
        double result = ExFifteen.getAverageFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getAverageFromMatrixResultNegativeOne() {
        // arrange
        int[][] array = new int[][]{{-1, 0, 1}, {-2, -3}};
        double expected = -1.0D;

        // act
        double result = ExFifteen.getAverageFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getAverageFromMatrixResultPointFive() {
        // arrange
        int[][] array = new int[][]{{-1, 0, 1}, {2, 1, 0}};
        double expected = 0.5D;

        // act
        double result = ExFifteen.getAverageFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getAverageFromMatrixResultFive() {
        // arrange
        int[][] array = new int[][]{{3, 6, 7}, {8, 1}};
        double expected = 5.0D;

        // act
        double result = ExFifteen.getAverageFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getProductFromMatrixResultZero() {
        // arrange
        int[][] array = new int[][]{{3, 6, 7}, {8, 1, 0}};
        double expected = 0.0D;

        // act
        double result = (double)ExFifteen.getProductFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getProductFromMatrixResultOne() {
        // arrange
        int[][] array = new int[][]{{1, 1, 1}, {1, 1, 1}};
        double expected = 1.0D;

        // act
        double result = (double)ExFifteen.getProductFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getProductFromMatrixResultOneThousandEight() {
        // arrange
        int[][] array = new int[][]{{3, 6, 7}, {8, 1}};
        double expected = 1008.0D;

        // act
        double result = (double)ExFifteen.getProductFromMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getNonRepeatedElementsFromMatrixResultOne() {
        // arrange
        int[][] array = new int[][]{{1, 0, 0}, {2, 2}, {3, 3}};
        int[] expected = new int[]{1};

        // act
        int[] result = ExFifteen.getNonRepeatedElementsFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getNonRepeatedElementsFromMatrixResultZeroOne() {
        // arrange
        int[][] array = new int[][]{{0}, {1}};
        int[] expected = new int[]{0, 1};

        // act
        int[] result = ExFifteen.getNonRepeatedElementsFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getNonRepeatedElementsFromMatrixResultOneTwoSixSeven() {
        // arrange
        int[][] array = new int[][]{{1, 2, 3}, {3, 5}, {5, 6, 7}};
        int[] expected = new int[]{1, 2, 6, 7};

        // act
        int[] result = ExFifteen.getNonRepeatedElementsFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getPrimeElementsFromMatrixResultThreeFiveSevenElevenThirteen() {
        // arrange
        int[][] array = {{3, 5, 7, 8}, {11, 14}, {13, 1}};
        int[] expected = {3,5,7,11,13};

        // act
        int[] result = ExFifteen.getPrimeElementsFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getPrimeElementsFromMatrixResultTwoThree() {
        // arrange
        int[][] array = {{1}, {2}, {3}};
        int[] expected = {2,3};

        // act
        int[] result = ExFifteen.getPrimeElementsFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getPrimeElementsFromMatrixResultTwo() {
        // arrange
        int[][] array = {{0}, {1}, {2}};
        int[] expected = {2};

        // act
        int[] result = ExFifteen.getPrimeElementsFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMainDiagonalFromMatrixInputArrayNotSquareResultNull() {
        // arrange
        double[][] array = {{0,1}, {1}};

        // act
        int[] result = ExFifteen.getMainDiagonalFromMatrix(array);

        // assert
        assertNull(result);
    }

    @Test
    void getMainDiagonalFromMatrixInputSquareResultOneTwoThree() {
        // arrange
        double[][] array = {{1,13,2},{1,2,5},{2,23,3}};
        int[] expected = {1,2,3};

        // act
        int[] result = ExFifteen.getMainDiagonalFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMainDiagonalFromMatrixInputHorizontalRectangleResultOneTwoThree() {
        // arrange
        double[][] array = {{1,13,2,0},{1,2,5,-2},{2,5,3,4}};
        int[] expected = {1,2,3};

        // act
        int[] result = ExFifteen.getMainDiagonalFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMainDiagonalFromMatrixInputVerticalRectangleResultOneTwoThree() {
        // arrange
        double[][] array = {{1,13,2},{1,2,5},{2,23,3},{0,0,0}};
        int[] expected = {1,2,3};

        // act
        int[] result = ExFifteen.getMainDiagonalFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getSecondaryDiagonalFromMatrixInputArrayNotSquareResultNull() {
        // arrange
        double[][] array = {{0,1}, {1}};

        // act
        int[] result = ExFifteen.getSecondaryDiagonalFromMatrix(array);

        // assert
        assertNull(result);
    }

    @Test
    void getSecondaryDiagonalFromMatrixInputSquareResultTwoTwoTwo() {
        // arrange
        double[][] array = {{1,13,2},{1,2,5},{2,23,3}};
        int[] expected = {2,2,2};

        // act
        int[] result = ExFifteen.getSecondaryDiagonalFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getSecondaryDiagonalFromMatrixInputHorizontalRectangleResultZeroFiveFive() {
        // arrange
        double[][] array = {{1,13,2,0},{1,2,5,-2},{2,5,3,4}};
        int[] expected = {0,5,5};

        // act
        int[] result = ExFifteen.getSecondaryDiagonalFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getSecondaryDiagonalFromMatrixInputVerticalRectangleResultTwoTwoTwo() {
        // arrange
        double[][] array = {{1,13,2},{1,2,5},{2,23,3},{0,0,0}};
        int[] expected = {2,2,2};

        // act
        int[] result = ExFifteen.getSecondaryDiagonalFromMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void isIdentityMatrixResultTrue() {
        // arrange
        double[][] array = {{1,0,0},{0,1,0},{0,0,1}};
        boolean expected = true;

        // act
        boolean result = ExFifteen.isIdentityMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isIdentityMatrixInputNotSquareResultFalse() {
        // arrange
        double[][] array = {{1,0,0,0},{0,1,0},{0,0,1}};
        boolean expected = false;

        // act
        boolean result = ExFifteen.isIdentityMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isIdentityMatrixInputOneOutOfPlaceResultFalse() {
        // arrange
        double[][] array = {{1,1,0},{0,1,0},{0,0,1}};
        boolean expected = false;

        // act
        boolean result = ExFifteen.isIdentityMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isIdentityMatrixInputDiagonalIsNotOnesResultFalse() {
        // arrange
        double[][] array = {{2,0,0},{0,1,0},{0,0,1}};
        boolean expected = false;

        // act
        boolean result = ExFifteen.isIdentityMatrix(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getInverseMatrixInputNotSquareResultNull() {
        // arrange
        double[][] array = {{2,0,0,0},{0,1,0},{0,0,1}};

        // act
        double[][] result = ExFifteen.getInverseMatrix(array);

        // assert
        assertNull(result);
    }

    @Test
    void getTransposedMatrixInputSquare() {
        // arrange
        double[][] array = {{1,2,3},{4,5,6},{7,8,9}};
        double[][] expected = {{1,4,7},{2,5,8},{3,6,9}};

        // act
        double[][] result = ExFifteen.getTransposedMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getTransposedMatrixInputVerticalRectangle() {
        // arrange
        double[][] array = {{1,2},{3,4},{5,6}};
        double[][] expected = {{1,3,5},{2,4,6}};

        // act
        double[][] result = ExFifteen.getTransposedMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getTransposedMatrixInputHorizontalRectangle() {
        // arrange
        double[][] array = {{1,2,3},{4,5,6}};
        double[][] expected = {{1,4},{2,5},{3,6}};

        // act
        double[][] result = ExFifteen.getTransposedMatrix(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfCofactorsSizeTwoSquare() {
        // arrange
        double[][] array = {{1,2},{3,4}};
        double[][] expected = {{1,-2},{3,-4}};

        // act
        double[][] result = ExFifteen.getMatrixOfCofactors(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfCofactorsSizeThreeSquare() {
        // arrange
        double[][] array = {{1,2,3},{4,5,6},{7,8,9}};
        double[][] expected = {{1,-2,3},{-4,5,-6},{7,-8,9}};

        // act
        double[][] result = ExFifteen.getMatrixOfCofactors(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfCofactorsRectangle() {
        // arrange
        double[][] array = {{1,2,3,4},{5,6,7,8}};
        double[][] expected = {{1,-2,3,-4},{5,-6,7,-8}};

        // act
        double[][] result = ExFifteen.getMatrixOfCofactors(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfCofactorsRectangleInputNegative() {
        // arrange
        double[][] array = {{-1,-2,-3,-4},{-5,-6,-7,-8}};
        double[][] expected = {{-1,2,-3,4},{-5,6,-7,8}};

        // act
        double[][] result = ExFifteen.getMatrixOfCofactors(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfMinorInputSizeTwoSquare() {
        // arrange
        double[][] array = {{2,3},{0,5}};
        double[][] expected = {{5,0},{3,2}};

        // act
        double[][] result = ExFifteen.getMatrixOfMinors(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfMinorInputSizeThreeSquare() {
        // arrange
        double[][] array = {{2,3,1},{0,5,6},{1,1,2}};
        double[][] expected = {{4,-6,-5},{5,3,-1},{13,12,10}};

        // act
        double[][] result = ExFifteen.getMatrixOfMinors(array);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfMinorInputSizeFourSquare() {
        // arrange
        double[][] array = {{2,3,1,2},{0,5,6,3},{1,1,2,6},{2,0,1,4}};
        double[][] expected = {{-11,39,34,3},{4,6,28,9},{53,24,38,36},{71,51,53,21}};

        // act
        double[][] result = ExFifteen.getMatrixOfMinors(array);

        // assert
        assertArrayEquals(expected, result);
    }
}