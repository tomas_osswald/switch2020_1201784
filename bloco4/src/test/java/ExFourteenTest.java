import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExFourteenTest {

    @Test
    void isMatrixRectangleInputTwoLinesOneColumnResultTrue() {
        // arrange
        double[][] array = {{4},{0}};
        boolean expected = true;

        // act
        boolean result = ExFourteen.isMatrixRectangle(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputTwoLinesTwoColumnsResultFalse() {
        // arrange
        double[][] array = {{0,1},{4,2}};
        boolean expected = false;

        // act
        boolean result = ExFourteen.isMatrixRectangle(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputFourLinesFourColumnsResultFalse() {
        // arrange
        double[][] array = {{1,2,3,4},{10,21,31,3},{-1,-2,-3,-4},{4,3,2,1}};
        boolean expected = false;

        // act
        boolean result = ExFourteen.isMatrixRectangle(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputDifferentLineLengthResultFalse() {
        // arrange
        double[][] array = {{0,1},{0,1},{2,1,2},{5,6}};
        boolean expected = false;

        // act
        boolean result = ExFourteen.isMatrixRectangle(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputMoreLinesThanColumnsResultTrue() {
        // arrange
        double[][] array = {{0,1},{0,1},{2,1},{2,6}};
        boolean expected = true;

        // act
        boolean result = ExFourteen.isMatrixRectangle(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void isMatrixSquareInputMoreColumnsThanLinesResultTrue() {
        // arrange
        double[][] array = {{0,1,2,4},{0,1,2,1}};
        boolean expected = true;

        // act
        boolean result = ExFourteen.isMatrixRectangle(array);

        // assert
        assertEquals(expected, result);
    }
}