import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExSixteenTest {

    @Test
    void getDeterminantOfMatrixWithLaplaceExpansionInputNotSquareResultZero() {
        // arrange
        double[][] array = {{2,0,0,0},{0,1,0},{0,0,1}};
        double expected = 0;

        // act
        double result = ExSixteen.getDeterminantOfMatrixWithLaplaceExpansion(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getDeterminantOfMatrixWithLaplaceExpansionResultZero() {
        // arrange
        double[][] array = {{1,2,3},{4,5,6},{7,8,9}};
        double expected = 0;

        // act
        double result = ExSixteen.getDeterminantOfMatrixWithLaplaceExpansion(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getDeterminantOfMatrixWithLaplaceExpansionResultNegativeTwentyFour() {
        // arrange
        double[][] array = {{9,2,3},{4,5,6},{7,8,9}};
        double expected = -24;

        // act
        double result = ExSixteen.getDeterminantOfMatrixWithLaplaceExpansion(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void getDeterminantOfMatrixWithLaplaceExpansionInputSizeFourSquareResult() {
        // arrange
        double[][] array = {{2,3,1,2},{0,5,6,4},{1,1,2,6},{3,6,8,2}};
        double expected = -300;

        // act
        double result = ExSixteen.getDeterminantOfMatrixWithLaplaceExpansion(array);

        // assert
        assertEquals(expected, result);
    }

    @Test
    void removeLineAndColumnFromSquareMatrixInputOutOfBoundLineResultNull() {
        // arrange
        double[][] array = {{2,0,0},{0,1,0},{0,0,1}};
        int line = 20;
        int column = 0;

        // act
        double[][] result = ExSixteen.removeLineAndColumnFromSquareMatrix(array, line, column);

        // assert
        assertNull(result);
    }

    @Test
    void removeLineAndColumnFromSquareMatrixInputOutOfBoundColumnResultNull() {
        // arrange
        double[][] array = {{2,0,0},{0,1,0},{0,0,1}};
        int line = 0;
        int column = 20;

        // act
        double[][] result = ExSixteen.removeLineAndColumnFromSquareMatrix(array, line, column);

        // assert
        assertNull(result);
    }

    @Test
    void removeLineAndColumnFromSquareMatrixInputNotSquareResultNull() {
        // arrange
        double[][] array = {{2,0,0,0},{0,1,0},{0,0,1}};
        int line = 0;
        int column = 0;

        // act
        double[][] result = ExSixteen.removeLineAndColumnFromSquareMatrix(array, line, column);

        // assert
        assertNull(result);
    }

    @Test
    void removeLineAndColumnFromSquareMatrixResultOneZeroAndZeroOne() {
        // arrange
        double[][] array = {{2,0,0,},{0,1,0},{0,0,1}};
        int line = 0;
        int column = 0;
        double[][] expected = {{1,0},{0,1}};

        // act
        double[][] result = ExSixteen.removeLineAndColumnFromSquareMatrix(array, line, column);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void removeLineAndColumnFromSquareMatrixInputSizeFourSquare() {
        // arrange
        double[][] array = {{2,0,0,2},{2,0,1,0},{2,0,0,1},{4,2,1,9}};
        int line = 0;
        int column = 0;
        double[][] expected = {{0,1,0},{0,0,1},{2,1,9}};

        // act
        double[][] result = ExSixteen.removeLineAndColumnFromSquareMatrix(array, line, column);

        // assert
        assertArrayEquals(expected, result);
    }
}