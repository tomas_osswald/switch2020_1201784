import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExElevenTest {

    @Test
    void getDotProductFromTwoArraysResultOne() {
        // arrange
        int[] arrayOne = {1};
        int[] arrayTwo = {1};
        int expected = 1;

        // act
        int result = ExEleven.getDotProductFromTwoArrays(arrayOne, arrayTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getDotProductFromTwoArraysResultZero() {
        // arrange
        int[] arrayOne = {1};
        int[] arrayTwo = {0};
        int expected = 0;

        // act
        int result = ExEleven.getDotProductFromTwoArrays(arrayOne, arrayTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getDotProductFromTwoArraysResultThirtyTwo() {
        // arrange
        int[] arrayOne = {1,2,3};
        int[] arrayTwo = {4,5,6};
        int expected = 32;

        // act
        int result = ExEleven.getDotProductFromTwoArrays(arrayOne, arrayTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getDotProductFromTwoArraysResultTwentyEight() {
        // arrange
        int[] arrayOne = {3,6,7,8,1};
        int[] arrayTwo = {3,2,1};
        int expected = 28;

        // act
        int result = ExEleven.getDotProductFromTwoArrays(arrayOne, arrayTwo);

        // assert
        assertEquals(expected, result, 0.01);
    }
}