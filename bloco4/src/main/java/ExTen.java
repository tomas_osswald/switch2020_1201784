public class ExTen {

    // Método que retorna menor elemento de um array
    public static int getSmallestIntFromArray(int[] array) {
        int smallest = array[0];

        for (int i = 0; i < array.length; i++) {
            if (smallest > array[i]) {
                smallest = array[i];
            }
        }

        return smallest;
    }

    // Método que retorna maior elemento de um array
    public static int getLargestIntFromArray(int[] array) {
        int largest = array[0];

        for (int i = 0; i < array.length; i++) {
            if (largest < array[i]) {
                largest = array[i];
            }
        }

        return largest;
    }

    //Método que retorna a média dos elementos de um array
    public static double getAverageFromArray(int[] array) {
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }

        return sum/(double)array.length;
    }

    // Método que retorna o produto dos elementos de um array
    public static int getProductFromArray(int[] array) {
        int product = 1;

        for (int i = 0; i < array.length; i++) {
            product *= array[i];
        }

        return product;
    }

    // Método que retorna um array com os elementos não repetidos de um array dado
    public static int[] getNonRepeatedElementsFromArray(int[] array) {
        int[] arrayNonRepeatedElements = new int[array.length];

        int countNonRepeatedElements = 0;

        // Para cada elemento do array
        for (int i = 0; i < array.length; i++) {

            int tempCounter = 0;

            // Verificar se idêntico a outro elemento do array. Se sim, aumentar tempCounter.
            for (int j = 0; j < array.length; j++) {
                if (array[i]==array[j]) {
                    tempCounter++;
                }
            }

            // Para não ser repetido, elemento só deve ser igual a ele próprio, logo tempCounter terá o valor 1
            if (tempCounter==1) {
                arrayNonRepeatedElements[countNonRepeatedElements] = array[i];
                countNonRepeatedElements++;
            }
        }

        // Truncar array pelo tamanho igual à quantidade de elementos não repetidos
        return ExSix.truncateArray(arrayNonRepeatedElements, countNonRepeatedElements);
    }

    // Método que retorna um array com os elementos de um array dado invertido
    public static int[] getReversedElementsFromArray(int[] array) {
        int[] reversedArray = new int[array.length];

        // Começando no início do array em crescente, copiar cada elemento para o fim do novo array, em decrescente
        for (int i = 0, j = array.length - 1; i < array.length; i++, j--) {
            reversedArray[j] = array[i];
        }

        return reversedArray;
    }

    // Método que retorna um array com os elementos primos de um array dado
    public static int[] getPrimeElementsFromArray(int[] array) {
        int[] arrayPrimes = new int[array.length];

        int primeCounter = 0;

        // Para cada elemento do array
        for (int i = 0; i < array.length; i++) {

            int tempCounter = 0;

            // Se o resto da divisão do elemento no array por um número entre 2 e este for 0, o elemento não e primo
            for (int j = 2; j < array[i]; j++) {
                if ( (array[i]%j==0) ) {
                    tempCounter++;
                }
            }

            // Se o tempCounter for maior que 0, número não é primo. Se for igual a zero, número é primo e escrito no novo array.
            if (tempCounter==0 && array[i] != 0 && array[i] != 1) {
                arrayPrimes[primeCounter] = array[i];
                primeCounter++;
            }
        }

        // Truncar novo array pela quantidade de números primos
        return ExSix.truncateArray(arrayPrimes, primeCounter);
    }
}
