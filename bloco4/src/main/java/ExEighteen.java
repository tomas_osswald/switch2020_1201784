import java.util.Scanner;

public class ExEighteen {

    public static void main(String[] args) {
        String word, wordOne, wordTwo;
        char[][] arrayLetters = {
                {'E','E','D','N','N','S','E'},
                {'L','A','A','R','R','T','V'},
                {'B','A','L','U','J','R','E'},
                {'U','R','N','T','B','I','E'},
                {'O','R','I','E','T','N','I'},
                {'D','A','G','R','T','G','L'},
                {'J','Y','J','A','V','A','T'}};

        printLetterMatrix(arrayLetters);

        Scanner ler = new Scanner(System.in);

        System.out.println("1 - Introduza uma palavra a procurar");
        System.out.println("2 - Determinar se uma palavra existe através das suas coordenadas de início e fim");
        System.out.println("3 - Determinar se duas palavras têm células em comum");
        int input = ler.nextInt();

        switch (input) {
            case 1:
                System.out.println("Introduza a palavra a procurar");
                word = ler.next();
                char[] arrayWord = getUpperCaseArrayFromString(word);
                System.out.println(isWordInMatrix(arrayLetters, arrayWord));
                break;
            case 2:
                System.out.println("Introduza a palavra a procurar");
                word = ler.next();
                arrayWord = getUpperCaseArrayFromString(word);

                System.out.println("Introduza a linha da coordenada de início");
                int rowBegin = ler.nextInt();

                System.out.println("Introduza a coluna da coordenada de início");
                int columnBegin = ler.nextInt();

                System.out.println("Introduza a linha da coordenada de fim");
                int rowEnd = ler.nextInt();

                System.out.println("Introduza a coluna da coordenada de fim");
                int columnEnd = ler.nextInt();

                System.out.println(checkWordFromBeginPointToEndPointInMatrix(arrayWord, arrayLetters, rowBegin, columnBegin, rowEnd, columnEnd));
                break;
            /*case 3:
                System.out.println("Introduza a primeira palavra a procurar");
                wordOne = ler.next();
                char[] arrayWordOne = getUpperCaseArrayFromString(wordOne);
                System.out.println("Introduza a segunda palavra a procurar");
                wordTwo = ler.next();
                char[] arrayWordTwo = getUpperCaseArrayFromString(wordTwo);
                System.out.println(hasCommonCells(arrayLetters, arrayWordOne, arrayWordTwo));
                break; */
            default:
                break;
        }
    }

    /**
     * Método que imprime a matriz de letras na linha de comandos
     * @param arrayLetters array de letras
     */
    public static void printLetterMatrix(char[][] arrayLetters) {

        System.out.print("     ");
        for (int p = 0; p < arrayLetters.length; p++) {
            System.out.print(" " + p + " ");
        }
        System.out.println();

        System.out.print("     ");
        for (int p = 0; p < arrayLetters.length; p++) {
            System.out.print("---");
        }
        System.out.println();

        for (int i = 0; i < arrayLetters.length; i++) {
            System.out.print(" " + i + " | ");
            for (int j = 0; j < arrayLetters[i].length; j++) {
                if (arrayLetters[i][j] == 0) {
                    System.out.print("   ");
                } else {
                    System.out.print(" " + arrayLetters[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    /**
     * Método que retorna a matriz máscara de uma dada matriz de letras e uma letra
     *
     * @param arrayLetters matriz de letras
     * @param letter       letra a procurar
     * @return matriz máscara
     */
    public static int[][] getMaskMatrix(char[][] arrayLetters, char letter) {

        int[][] maskArray = new int[arrayLetters.length][arrayLetters.length];

        for (int i = 0; i < arrayLetters.length; i++) {
            for (int j = 0; j < arrayLetters[i].length; j++) {

                // Verificar se caracter da matriz é lower ou upper case
                if (Character.isUpperCase(arrayLetters[i][j])) {
                    if (arrayLetters[i][j] == Character.toUpperCase(letter)) {
                        maskArray[i][j] = 1;
                    }
                } else {
                    if (arrayLetters[i][j] == Character.toLowerCase(letter)) {
                        maskArray[i][j] = 1;
                    }
                }
            }
        }

        return maskArray;

    }

    /**
     * Método que passa uma palavra numa String para um array em maiúsculas
     * @param word palavra em String
     * @return palavra em array
     */
    public static char[] getUpperCaseArrayFromString(String word) {
        char[] arrayWord = new char[word.length()];

        for (int i = 0; i < arrayWord.length; i++) {
            arrayWord[i] = Character.toUpperCase(word.charAt(i));
        }

        return arrayWord;
    }

    /**
     * Método que procura se uma palavra existe numa matriz de letras
     * @param arrayLetters matriz de letras
     * @param word palavra em array
     * @return resultado
     */
    public static boolean isWordInMatrix(char[][] arrayLetters, char[] word) {
        boolean foundWord = false;

        for (int i = 0; i < arrayLetters.length && !foundWord; i++) {
            for (int j = 0; j < arrayLetters[i].length && !foundWord; j++) {
                if (getMaskMatrix(arrayLetters, word[0])[i][j] == 1) {
                    if (checkWordFromPointInMatrix(word, arrayLetters, i, j)) {
                        foundWord = true;
                    }
                }
            }
        }

        return foundWord;
    }

    /**
     * Método que procura uma palavra numa matriz de letras a partir de um ponto dessa matriz
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordFromPointInMatrix (char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;

        if (checkWordLeftRight(word,arrayLetters,row,column) ||
            checkWordRightLeft(word,arrayLetters,row,column) ||
            checkWordUpDown(word,arrayLetters,row,column) ||
            checkWordDownUp(word,arrayLetters,row,column) ||
            checkWordDiagonalDownRight(word,arrayLetters,row,column) ||
            checkWordDiagonalDownLeft(word,arrayLetters,row,column) ||
            checkWordDiagonalUpRight(word,arrayLetters,row,column) ||
            checkWordDiagonalUpLeft(word,arrayLetters,row,column)) {
            foundWord = true;
        }

        return foundWord;
    }

    /**
     * Método que verifica se uma palavra existe numa matriz, entre as coordenadas dadas
     * @param word palavra a verificar
     * @param arrayLetters matriz de letras
     * @param rowBegin linha da coordenada de início
     * @param columnBegin coluna da coordenada de início
     * @param rowEnd linha da coordenada de fim
     * @param columnEnd coluna da coordenada de fim
     * @return true = palavra existe nessas coordenadas, false = palavra NÃO existe nessas coordenadas
     */
    public static boolean checkWordFromBeginPointToEndPointInMatrix (char[] word, char[][] arrayLetters, int rowBegin, int columnBegin, int rowEnd, int columnEnd) {
        boolean foundWord = false;

        if (rowBegin == rowEnd && columnBegin < columnEnd) {
            foundWord = checkWordLeftRight(word, arrayLetters, rowBegin, columnBegin);
        } else if (rowBegin == rowEnd && columnBegin > columnEnd) {
            foundWord = checkWordRightLeft(word, arrayLetters, rowBegin, columnBegin);
        } else if (rowBegin < rowEnd && columnBegin == columnEnd) {
            foundWord = checkWordUpDown(word, arrayLetters, rowBegin, columnBegin);
        } else if (rowBegin > rowEnd && columnBegin == columnEnd) {
            foundWord = checkWordDownUp(word, arrayLetters, rowBegin, columnBegin);
        } else if (rowBegin < rowEnd && columnBegin < columnEnd) {
            foundWord = checkWordDiagonalDownRight(word, arrayLetters, rowBegin, columnBegin);
        } else if (rowBegin < rowEnd && columnBegin > columnEnd) {
            foundWord = checkWordDiagonalDownLeft(word, arrayLetters, rowBegin, columnBegin);
        } else if (rowBegin > rowEnd && columnBegin < columnEnd) {
            foundWord = checkWordDiagonalUpRight(word, arrayLetters, rowBegin,columnBegin);
        } else {
            foundWord = checkWordDiagonalUpLeft(word, arrayLetters, rowBegin, columnBegin);
        }

        return foundWord;
    }

    /**
     * Método que procura uma palavra da esquerda para a direita a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordLeftRight(char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = column; (k < word.length + column && k < arrayLetters.length) && flag; k++) {

            flag = false;

            if (word[wordLetter]==arrayLetters[row][k]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /**
     * Método que procura uma palavra da direita para a esquerda a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordRightLeft (char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = column; (k > column - word.length && k >= 0) && flag; k--) {

            flag = false;

            if (word[wordLetter]==arrayLetters[row][k]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /**
     * Método que procura uma palavra de cima para baixo a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordUpDown (char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = row; (k < word.length + row && k < arrayLetters.length) && flag; k++) {

            flag = false;

            if (word[wordLetter]==arrayLetters[k][column]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /**
     * Método que procura uma palavra de baixo para cima a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordDownUp (char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = row; (k > row - word.length && k >= 0) && flag; k--) {

            flag = false;

            if (word[wordLetter]==arrayLetters[k][column]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /**
     * Método que procura uma palavra na diagonal para baixo/direita a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordDiagonalDownRight(char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = row, p = column; (k < word.length + row && p < word.length + column) && (k < arrayLetters.length && p < arrayLetters.length) && flag; k++, p++) {

            flag = false;

            if (word[wordLetter]==arrayLetters[k][p]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /**
     * Método que procura uma palavra na diagonal para baixo/esquerda a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordDiagonalDownLeft(char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = row, p = column; (k < word.length + row && p > column - word.length) && (k < arrayLetters.length && p >= 0) && flag; k++, p--) {

            flag = false;

            if (word[wordLetter]==arrayLetters[k][p]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /**
     * Método que procura uma palavra na diagonal para cima/direita a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordDiagonalUpRight(char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = row, p = column; (k > row - word.length && p < word.length + column) && (k >= 0 && p < arrayLetters.length) && flag; k--, p++) {

            flag = false;

            if (word[wordLetter]==arrayLetters[k][p]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /**
     * Método que procura uma palavra na diagonal para cima/esquerda a partir de um ponto numa matriz de letras
     * @param word palavra em array
     * @param arrayLetters matriz de letras
     * @param row linha do ponto de partida
     * @param column coluna do ponto de partida
     * @return resultado
     */
    public static boolean checkWordDiagonalUpLeft(char[] word, char[][] arrayLetters, int row, int column) {
        boolean foundWord = false;
        int wordLetter = 0;
        boolean flag = true;

        for (int k = row, p = column; (k > row - word.length && p > column - word.length) && (k >= 0 && p >= 0) && flag; k--, p--) {

            flag = false;

            if (word[wordLetter]==arrayLetters[k][p]) {
                wordLetter++;
                flag = wordLetter != word.length;
            }
        }

        if (wordLetter==word.length) {
            foundWord = true;
        }
        return foundWord;
    }

    /*
     * Método que retorna se duas palavras têm células em comum na matriz da sopa de letras
     * @param arrayLetters matriz de letras
     * @param wordOne array da primeira palavra
     * @param wordTwo array da segunda palavra
     * @return true = têm células em comum, false = não têm células em comum
     */
    /*
    public static boolean hasCommonCells(char[][] arrayLetters, char[] wordOne, char[] wordTwo) {
        boolean commonCells = false;

        if (isWordInMatrix(arrayLetters, wordOne) && isWordInMatrix(arrayLetters, wordTwo)) {

            int[][] maskMatrixWordOne = getMaskMatrixFromWord(arrayLetters, wordOne);
            int[][] maskMatrixWordTwo = getMaskMatrixFromWord(arrayLetters, wordTwo);

            int[][] sumMaskMatrices = getSumSquareMatrices(maskMatrixWordOne, maskMatrixWordTwo);

            for (int i = 0; i < sumMaskMatrices.length; i++) {
                for (int j = 0; j < sumMaskMatrices[i].length; j++) {
                    if (sumMaskMatrices[i][j]==2) {
                        commonCells = true;
                    }
                }
            }
        }

        return commonCells;
    }*/

    /**
     * Método que retorna a soma de duas matrizes quadradas
     * @param matrixOne primeira matriz
     * @param matrixTwo segunda matriz
     * @return soma das duas matrizes
     */
    public static int[][] getSumSquareMatrices(int[][] matrixOne, int[][] matrixTwo) {
        int[][] sumMatrix = new int[matrixOne.length][matrixOne.length];

        for (int i = 0; i < sumMatrix.length; i++) {
            for (int j = 0; j < sumMatrix[i].length; j++) {
                sumMatrix[i][j] = matrixOne[i][j] + matrixTwo[i][j];
            }
        }

        return sumMatrix;
    }
}
