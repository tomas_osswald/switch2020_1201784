public class ExFour {

    // Método que retorna um vector cujos elementos são os elementos pares de um vector dado
    public static int[] getArrayEvensFromArray(int[] array) {
        int evens = getEvensFromArray(array);

        if (evens == -1) {
            return null;
        }

        int[] arrayEvens = new int[ExOne.getNumberDigitsInInt(evens)];
        arrayEvens = ExTwo.writeDigitsIntoArray(evens);

        return arrayEvens;
    }

    // Método que retorna um vector cujos elementos são os elementos impares de um vector dado
    public static int[] getArrayOddsFromArray(int[] array) {
        int odds = getOddsFromArray(array);

        if (odds == 0) {
            return null;
        }

        int[] arrayOdds = new int[ExOne.getNumberDigitsInInt(odds)];
        arrayOdds = ExTwo.writeDigitsIntoArray(odds);

        return arrayOdds;
    }

    // Método que retorna se um número é par ou não
    public static boolean isEven(int number) {
        boolean isEven = false;

        if ( (number % 2) == 0 ) {
            isEven = true;
        }

        return isEven;
    }

    // Método que retorna um inteiro com os elementos pares de um vector dado
    public static int getEvensFromArray(int[] array) {
        int digit;
        int evens=0;
        int evensCounter=0;

        for (int i = 0; i < array.length; i++) {
            if (isEven(array[i])) {
                evens = evens*10 + array[i];
                evensCounter++;
            }
        }

        if (evensCounter == 0) {
            return -1;
        }

        return evens;
    }

    // Método que retorna um inteiro com os elementos impares de um vector dado
    public static int getOddsFromArray(int[] array) {
        int digit;
        int odds=0;

        for (int i = 0; i < array.length; i++) {
            if (!isEven(array[i])) {
                odds = odds*10 + array[i];
            }
        }

        return odds;
    }
}
