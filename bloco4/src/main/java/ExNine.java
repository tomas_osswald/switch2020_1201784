public class ExNine {

    // Método que verifica se um número é capicua através de um array
    public static boolean isNumeralPalindrome(int number) {
        boolean isPalindrome = false;

        // Escrever os dígitos do número num array
        int[] array = ExTwo.writeDigitsIntoArray(number);

        int counter = 0;

        // Começando na primeira e última posição do array, verificar se os números são idênticos (aumentar contador)
        for (int i = 0, j = array.length - 1; i < array.length; i++, j--) {
            if (array[i] == array[j])
                counter++;
        }

        // Se o contador for idêntico o comprimento do array, todos os numeros serão idênticos
        if (counter == array.length) {
            isPalindrome = true;
        }

        return isPalindrome;
    }
}
