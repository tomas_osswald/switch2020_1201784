import java.util.Scanner;

public class ExNineteen {

    static final int[][] BASE_SUDOKU = {
            {0, 2, 0, 0, 1, 7, 0, 3, 0},
            {8, 5, 1, 0, 9, 4, 2, 0, 0},
            {4, 7, 0, 2, 6, 0, 9, 0, 0},
            {3, 4, 0, 0, 5, 0, 0, 0, 7},
            {0, 0, 8, 0, 0, 2, 1, 0, 3},
            {0, 0, 0, 6, 0, 8, 5, 0, 0},
            {0, 0, 4, 0, 0, 9, 6, 1, 0},
            {6, 0, 0, 0, 0, 3, 7, 0, 9},
            {0, 1, 0, 0, 4, 6, 0, 5, 0}};

    /*
        Dificuldade baixa = {
            {0,2,0,0,1,7,0,3,0},
            {8,5,1,0,9,4,2,0,0},
            {4,7,0,2,6,0,9,0,0},
            {3,4,0,0,5,0,0,0,7},
            {0,0,8,0,0,2,1,0,3},
            {0,0,0,6,0,8,5,0,0},
            {0,0,4,0,0,9,6,1,0},
            {6,0,0,0,0,3,7,0,9},
            {0,1,0,0,4,6,0,5,0}};

        Dificuldade média = {
            {0,8,0,9,0,0,0,5,0},
            {4,0,0,6,5,0,9,1,3},
            {0,0,5,4,0,0,0,0,0},
            {1,0,2,0,6,5,0,0,0},
            {7,0,4,0,0,0,5,0,8},
            {0,0,0,0,0,0,0,0,2},
            {3,0,0,0,0,1,0,0,6},
            {8,7,0,0,0,0,0,4,0},
            {0,5,0,0,4,0,8,0,1}};

        Dificuldade alta = {
            {6,0,0,0,0,0,9,0,4},
            {0,3,0,5,0,0,0,6,0},
            {0,0,9,7,0,0,0,0,0},
            {0,0,3,0,0,0,1,0,0},
            {0,0,0,3,0,4,0,2,0},
            {0,2,0,0,0,0,5,4,0},
            {0,9,0,0,0,0,0,0,0},
            {0,0,0,9,0,1,7,0,0},
            {0,0,8,0,7,2,3,1,0}};
     */

    public static void main(String[] args) {
        int[][] playedSudoku = createCopyOfSquareMatrix(BASE_SUDOKU);

        do {
            printSudoku(playedSudoku);
            int number = readNumberBetweenZeroAndNine();
            int row = readRowBetweenZeroAndEight();
            int column = readColumnBetweenZeroAndEight();
            playedSudoku = writeNumber(playedSudoku, number, row, column);
        } while (existEmptySpacesInArray(playedSudoku));

        printSudoku(playedSudoku);
        System.out.println("Parabéns, terminou o jogo");
    }

    /**
     * Método que lê e retorna um número a jogar entre 0 e 9 inserido pelo utilizador
     *
     * @return número inserido
     */
    public static int readNumberBetweenZeroAndNine() {
        Scanner ler = new Scanner(System.in);
        int number = -1;

        do {
            System.out.println("Introduza o número entre 1 a 9 escrever (ou 0 para apagar um número)");
            number = ler.nextInt();
        } while (number < 0 || number > 9);

        return number;
    }

    /**
     * Método que lê e retorna a linha inserida pelo utilizador entre 0 e 8
     *
     * @return linha inserida
     */
    public static int readRowBetweenZeroAndEight() {
        Scanner ler = new Scanner(System.in);
        int row = -1;

        do {
            System.out.println("Introduza a linha entre 1 a 9");
            row = ler.nextInt() - 1;
        } while (row < 0 || row >= BASE_SUDOKU.length);

        return row;
    }

    /**
     * Método que lê e retorna a coluna inserida pelo utilizador entre 0 e 8
     *
     * @return coluna inserida
     */
    public static int readColumnBetweenZeroAndEight() {
        Scanner ler = new Scanner(System.in);
        int column = -1;

        do {
            System.out.println("Introduza a coluna entre 1 a 9");
            column = ler.nextInt() - 1;
        } while (column < 0 || column >= BASE_SUDOKU.length);

        return column;
    }

    /**
     * Método que cria uma cópia de uma matriz quadrada existente
     *
     * @param array matriz quadrada
     * @return cópia da matriz quadrada
     */
    public static int[][] createCopyOfSquareMatrix(int[][] array) {

        if (!ExThirteen.isIntsMatrixSquare(array)) {
            return null;
        }

        int[][] newArray = new int[array.length][array.length];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                newArray[i][j] = array[i][j];
            }
        }

        return newArray;
    }

    /**
     * Método que imprime a matriz do Sudoku na linha de comandos
     *
     * @param playedSudoku matriz do Sudoku
     */
    public static void printSudoku(int[][] playedSudoku) {

        System.out.println("      1  2  3   4  5  6   7  8  9");
        System.out.println("     -----------------------------");
        for (int i = 0; i < playedSudoku.length; i++) {
            System.out.print(" " + (i + 1) + " | ");
            for (int j = 0; j < playedSudoku[i].length; j++) {
                if (playedSudoku[i][j] == 0) {
                    System.out.print("   ");
                } else {
                    System.out.print(" " + playedSudoku[i][j] + " ");
                }
                if (j == 2 || j == 5) {
                    System.out.print("|");
                }
            }
            System.out.println();
            if (i == 2 || i == 5) {
                System.out.println("   | -----------------------------");
            }
        }
    }

    /**
     * Método que retorna uma matriz máscara de zeros e uns a partir de uma matriz de números e um dado número, com os uns no lugar do número inserido
     *
     * @param number número a procurar e substituir por uns
     * @return matriz máscara
     */
    public static int[][] getMaskMatrix(int number) {

        int[][] maskArray = new int[BASE_SUDOKU.length][BASE_SUDOKU.length];

        for (int i = 0; i < BASE_SUDOKU.length; i++) {
            for (int j = 0; j < BASE_SUDOKU[i].length; j++) {
                if (BASE_SUDOKU[i][j] == number) {
                    maskArray[i][j] = 1;
                }
            }
        }

        return maskArray;
    }

    /**
     * Método que verifica se existem espaços por preencher no Sudoku
     *
     * @param arraySudoku matriz do sudoku
     * @return true = existem espaços por preencher, false = Sudoku está completo
     */
    public static boolean existEmptySpacesInArray(int[][] arraySudoku) {
        boolean emptySpaces = false;

        for (int i = 0; i < arraySudoku.length && !emptySpaces; i++) {
            for (int j = 0; j < arraySudoku[i].length && !emptySpaces; j++) {
                if (arraySudoku[i][j] == 0) {
                    emptySpaces = true;
                }
            }
        }

        return emptySpaces;
    }

    /**
     * Método que escreve um número dado na matriz do Sudoku. Se tentar escrever numa posição preenchida no Sudoku base, devolve igual
     *
     * @param playedSudoku matriz do sudoku
     * @param number       número a escrever
     * @param row          linha do número a escrever
     * @param column       coluna do número a escrever
     * @return matriz do sudoku alterada
     */
    public static int[][] writeNumber(int[][] playedSudoku, int number, int row, int column) {

        if (BASE_SUDOKU[row][column] == 0) {
            if (number == 0) {
                playedSudoku[row][column] = number;
            }
            if (isWritingNumberInRowValid(playedSudoku, number, row) &&
                    isWritingNumberInColumnValid(playedSudoku, number, column) &&
                    isWritingNumberInGridValid(playedSudoku, number, row, column)) {
                playedSudoku[row][column] = number;
            } else {
                System.out.println("Não pode repetir um número na mesma linha, coluna ou quadrado 3x3");
            }
        } else {
            System.out.println("Não pode alterar esse número");
        }

        return playedSudoku;


    }

    /**
     * Método que retorna se é válido escrever um número numa determinada linha, verificando se este não é repetido
     *
     * @param playedSudoku matriz do sudoku
     * @param number       número a escrever
     * @param row          linha a verificar
     * @return true = válido, número NÃO repetido; false = inválido, número repetido
     */
    public static boolean isWritingNumberInRowValid(int[][] playedSudoku, int number, int row) {
        boolean isValid = true;

        for (int i = 0; i < playedSudoku.length && isValid; i++) {
            if (playedSudoku[row][i] == number) {
                isValid = false;
            }
        }

        return isValid;
    }

    /**
     * Método que retorna se é válido escever um número numa determinada coluna, verificando se este não é repetido
     *
     * @param playedSudoku matriz do sudoku
     * @param number       número a escrever
     * @param column       coluna a verificar
     * @return true = válido, número NÃO repetido, false = inválido, número repetido
     */
    public static boolean isWritingNumberInColumnValid(int[][] playedSudoku, int number, int column) {
        boolean isValid = true;

        for (int i = 0; i < playedSudoku.length && isValid; i++) {
            if (playedSudoku[i][column] == number) {
                isValid = false;
            }
        }

        return isValid;
    }

    /**
     * Método que retorna se é válido escever um número numa determinada grelha 3x3, verificando se este não é repetido
     *
     * @param playedSudoku matriz do sudoku
     * @param number       número a escrever
     * @param row          linha para identificar grelha verificar
     * @param column       coluna para identificar grelha
     * @return true = válido, número NÃO repetido, false = inválido, número repetido
     */
    public static boolean isWritingNumberInGridValid(int[][] playedSudoku, int number, int row, int column) {
        boolean isValid = true;


        int rowMinimum = (row / 3) * 3;
        int columnMinimum = (column / 3) * 3;

        for (int i = rowMinimum; i <= rowMinimum + 3 && isValid; i++) {
            for (int j = columnMinimum; j <= columnMinimum + 3 && isValid; j++) {
                if (playedSudoku[i][j] == number) {
                    isValid = false;
                }
            }
        }

        return isValid;
    }
}

