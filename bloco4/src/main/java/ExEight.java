public class ExEight {

    // Método para contar os múltiplos comuns dos elementos dum array
    public static int countMultiplesFromArrayInInterval(int[] array, int intervalBegin, int intervalEnd) {
        int countMultiples = 0;

        for (int i = intervalBegin; i <= intervalEnd; i++) {

            int tempCounter = 0;

            for (int j = 0; j < array.length; j++) {
                if ( ExSeven.isNumberOneMultipleOfNumberTwo(i, array[j]) ) {
                    tempCounter++;
                }
            }
            if ( tempCounter == array.length ) {
                countMultiples++;
            }

        }
        return countMultiples;
    }

    // Método que retorna um array com os múltiplos comuns dos elementos dum array dado
    // Primeiro chama o método para descobrir quantos múltiplos são e assim definir length do novo array.
    public static int[] getCommonMultiplesFromArrayInInterval(int[] array, int intervalBegin, int intervalEnd) {
        int[] arrayCommonMultiples = new int[countMultiplesFromArrayInInterval(array, intervalBegin, intervalEnd)];
        int x = 0;

        for (int i = intervalBegin; i <= intervalEnd; i++) {

            int tempCounter = 0;

            for (int j = 0; j < array.length; j++) {
                if ( ExSeven.isNumberOneMultipleOfNumberTwo(i, array[j]) ) {
                    tempCounter++;
                }
            }
            if ( tempCounter == array.length ) {
                arrayCommonMultiples[x] = i;
                x++;
            }

        }
        return arrayCommonMultiples;
    }

    // Método que retorna um array com os múltiplos comuns dos elementos dum array dado
    // Inicializa o novo array com um tamanho de 20 e no final chama um método para truncar pelo tamanho mínimo.
    public static int[] getCommonMultiplesFromArrayInIntervalVersionTwo(int[] array, int intervalBegin, int intervalEnd) {
        int[] arrayCommonMultiples = new int[20];
        int commonMultiplesCounter = 0;

        // Por cada número do intervalo
        for (int i = intervalBegin; i <= intervalEnd; i++) {

            int tempCounter = 0;

            // Por cada número do array, verificar se i é múltiplo deste
            for (int j = 0; j < array.length; j++) {
                if ( ExSeven.isNumberOneMultipleOfNumberTwo(i, array[j]) ) {
                    tempCounter++;
                }
            }

            // Se i for múltiplo de todos os elementos do array, escrever i no array e aumentar contador de múltiplos
            // i é múltiplo de todos os elementos do array se tempCounter for igual ao tamanho do array
            if ( tempCounter == array.length ) {
                arrayCommonMultiples[commonMultiplesCounter] = i;
                commonMultiplesCounter++;
            }
        }

        // Caso não haja múltiplos comuns, retornar null
        if (commonMultiplesCounter==0) {
            return null;
        }

        // Truncar o novo array pelo comprimento que é o contador de múltiplos
        return ExSix.truncateArray(arrayCommonMultiples, commonMultiplesCounter);
    }

}
