public class ExSeven {

    // Método que retorna array com os múltiplos de um número dado num intervalo dado
    public static int[] getMultiplesOfNumberInInterval(int number, int intervalBegin, int intervalEnd) {
        int multiples = 0;

        // Juntar os múltiplos num inteiro
        for (int i = intervalBegin; i <= intervalEnd; i++) {
            if ( isNumberOneMultipleOfNumberTwo(i, number) ) {
                multiples = multiples*10 + i;
            }
        }

        int j = 0;

        // Usar os múltiplos para criar um array com o tamanho exacto
        int[] arrayMultiples = new int[ExOne.getNumberDigitsInInt(multiples)];

        // Escrever os múltiplos no array
        for (int i = intervalBegin; i <= intervalEnd; i++) {
            if ( isNumberOneMultipleOfNumberTwo(i, number) ) {
                arrayMultiples[j] = i;
                j++;
            }
        }
        return arrayMultiples;
    }

    // Método que retorna se um número é múltiplo de outro
    public static boolean isNumberOneMultipleOfNumberTwo(int numberOne, int numberTwo) {
        boolean isMultiple = false;

        if ( (numberOne % numberTwo) == 0 ) {
            isMultiple = true;
        }

        return isMultiple;
    }
}
