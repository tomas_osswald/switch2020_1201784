public class ExTwo {

    // Método que retorna um vector cujos elementos sejam os digitos de um número dado
    public static int[] writeDigitsIntoArray(int number) {
        int[] digits = new int[ExOne.getNumberDigitsInInt(number)];

        for (int i=digits.length-1; i >= 0; i--) {
            digits[i] = number % 10;
            number /= 10;
        }

        return digits;
    }
}
