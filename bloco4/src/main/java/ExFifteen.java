public class ExFifteen {

    // Método que retorna o elemento menor de uma matriz
    public static int getSmallestIntFromMatrix(int[][] array) {
        int smallest = array[0][0];

        // Como no ExTen, mas agora como é uma matriz é necessário outro loop
        for (int i = 0; i < array.length; ++i) {

            for (int j = 0; j < array[i].length; ++j) {
                if (smallest > array[i][j]) {
                    smallest = array[i][j];
                }
            }

        }

        return smallest;
    }

    // Método que retorna o elemento maior de uma matriz
    public static int getLargestIntFromMatrix(int[][] array) {
        int largest = array[0][0];

        for (int i = 0; i < array.length; ++i) {

            for (int j = 0; j < array[i].length; ++j) {
                if (largest < array[i][j]) {
                    largest = array[i][j];
                }
            }

        }

        return largest;
    }

    // Método que retorna a média dos elementos de uma matriz
    public static double getAverageFromMatrix(int[][] array) {
        int sum = 0;
        double elementCounter = 0.0D;

        for (int i = 0; i < array.length; ++i) {

            for (int j = 0; j < array[i].length; ++j) {
                sum += array[i][j];
                ++elementCounter;
            }

        }

        return (double) sum / elementCounter;
    }

    // Método que retorna o producto dos elementos de uma matriz
    public static int getProductFromMatrix(int[][] array) {
        int product = 1;

        // A pedido do intellij usar 'enhanced for' para substituir 'for'
        for (int[] ints : array) {

            for (int anInt : ints) {
                product *= anInt;
            }
        }

        return product;
    }

    // Método que retorna os elementos não repetidos de uma matriz
    public static int[] getNonRepeatedElementsFromMatrix(int[][] array) {
        int[] arrayNonRepeatedElements = new int[20];
        int countNonRepeatedElements = 0;

        // Para cada elemento da matriz
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; ++j) {

                int tempCounter = 0;

                // Verificar com cada elemento da matriz
                for (int k = 0; k < array.length; ++k) {
                    for (int p = 0; p < array[k].length; ++p) {

                        // Se igual aumentar tempCounter
                        // Para não ser repetido, só deve ser igual a si mesmo (tempCounter = 1)
                        if (array[i][j] == array[k][p]) {
                            ++tempCounter;
                        }

                    }

                }

                // Se número não repetido, escrever no novo array e aumentar contagem
                if (tempCounter == 1) {
                    arrayNonRepeatedElements[countNonRepeatedElements] = array[i][j];
                    ++countNonRepeatedElements;
                }
            }
        }

        // Truncar novo array pelo tamanho igual à contagem de elementos não repetidos
        return ExSix.truncateArray(arrayNonRepeatedElements, countNonRepeatedElements);
    }

    // Método que retorna os elementos primos de uma matriz
    // Inicializar array com 20 para depois truncar
    public static int[] getPrimeElementsFromMatrix(int[][] array) {
        int[] arrayPrimes = new int[20];
        int primeCounter = 0;

        // Para cada elemento da matriz
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[i].length; j++) {

                int tempCounter = 0;

                // Para cada número entre 2 e o elemento da matriz
                for (int k = 2; k < array[i][j]; ++k) {

                    // Se resto da divisão do elemento pelo número for 0, número não é primo
                    if (array[i][j] % k == 0) {
                        ++tempCounter;
                    }
                }

                /* Escrever no array de números primos caso tempCounter = 0 (número não divisível por digito menor que número e maior que 1)
                 * Excluir 1 e 0, que não são números primos
                 */
                if (tempCounter == 0 && array[i][j] > 1) {
                    arrayPrimes[primeCounter] = array[i][j];
                    ++primeCounter;
                }
            }
        }

        return ExSix.truncateArray(arrayPrimes, primeCounter);
    }

    // Método que retorna a diagonal principal da matriz
    public static int[] getMainDiagonalFromMatrix(double[][] array) {
        int[] arrayMainDiagonal = new int[20];
        int lengthDiagonal = 0;

        // Verificar se matriz é quadrada ou rectangular, caso contrário retorna null
        if (!ExThirteen.isDoublesMatrixSquare(array) && !ExFourteen.isMatrixRectangle(array)) {
            return null;
        }

        // Para cada elemento da matriz em diagonal, não excedendo número de linhas ou colunas
        for (int i = 0; i < array.length && i < array[i].length; i++) {
            arrayMainDiagonal[lengthDiagonal] = (int) array[i][i];
            lengthDiagonal++;
        }

        // Truncar novo array pelo tamanho que é o comprimento da diagonal
        return ExSix.truncateArray(arrayMainDiagonal, lengthDiagonal);

    }

    // Método que retorna a diagonal secundária da matriz
    public static int[] getSecondaryDiagonalFromMatrix(double[][] array) {
        int[] arraySecondaryDiagonal = new int[20];
        int lengthDiagonal = 0;

        // Verificar se matriz é quadrada ou rectangular, caso contrário retorna null
        if (!ExThirteen.isDoublesMatrixSquare(array) && !ExFourteen.isMatrixRectangle(array)) {
            return null;
        }

        // Para cada elemento da matriz em diagonal inversa
        for (int i = (array[0].length - 1), j = 0; i >= 0 && j < array.length; i--, j++) {
            arraySecondaryDiagonal[lengthDiagonal] = (int) array[j][i];
            lengthDiagonal++;
        }

        return ExSix.truncateArray(arraySecondaryDiagonal, lengthDiagonal);
    }

    // Método que verifica se matriz é uma matriz identidade
    public static boolean isIdentityMatrix(double[][] array) {

        // Verificar se matriz é quadrada caso contrário retorna false
        if (!ExThirteen.isDoublesMatrixSquare(array)) {
            return false;
        }

        boolean isIdentityMatrix = true;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j) {
                    if (array[i][j] != 1) {
                        isIdentityMatrix = false;
                        j = array[i].length;
                    }
                } else {
                    if (array[i][j] != 0) {
                        isIdentityMatrix = false;
                        j = array[i].length;
                    }
                }
            }
        }

        return isIdentityMatrix;
    }

    /**
     * Método que retorna a matriz inversa:
     * <p>
     * Step 1: calculating the Matrix of Minors,
     * Step 2: then turn that into the Matrix of Cofactors,
     * Step 3: then the Adjugate, and
     * Step 4: multiply that by 1/Determinant.
     *
     * @param array matriz dado
     * @return matriz inversa
     */
    public static double[][] getInverseMatrix(double[][] array) {
        double[][] arrayResult = array;

        // Step 1
        arrayResult = getMatrixOfMinors(arrayResult);

        // Step 2
        arrayResult = getMatrixOfCofactors(arrayResult);

        // Step 3
        arrayResult = getTransposedMatrix(arrayResult);

        // Step 4
        arrayResult = ExSeventeen.getProductMatrixWithConstant(arrayResult, (1 / ExSixteen.getDeterminantOfMatrixWithLaplaceExpansion(array)));

        return arrayResult;
    }

    /**
     * Retorna matriz com sinais invertidos
     *
     * @param array matriz dada
     * @return matriz com sinais invertidos
     */
    public static double[][] getMatrixOfCofactors(double[][] array) {
        double[][] arrayCofactors = array;

        // alternator muda a cada iteração do ciclo, alterando o sinal
        boolean alternator = true;
        // Para cada linha do array
        for (int i = 0; i < array.length; i++) {

            // Para cada coluna do array
            for (int j = 0; j < array[i].length; j++) {
                if (alternator) {
                    alternator = false;
                } else {
                    arrayCofactors[i][j] = -arrayCofactors[i][j];
                    alternator = true;
                }
            }
        }

        return arrayCofactors;
    }

    /**
     * Método que retorna a matriz transposta
     *
     * @param array matriz dada
     * @return matriz transposta
     */
    public static double[][] getTransposedMatrix(double[][] array) {
        double[][] arrayTransposed = new double[array[0].length][array.length];

        // Para cada linha do array
        for (int i = 0; i < array.length; i++) {

            // Para cada coluna do array
            for (int j = 0; j < array[i].length; j++) {

                // Copia o valor do array original que está espelhado na diagonal
                arrayTransposed[j][i] = array[i][j];
            }
        }

        return arrayTransposed;
    }

    /**
     * Método que retorna a Matrix of Minors
     *
     * @param array matriz dada
     * @return matrix of minors
     */
    public static double[][] getMatrixOfMinors(double[][] array) {
        double[][] arrayMinors = new double[array.length][array.length];

        if (array.length == 2) {
            arrayMinors[0][0] = array[1][1];
            arrayMinors[0][1] = array[1][0];
            arrayMinors[1][0] = array[0][1];
            arrayMinors[1][1] = array[0][0];
        } else {
            // Para cada linha do array
            for (int i = 0; i < array.length; i++) {

                // Para cada coluna do array
                for (int j = 0; j < array[i].length; j++) {
                    arrayMinors[i][j] = ExSixteen.getDeterminantOfMatrixWithLaplaceExpansion(ExSixteen.removeLineAndColumnFromSquareMatrix(array, i, j));
                }
            }
        }
        return arrayMinors;
    }
}

//  = ExSeventeen.getProductMatrixWithConstant(arrayMinors, (1 / ExSixteen.getDeterminantOfMatrixWithLaplaceExpansion(array)));