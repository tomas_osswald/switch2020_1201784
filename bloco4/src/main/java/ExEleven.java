public class ExEleven {

    // Método que retorna o producto escalar de dois arrays dados
    public static int getDotProductFromTwoArrays(int[] arrayOne, int[] arrayTwo) {
        int dotProduct = 0;

        // Para cada elemento do array até ao fim do primeiro ou do segundo, multiplicar com o elemento da mesma posição no outro array
        for (int i = 0; i < arrayOne.length && i < arrayTwo.length; i++) {
            dotProduct += arrayOne[i] * arrayTwo[i];
        }

        return dotProduct;
    }
}