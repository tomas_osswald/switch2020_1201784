public class ExFive {

    // Método para somar os dígitos pares de um número inteiro dado
    public static int getSumEvensFromInt(int number) {
        int evens = getEvensFromInt(number);
        int sumEvens = getSumDigitsFromInt(evens);
        return sumEvens;
    }

    // Método para somar os dígitos impares de um número inteiro dado
    public static int getSumOddsFromInt(int number) {
        int odds = getOddsFromInt(number);
        int sumOdds = getSumDigitsFromInt(odds);
        return sumOdds;
    }

    // Método que retorna um inteiro com os dígitos pares de um número inteiro dado
    public static int getEvensFromInt(int number) {
        int digit;
        int evens=0;

        while (number > 0) {
            digit = number % 10;
            number /= 10;
            if (ExFour.isEven(digit)) {
                evens = evens*10 + digit;
            }
        }

        return evens;
    }

    // Método que retorna um inteiro com os dígitos impares de um número inteiro dado
    public static int getOddsFromInt(int number) {
        int digit;
        int evens=0;

        while (number > 0) {
            digit = number % 10;
            number /= 10;
            if (!ExFour.isEven(digit)) {
                evens = evens*10 + digit;
            }
        }

        return evens;
    }

    // Método que retorna um inteiro com a soma dos dígitos de um número inteiro dado
    public static int getSumDigitsFromInt(int number) {
        int sum = 0;

        do {
            sum += (number % 10);
            number /= 10;
        } while (number != 0);

        return sum;
    }
}
