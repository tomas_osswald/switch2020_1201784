public class ExSeventeen {

    /**
     *  Método para calcular o producto de uma matriz por uma constante
     * @param array matriz dada
     * @param constant constante dada
     * @return resultado
     */
    public static double[][] getProductMatrixWithConstant(double[][] array, double constant) {
        double[][] arrayResult = array;

        for (int i = 0; i < arrayResult.length; i++) {
            for (int j = 0; j < arrayResult[i].length; j++) {
                arrayResult[i][j] *= constant;
            }
        }

        return arrayResult;
    }

    /**
     * Método para somar dois arrays (tamanho tem de ser igual)
     * @param arrayOne primeira matriz
     * @param arrayTwo segunda matriz
     * @return resultado
     */
    public static double[][] getSumTwoMatrices(double[][] arrayOne, double[][] arrayTwo) {
        double[][] arrayResult = arrayOne;

        for (int i = 0; i < arrayResult.length; i++) {
            for (int j = 0; j < arrayResult[i].length; j++) {
                arrayResult[i][j] = arrayOne[i][j] + arrayTwo[i][j];
            }
        }

        return arrayResult;
    }

    /**
     * Método que retorna o producto de duas matrizes de números inteiros
     * @param arrayOne primeira matriz
     * @param arrayTwo segunda matriz
     * @return resultado
     */
    public static double[][] getProductTwoMatrices(double[][] arrayOne, double[][] arrayTwo) {
        double[][] arrayResult = arrayOne;

        for (int i = 0; i < arrayResult.length; i++) {
            for (int j = 0; j < arrayResult[i].length; j++) {
                arrayResult[i][j] = arrayOne[i][j] * arrayTwo[i][j];
            }
        }

        return arrayResult;
    }
}
