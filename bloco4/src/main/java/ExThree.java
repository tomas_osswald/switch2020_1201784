public class ExThree {

    // Método que calcula a soma dos elementos de um vector de inteiros
    public static int getSumArray(int[] array) {
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }

        return sum;
    }
}
