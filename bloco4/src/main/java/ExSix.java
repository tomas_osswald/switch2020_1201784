public class ExSix {

    // Método que retorna um array com apenas os primeiros X elementos de um array dado
    public static int[] truncateArray(int[] array, int number) {
        if (number > array.length) {
            number = array.length;
        }

        int[] newArray = new int[number];

        for (int i=0; i < number; i++) {
            newArray[i] = array[i];
        }

        return newArray;
    }
}
