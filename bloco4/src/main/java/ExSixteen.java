public class ExSixteen {

    // Método que retorna a determinante de uma matriz quadradada
    public static double getDeterminantOfMatrixWithLaplaceExpansion(double[][] array) {
        // Verificar se matriz é quadrada, caso contrário retorna zero
        if (!ExThirteen.isDoublesMatrixSquare(array)) {
            return 0;
        }

        boolean alternator = true;
        double determinant = 0;

        // Se matriz quadrada tiver tamanho 2, a determinante pode ser calculada directamente
        if(array.length == 2) {
            determinant = ExTen.getProductFromArray(ExFifteen.getMainDiagonalFromMatrix(array)) - ExTen.getProductFromArray(ExFifteen.getSecondaryDiagonalFromMatrix(array));
        } else {

            // Determinante de uma matriz quadrada com 3 linhas é calculada dividindo-a em matrizes menores quadradas de duas linhas
            for (int i = 0; i < array.length; i++) {

                // O cálculo é feito com alternância de positivo e negativo, assegurada pela variável alternator
                if (alternator) {
                    determinant += array[0][i] * getDeterminantOfMatrixWithLaplaceExpansion(removeLineAndColumnFromSquareMatrix(array, 0, i));
                    alternator = false;
                } else {
                    determinant -= array[0][i] * getDeterminantOfMatrixWithLaplaceExpansion(removeLineAndColumnFromSquareMatrix(array, 0, i));
                    alternator = true;
                }
            }
        }

        return determinant;
    }

    /**
     * Método que remove linha e coluna de uma matriz
     * @param array matriz (se não for quadrada retorna null)
     * @param line linha a remover (se out of bounds retorna null)
     * @param column coluna a remover (se out of bounds retorna null)
     * @return nova matriz com as coluna e linha indicada removidas
     */
    public static double[][] removeLineAndColumnFromSquareMatrix(double[][] array, int line, int column) {

        // Verificar condições
        if ((line < 0 || line >= array.length) || (column < 0 || column >= array.length) || !ExThirteen.isDoublesMatrixSquare(array)) {
            return null;
        }

        // Criar novo array com a dimensões idênticas - 1 ao anterior
        double[][] newArray = new double[array.length-1][array.length-1];
        int newArrayLine = 0;

        // Para cada linha do array
        for (int i = 0;  i < array.length; i++) {

            int newArrayColumn = 0;

            // Para cada coluna do array
            for (int j = 0; j < array[i].length; j++) {

                // Se elemento não estiver numa coluna ou linha a remover, copiar para novo array
                if (i != line && j != column) {
                    newArray[newArrayLine][newArrayColumn] = array[i][j];
                    newArrayColumn++;
                }
            }

            if (i != line) {
                newArrayLine++;
            }
        }

        return newArray;
    }
}
