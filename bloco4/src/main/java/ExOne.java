public class ExOne {

    // Método para calcular o número de dígitos de um inteiro
    public static int getNumberDigitsInInt(int number) {
        int i = 0;

        do {
            number /= 10;
            i++;
        } while (number != 0);

        return i;
    }
}
