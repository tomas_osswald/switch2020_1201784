public class ExTwelve {

    // Método que verifica se número de colunas das linhas de uma matriz são iguais
    // Se sim retorna número de colunas, se não retorna -1
    public static int checkIfNumberOfColumnsIsEqualDoublesArray(double[][] array) {
        int numberOfColumns = -1;
        int tempCounter = 0;

        // Para cada linha do array verificar se o seu número de colunas é idêntico ao número de colunas da próxima linha
        for (int i = 1; i < array.length; i++) {
            if (array[0].length == array[i].length) {
                tempCounter++;
            }

            // Se tempCounter for igual ao número de linhas menos um, significa que todas as linhas têm o mesmo número de colunas
            if (tempCounter == (array.length - 1) ) {
                numberOfColumns = array[0].length;
            }
        }

        return numberOfColumns;
    }

    // Método idêntico mas para arrays de ints.
    public static int checkIfNumberOfColumnsIsEqualIntsArray(int[][] array) {
        int numberOfColumns = -1;
        int tempCounter = 0;

        // Para cada linha do array verificar se o seu número de colunas é idêntico ao número de colunas da próxima linha
        for (int i = 1; i < array.length; i++) {
            if (array[0].length == array[i].length) {
                tempCounter++;
            }

            // Se tempCounter for igual ao número de linhas menos um, significa que todas as linhas têm o mesmo número de colunas
            if (tempCounter == (array.length - 1) ) {
                numberOfColumns = array[0].length;
            }
        }

        return numberOfColumns;
    }
}
