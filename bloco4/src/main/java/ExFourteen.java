public class ExFourteen {

    // Método que retorna se uma matriz é um rectângulo
    public static boolean isMatrixRectangle(double[][] array) {
        boolean isRectangle = false;

        // Se o número de colunas de todas as linhas for diferente do número de linhas, a matriz é rectangular
        // Se o método devolver -1 as linhas não têm o mesmo número de colunas, logo não é rectangular
        if (ExTwelve.checkIfNumberOfColumnsIsEqualDoublesArray(array) != array.length && ExTwelve.checkIfNumberOfColumnsIsEqualDoublesArray(array) != -1) {
            isRectangle = true;
        }

        return isRectangle;
    }
}
