public class ExThirteen {

    // Método que retorna se uma matriz é quadrada
    public static boolean isDoublesMatrixSquare(double[][] array) {
        boolean isSquare = false;

        // Se o número de colunas de todas as linhas for igual ao número de linhas, a matriz é quadrada
        if (ExTwelve.checkIfNumberOfColumnsIsEqualDoublesArray(array) == array.length) {
            isSquare = true;
        }

        return isSquare;
    }

    // Método que retorna se uma matriz é quadrada
    public static boolean isIntsMatrixSquare(int[][] array) {
        boolean isSquare = false;

        // Se o número de colunas de todas as linhas for igual ao número de linhas, a matriz é quadrada
        if (ExTwelve.checkIfNumberOfColumnsIsEqualIntsArray(array) == array.length) {
            isSquare = true;
        }

        return isSquare;
    }
}
