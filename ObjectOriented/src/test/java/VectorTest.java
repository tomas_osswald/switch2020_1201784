import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @Test
    void VectorContructorErrorInputNullArray() {
        int[] array = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Vector vectorOne = new Vector(array);
            }, "Array is empty or null");
    }

    @Test
    void VectorContructorErrorInputEmptyArray() {
        int[] array = { };
        assertThrows(IllegalArgumentException.class, () -> {
            Vector vectorOne = new Vector(array);
        }, "Array is empty or null");
    }

    @Test
    void sortAscendingInputArraySizeOne() {
        // arrange
        int[] array = {1};
        Vector vectorOne = new Vector(array);
        int[] expected = {1};

        // act
        int[] result = vectorOne.sortAscending();

        // assert
        assertSame(array, result);
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }

    @Test
    void sortAscendingInputArraySizeTwoAlreadySorted() {
        // arrange
        int[] array = {1,2};
        Vector vectorOne = new Vector(array);
        int[] expected = {1,2};

        // act
        int[] result = vectorOne.sortAscending();

        // assert
        assertSame(array, result);
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }

    @Test
    void sortAscendingInputArraySizeTwoNotSorted() {
        // arrange
        int[] array = {2,1};
        Vector vectorOne = new Vector(array);
        int[] expected = {1,2};

        // act
        int[] result = vectorOne.sortAscending();

        // assert
        assertSame(array, result);
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }

    @Test
    void sortAscendingInputArraySizeFiveNotSorted() {
        // arrange
        int[] array = {30,25,25,-1,20};
        Vector vectorOne = new Vector(array);
        int[] expected = {-1,20,25,25,30};

        // act
        int[] result = vectorOne.sortAscending();

        // assert
        assertSame(array, result);
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }

    @Test
    void createCopyOfSizeTenArray() {
        // arrange
        int[] array = {-21, 30,20,12,34,12,55,0, -1, -9};
        Vector vectorOne = new Vector(array);
        int[] expected = {-21, 30,20,12,34,12,55,0, -1, -9};

        // act
        int[] result = vectorOne.createCopy();

        // assert
        assertNotSame(array, result);
        assertEquals(array.length, result.length);
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }

    @Test
    void createCopyOfSizeFourArrayPlusChanges() {
        // arrange
        int[] array = {31,21,4,34};
        Vector vectorOne = new Vector(array);
        int[] expected = {31,21,5,34};

        // act
        int[] result = vectorOne.createCopy();
        result[2] = 5;

        // assert
        assertNotSame(array, result);
        assertNotSame(expected, result);
        assertEquals(array.length, result.length);
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }
}