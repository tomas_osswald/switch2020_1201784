import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    void StudentConstructorValidStudentInputNumberAndName() {
        int number = 1234567;
        String name = "ABCDE";
        Student studentOne = new Student(number, name);
        assertNotNull(studentOne);
    }

    @Test
    void StudentConstructorValidStudentInputNumberNameAndGrade() {
        int number = 1234567;
        String name = "ABCDE";
        int grade = 20;
        Student studentOne = new Student(number, name, grade);
        assertNotNull(studentOne);
    }

    @Test
    void StudentContructorErrorInputNumberSixDigits() {
        int number = 123456;
        String name = "AAAAA";
        assertThrows(IllegalArgumentException.class, () -> {
            Student studentOne = new Student(number, name);
        }, "Invalid Number: Must be 7 digits long");
    }

    @Test
    void StudentContructorErrorInputNumberEightDigits() {
        int number = 12345678;
        String name = "AAAAA";
        assertThrows(IllegalArgumentException.class, () -> {
            Student studentOne = new Student(number, name);
        }, "Invalid Number: Must be 7 digits long");
    }

    @Test
    void StudentContructorErrorInputNumberNegative() {
        int number = -1234567;
        String name = "AAAAA";
        assertThrows(IllegalArgumentException.class, () -> {
            Student studentOne = new Student(number, name);
        }, "Invalid Number: Must be 7 digits long");
    }

    @Test
    void StudentContructorErrorInputNameNull() {
        int number = 1234567;
        String name = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Student studentOne = new Student(number, name);
        }, "Invalid Name: Must be at least 5 characters long");
    }

    @Test
    void StudentContructorErrorInputNameFourCharacters() {
        int number = 1234567;
        String name = "José";
        assertThrows(IllegalArgumentException.class, () -> {
            Student studentOne = new Student(number, name);
        }, "Invalid Name: Must be at least 5 characters long");
    }

    @Test
    void StudentContructorErrorInputNameSpaces() {
        int number = 1234567;
        String name = "  aa  ";
        assertThrows(IllegalArgumentException.class, () -> {
            Student studentOne = new Student(number, name);
        }, "Invalid Name: Must be at least 5 characters long");
    }

    @Test
    void StudentContructorErrorInputInvalidNameAndNumber() {
        int number = 1;
        String name = "a";
        assertThrows(IllegalArgumentException.class, () -> {
            Student studentOne = new Student(number, name);
        }, "Invalid Name: Must be at least 5 characters long");
    }

    @Test
    void doEvaluationValidGrade() {
        int number = 1234567;
        String name = "ABCDE";
        int grade = 1;
        Student studentOne = new Student(number, name);
        assertDoesNotThrow(() -> {
            studentOne.doEvaluation(grade);
        });
    }

    @Test
    void doEvaluationInvalidGradeUnder() {
        int number = 1234567;
        String name = "ABCDE";
        int grade = -1;
        Student studentOne = new Student(number, name);
        assertThrows(IllegalArgumentException.class, () -> {
            studentOne.doEvaluation(grade);
        }, "Invalid Grade: Must be between 0 and 20");
    }

    @Test
    void doEvaluationInvalidGradeOver() {
        int number = 1234567;
        String name = "ABCDE";
        int grade = 21;
        Student studentOne = new Student(number, name);
        assertThrows(IllegalArgumentException.class, () -> {
            studentOne.doEvaluation(grade);
        }, "Invalid Grade: Must be between 0 and 20");
    }

    @Test
    void doEvaluationAlreadyEvaluated() {
        int number = 1234567;
        String name = "ABCDE";
        int grade = 1;
        Student studentOne = new Student(number, name, grade);
        assertThrows(IllegalArgumentException.class, () -> {
            studentOne.doEvaluation(grade);
        }, "Student has already been evaluated");
    }

    @Test
    void isApprovedResultFalse() {
        int number = 1234567;
        String name = "ABCDE";
        int grade = 9;
        Student studentOne = new Student(number, name, grade);
        boolean expected = false;
        boolean result = studentOne.isApproved();
        assertEquals(expected, result);
    }

    @Test
    void isApprovedResultTrue() {
        int number = 1234567;
        String name = "ABCDE";
        int grade = 10;
        Student studentOne = new Student(number, name, grade);
        boolean expected = true;
        boolean result = studentOne.isApproved();
        assertEquals(expected, result);
    }

    @Test
    void createStudentList() {
        StudentList stList = new StudentList();
        Student[] result = stList.toArray();
        assertEquals(0, result.length);
    }


    @Test
    void sortByNumberAscendingAlreadySorted() {
        Student studentOne = new Student(1234567, "AAAAA");
        Student studentTwo = new Student(1234568, "AAAAB");
        Student studentThree = new Student(1234569, "AAAAC");

        Student[] students = {studentOne, studentTwo, studentThree};
        StudentList stList = new StudentList(students);
        Student[] expected = {studentOne, studentTwo, studentThree};

        stList.sortByNumberAscending();
        Student[] result = stList.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortByNumberAscendingNotSorted() {
        Student studentOne = new Student(1234567, "AAAAA");
        Student studentTwo = new Student(1234560, "AAAAB");
        Student studentThree = new Student(1234563, "AAAAC");

        Student[] students = {studentOne, studentTwo, studentThree};
        StudentList stList = new StudentList(students);
        Student[] expected = {studentTwo, studentThree, studentOne};

        stList.sortByNumberAscending();
        Student[] result = stList.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortByGradeDescendingAlreadySorted() {
        Student studentOne = new Student(1234567, "AAAAA");
        studentOne.doEvaluation(17);
        Student studentTwo = new Student(1234568, "AAAAB");
        studentTwo.doEvaluation(15);
        Student studentThree = new Student(1234569, "AAAAC");
        studentThree.doEvaluation(12);

        Student[] students = {studentOne, studentTwo, studentThree};
        StudentList stList = new StudentList(students);
        Student[] expected = {studentOne, studentTwo, studentThree};

        stList.sortByGradeDescending();
        Student[] result = stList.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortByGradeDescendingNotSorted() {
        Student studentOne = new Student(1234567, "AAAAA");
        studentOne.doEvaluation(12);
        Student studentTwo = new Student(1234568, "AAAAB");
        studentTwo.doEvaluation(17);
        Student studentThree = new Student(1234569, "AAAAC");
        studentThree.doEvaluation(15);

        Student[] students = {studentOne, studentTwo, studentThree};
        StudentList stList = new StudentList(students);
        Student[] expected = {studentTwo, studentThree, studentOne};

        stList.sortByGradeDescending();
        Student[] result = stList.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void equalsTrue() {
        Student studentOne = new Student(1234567, "AAAAA");
        Student studentTwo = new Student(1234567, "AAAAB");

        boolean result = studentOne.equals(studentTwo);

        assertTrue(result);
    }

    @Test
    void equalsTrueToItself() {
        Student studentOne = new Student(1234567, "AAAAA");
        boolean result = studentOne.equals(studentOne);
        assertTrue(result);
    }

    @Test
    void equalsFalseDueToNull() {
        Student studentOne = new Student(1234567, "AAAAA");
        boolean result = studentOne.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsFalseDueToDifferentType() {
        Student studentOne = new Student(1234567, "AAAAA");
        boolean result = studentOne.equals(new String("1234567"));
        assertFalse(result);
    }

    @Test
    void equalsFalseDueToDifferentNumbers() {
        Student studentOne = new Student(1234567, "AAAAA");
        Student studentTwo = new Student(1234568, "AAAAB");

        boolean result = studentOne.equals(studentTwo);

        assertFalse(result);
    }
}