public class Vector {

    private int[] vector;

    private Vector() {
    }

    /**
     * Contructor for vector, if array is valid assigns array to vector
     * @param array
     */
    public Vector(int[] array) {
        if (!isValid(array)) {
            throw new IllegalArgumentException("Array is empty or null");
        }
        this.vector = array;
    }

    /**
     * Check if input array is not null and is not empty
     * @param array
     * @return true = array is valid, false = array is null or empty
     */
    private boolean isValid(int[] array) {
        boolean isValid = true;
        if (array == null || array.length == 0) {
            isValid = false;
        }
        return isValid;
    }

    /**
     * Returns Vector in ascending order
     * @return
     */
    public int[] sortAscending() {
        int temp = 0;

        for (int i = 0; i < vector.length; i++) {
            for (int j = i + 1; j < vector.length; j++) {
                if (vector[i] > vector[j]) {
                    temp = vector[i];
                    vector[i] = vector[j];
                    vector[j] = temp;
                }
            }
        }

        return vector;
    }

    /**
     * Returns copy of vector
     * @return
     */
    public int[] createCopy() {
        int[] copy = new int[vector.length];

        for (int i = 0; i < vector.length; i++) {
            copy[i] = vector[i];
        }

        return copy;
    }
}
