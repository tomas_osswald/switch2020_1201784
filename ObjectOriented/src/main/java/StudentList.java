import java.util.ArrayList;
import java.util.Comparator;

public class StudentList {

    // attributes
    private final ArrayList<Student> students;

    // constructor

    public StudentList() {
        this.students = new ArrayList();
    }

    public StudentList(Student[] students) {
        if (students == null) {
            throw new IllegalArgumentException("Students array should not be null");
        }
        this.students = new ArrayList();
        for (Student st : students) {
            this.add(st);
        }
    }

    // private classes

    private class SortByNumberAscending implements Comparator<Student> {
        public int compare(Student studentOne, Student studentTwo) {
            return studentOne.compareToByNumber(studentTwo);
        }
    }

    private class SortByGradeDescending implements Comparator<Student> {
        public int compare(Student studentOne, Student studentTwo) {
            return studentOne.compareToByGrade(studentTwo) * -1;
        }
    }

    // public methods

    /**
     * Sorts students by number in asceding order
     */
    public void sortByNumberAscending() {
        this.students.sort(new SortByNumberAscending());
    }

    /**
     * Sorts students by grade in descending order
     */
    public void sortByGradeDescending() {
        this.students.sort(new SortByGradeDescending());
    }

    /**
     * Add a student that does not exist yet in the studentList
     *
     * @param student
     * @return
     */
    public boolean add(Student student) {
        if (student == null) {
            return false;
        }
        if (this.students.contains(student)) {
            return false;
        }
        return this.students.add(student);
    }

    /**
     * Remove a student that has to exist in the Student List
     * @param student
     * @return
     */
    public boolean remove(Student student) {
        if (student == null) {
            return false;
        }
        if (!this.students.contains(student)) {
            return false;
        }
        return this.students.remove(student);
    }

    /**
     * Copy content of Student List to an array of Students
     *
     * @return
     */
    public Student[] toArray() {
        Student[] array = new Student[this.students.size()];
        return this.students.toArray(array);
    }
}
