public class Student {

    // attributes

    private int number;
    private String name;
    private int grade = -1;

    // constructors

    private Student() {

    }

    /**
     * Constructor for Student using number and name
     *
     * @param number
     * @param name
     */
    public Student(int number, String name) {
        setNumber(number);
        setName(name);
    }

    /**
     * Constructor for Student using number, name and grade
     *
     * @param number
     * @param name
     * @param grade
     */
    public Student(int number, String name, int grade) {
        setNumber(number);
        setName(name);
        setGrade(grade);
    }

    // private methods

    /**
     * Set Student grade, check if valid
     *
     * @param grade
     */
    private void setGrade(int grade) {
        if (!isGradeValid(grade)) {
            throw new IllegalArgumentException("Invalid Grade: Must be between 0 and 20");
        }
        this.grade = grade;
    }

    /**
     * Set Student number, check if valid
     *
     * @param number
     */
    private void setNumber(int number) {
        if (!isNumberValid(number)) {
            throw new IllegalArgumentException("Invalid Number: Must be 7 digits long");
        }
        this.number = number;
    }

    /**
     * Set Student name, check if valid
     *
     * @param name
     */
    private void setName(String name) {
        if (!isNameValid(name)) {
            throw new IllegalArgumentException("Invalid Name: Must be at least 5 characters long");
        }
        this.name = name;
    }

    /**
     * Return if number is valid (7 digits long and positive)
     *
     * @param number
     * @return
     */
    private boolean isNumberValid(int number) {
        boolean validNumber = false;
        if (number > 0) {
            String numberToString = Integer.toString(number);
            if (numberToString.length() == 7) {
                validNumber = true;
            }
        }
        return validNumber;
    }

    /**
     * Return if name is valid (not null, at least 5 characters)
     *
     * @param name
     * @return
     */
    private boolean isNameValid(String name) {
        boolean validName = false;
        if (name != null) {
            name = name.trim();
            if (name.length() > 4) {
                validName = true;
            }
        }
        return validName;
    }

    /**
     * Return if grade is valid (between 0 and 20)
     *
     * @param grade
     * @return
     */
    private boolean isGradeValid(int grade) {
        return (grade > -1 && grade < 21);
    }

    /**
     * Return if a student has been evaluated (-1 means not evaluated)
     *
     * @return
     */
    private boolean isEvaluated() {
        return (grade != -1);
    }

    // public methods

    /**
     * Compare this Student number to another Student number
     *
     * @param other
     * @return 0 = same number, -1 = this less than other, 1 = this more than other
     */
    public int compareToByNumber(Student other) {
        int compare = 0;
        if (this.number < other.number) {
            compare = -1;
        }
        if (this.number > other.number) {
            compare = 1;
        }
        return compare;
    }

    /**
     * Compare this Student grade to another Student grade
     *
     * @param other
     * @return 0 = same grade, -1 = this less than other, 1 = this more than other
     */
    public int compareToByGrade(Student other) {
        int compare = 0;
        if (this.grade < other.grade) {
            compare = -1;
        }
        if (this.grade > other.grade) {
            compare = 1;
        }
        return compare;
    }

    /**
     * Set a grade for a student if grade is valid and student has not been evaluated yet
     *
     * @param grade
     */
    public void doEvaluation(int grade) {
        if (!isEvaluated()) {
            setGrade(grade);
        } else {
            throw new IllegalArgumentException("Student has already been evaluated");
        }
    }

    /**
     * Return if a student has a passing grade (grade at least 10)
     *
     * @return
     */
    public boolean isApproved() {
        return (this.grade >= 10);
    }

    /**
     * Return if a student has the same number as another student
     * @param otherStudent
     * @return true = same number; false = different number, null or different class type
     */
    public boolean equals(Object otherStudent) {
        if (this == otherStudent) {
            return true;
        }
        if (otherStudent == null || getClass() != otherStudent.getClass()) {
            return false;
        }
        Student student = (Student) otherStudent;
        return this.number == student.number;
    }
}
