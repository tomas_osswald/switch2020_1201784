package sudoku;

import org.junit.jupiter.api.Test;
import sudoku.SudokuCell;

import static org.junit.jupiter.api.Assertions.*;

class SudokuCellTest {

    @Test
    void isNumberEqualResultFalseInputDifferentNumbers() {
        int numberOne = 1, rowOne = 1, columnOne = 1;
        int number = 2;
        SudokuCell one = new SudokuCell(numberOne, rowOne, columnOne);
        boolean result;

        result = one.isNumberEqual(number);

        assertFalse(result);
    }

    @Test
    void isNumberEqualResultTrueInputEqualNumbers() {
        int numberOne = 1, rowOne = 1, columnOne = 1;
        int number = 1;
        SudokuCell one = new SudokuCell(numberOne, rowOne, columnOne);
        boolean result;

        result = one.isNumberEqual(number);

        assertTrue(result);
    }

    @Test
    void setNumberDifferentNumber() {
        int numberOne = 1, rowOne = 1, columnOne = 1;
        int number = 2;
        SudokuCell one = new SudokuCell(numberOne, rowOne, columnOne);
        boolean result;

        one.setNumber(number);
        result = one.isNumberEqual(number);

        assertTrue(result);
    }

    @Test
    void setNumberSameNumber() {
        int numberOne = 1, rowOne = 1, columnOne = 1;
        int number = 1;
        SudokuCell one = new SudokuCell(numberOne, rowOne, columnOne);
        boolean result;

        one.setNumber(number);
        result = one.isNumberEqual(number);

        assertTrue(result);
    }

    @Test
    void equalsDifferentCells() {
        int numberOne = 1, rowOne = 1, columnOne = 1;
        int numberTwo = 2, rowTwo = 1, columnTwo = 1;

        SudokuCell one = new SudokuCell(numberOne, rowOne, columnOne);
        SudokuCell two = new SudokuCell(numberTwo, rowTwo, columnTwo);

        assertNotEquals(one, two);
    }

    @Test
    void equalsEqualCells() {
        int numberOne = 1, rowOne = 1, columnOne = 1;
        int numberTwo = 1, rowTwo = 1, columnTwo = 1;

        SudokuCell one = new SudokuCell(numberOne, rowOne, columnOne);
        SudokuCell two = new SudokuCell(numberTwo, rowTwo, columnTwo);

        assertEquals(one, two);
    }
}