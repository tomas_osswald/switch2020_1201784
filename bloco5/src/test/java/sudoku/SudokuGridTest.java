package sudoku;

import org.junit.jupiter.api.Test;
import sudoku.SudokuGrid;

import static org.junit.jupiter.api.Assertions.*;

class SudokuGridTest {

    @Test
    void testEqualsSameArrays() {
        int[][] easy = {
                {0, 2, 0, 0, 1, 7, 0, 3, 0},
                {8, 5, 1, 0, 9, 4, 2, 0, 0},
                {4, 7, 0, 2, 6, 0, 9, 0, 0},
                {3, 4, 0, 0, 5, 0, 0, 0, 7},
                {0, 0, 8, 0, 0, 2, 1, 0, 3},
                {0, 0, 0, 6, 0, 8, 5, 0, 0},
                {0, 0, 4, 0, 0, 9, 6, 1, 0},
                {6, 0, 0, 0, 0, 3, 7, 0, 9},
                {0, 1, 0, 0, 4, 6, 0, 5, 0}};
        SudokuGrid gridOne = new SudokuGrid(easy);
        SudokuGrid gridTwo = new SudokuGrid(easy);

        assertEquals(gridOne, gridTwo);
    }

    @Test
    void testEqualsDifferentArrays() {
        int[][] easy = {
                {0, 2, 0, 0, 1, 7, 0, 3, 0},
                {8, 5, 1, 0, 9, 4, 2, 0, 0},
                {4, 7, 0, 2, 6, 0, 9, 0, 0},
                {3, 4, 0, 0, 5, 0, 0, 0, 7},
                {0, 0, 8, 0, 0, 2, 1, 0, 3},
                {0, 0, 0, 6, 0, 8, 5, 0, 0},
                {0, 0, 4, 0, 0, 9, 6, 1, 0},
                {6, 0, 0, 0, 0, 3, 7, 0, 9},
                {0, 1, 0, 0, 4, 6, 0, 5, 0}};
        int[][] medium = {
                {0, 8, 0, 9, 0, 0, 0, 5, 0},
                {4, 0, 0, 6, 5, 0, 9, 1, 3},
                {0, 0, 5, 4, 0, 0, 0, 0, 0},
                {1, 0, 2, 0, 6, 5, 0, 0, 0},
                {7, 0, 4, 0, 0, 0, 5, 0, 8},
                {0, 0, 0, 0, 0, 0, 0, 0, 2},
                {3, 0, 0, 0, 0, 1, 0, 0, 6},
                {8, 7, 0, 0, 0, 0, 0, 4, 0},
                {0, 5, 0, 0, 4, 0, 8, 0, 1}};
        SudokuGrid gridOne = new SudokuGrid(easy);
        SudokuGrid gridTwo = new SudokuGrid(medium);

        assertNotEquals(gridOne, gridTwo);
    }

    @Test
    void writeNumberSuccessTrue() {
        int[][] easy = {
                {0, 2, 0, 0, 1, 7, 0, 3, 0},
                {8, 5, 1, 0, 9, 4, 2, 0, 0},
                {4, 7, 0, 2, 6, 0, 9, 0, 0},
                {3, 4, 0, 0, 5, 0, 0, 0, 7},
                {0, 0, 8, 0, 0, 2, 1, 0, 3},
                {0, 0, 0, 6, 0, 8, 5, 0, 0},
                {0, 0, 4, 0, 0, 9, 6, 1, 0},
                {6, 0, 0, 0, 0, 3, 7, 0, 9},
                {0, 1, 0, 0, 4, 6, 0, 5, 0}};
        SudokuGrid gridOne = new SudokuGrid(easy);
        int[][] easyPlusNumber = {
                {9, 2, 0, 0, 1, 7, 0, 3, 0},
                {8, 5, 1, 0, 9, 4, 2, 0, 0},
                {4, 7, 0, 2, 6, 0, 9, 0, 0},
                {3, 4, 0, 0, 5, 0, 0, 0, 7},
                {0, 0, 8, 0, 0, 2, 1, 0, 3},
                {0, 0, 0, 6, 0, 8, 5, 0, 0},
                {0, 0, 4, 0, 0, 9, 6, 1, 0},
                {6, 0, 0, 0, 0, 3, 7, 0, 9},
                {0, 1, 0, 0, 4, 6, 0, 5, 0}};
        SudokuGrid result = new SudokuGrid(easyPlusNumber);
        int number = 9, row = 0, column = 0;
        gridOne.writeNumber(number, row, column);

        assertEquals(gridOne, result);
    }

    @Test
    void writeNumberSuccessFalse() {
        int[][] easy = {
                {0, 2, 0, 0, 1, 7, 0, 3, 0},
                {8, 5, 1, 0, 9, 4, 2, 0, 0},
                {4, 7, 0, 2, 6, 0, 9, 0, 0},
                {3, 4, 0, 0, 5, 0, 0, 0, 7},
                {0, 0, 8, 0, 0, 2, 1, 0, 3},
                {0, 0, 0, 6, 0, 8, 5, 0, 0},
                {0, 0, 4, 0, 0, 9, 6, 1, 0},
                {6, 0, 0, 0, 0, 3, 7, 0, 9},
                {0, 1, 0, 0, 4, 6, 0, 5, 0}};
        SudokuGrid gridOne = new SudokuGrid(easy);
        int number = 2, row = 0, column = 0;
        boolean result;
        result = gridOne.writeNumber(number, row, column);

        assertFalse(result);
    }

    @Test
    void checkIfGridIsFullResultFalse() {
        int[][] easy = {
                {0, 2, 0, 0, 1, 7, 0, 3, 0},
                {8, 5, 1, 0, 9, 4, 2, 0, 0},
                {4, 7, 0, 2, 6, 0, 9, 0, 0},
                {3, 4, 0, 0, 5, 0, 0, 0, 7},
                {0, 0, 8, 0, 0, 2, 1, 0, 3},
                {0, 0, 0, 6, 0, 8, 5, 0, 0},
                {0, 0, 4, 0, 0, 9, 6, 1, 0},
                {6, 0, 0, 0, 0, 3, 7, 0, 9},
                {0, 1, 0, 0, 4, 6, 0, 5, 0}};
        SudokuGrid gridOne = new SudokuGrid(easy);
        boolean result;
        result = gridOne.checkIfGridIsFull();

        assertFalse(result);
    }

    @Test
    void checkIfGridIsFullResultTrue() {
        int[][] easy = {
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4},
                {1, 2, 3, 4, 1, 7, 2, 3, 4}};
        SudokuGrid gridOne = new SudokuGrid(easy);
        boolean result;
        result = gridOne.checkIfGridIsFull();

        assertTrue(result);
    }
}