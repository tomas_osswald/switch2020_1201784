package array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {


    @Test
    void ConstructorWithNoArgumentsEmptyVector() {
        assertDoesNotThrow(() -> {
            Vector newVector = new Vector();
            assertTrue(newVector.isEmpty());
        });
    }

    @Test
    void ConstructorWithArrayNullException() {
        int[] numbers = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Vector newVector = new Vector(numbers);
        }, "Array should not be null");
    }

    @Test
    void ConstructorWithArrayEmpty() {
        int[] numbers = {};
        assertDoesNotThrow(() -> {
            Vector newVector = new Vector(numbers);
            assertTrue(newVector.isEmpty());
        });
    }

    @Test
    void ConstructorWithArray() {
        int[] numbers = {1, 2, 3};
        int expected = 3;
        assertDoesNotThrow(() -> {
            Vector newVector = new Vector(numbers);
            int result = newVector.getNumberOfElements();
            assertEquals(expected, result);
        });
    }

    @Test
    void ConstructorWithVectorNullException() {
        Vector initialVector = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Vector newVector = new Vector(initialVector);
        }, "Vector should not be null");
    }

    @Test
    void ConstructorWithVectorEmpty() {
        int[] numbers = {};
        Vector initialVector = new Vector(numbers);
        assertDoesNotThrow(() -> {
            Vector newVector = new Vector(initialVector);
            assertTrue(newVector.isEmpty());
        });
    }

    @Test
    void ConstructorWithVector() {
        int[] numbers = {1, 2, 3, 4};
        Vector initialVector = new Vector(numbers);
        int expected = 4;
        assertDoesNotThrow(() -> {
            Vector newVector = new Vector(initialVector);
            int result = newVector.getNumberOfElements();
            assertEquals(expected, result);
        });
    }

    @Test
    void testEqualsSameObject() {
        int[] numbers = {1, 2, 3, 4};
        Vector vectorOne = new Vector(numbers);
        assertEquals(vectorOne, vectorOne);
        assertSame(vectorOne, vectorOne);
    }

    @Test
    void testEqualsSameElements() {
        int[] numbers = {1, 2, 3, 4};
        Vector vectorOne = new Vector(numbers);
        Vector vectorTwo = new Vector(numbers);
        assertEquals(vectorOne, vectorTwo);
        assertNotSame(vectorOne, vectorTwo);
    }

    @Test
    void testEqualsDifferentElements() {
        int[] numbers = {1, 2, 3, 4};
        int number = 5;
        Vector vectorOne = new Vector(numbers);
        Vector vectorTwo = new Vector(numbers);
        vectorTwo.append(number);
        assertNotEquals(vectorOne, vectorTwo);
    }

    @Test
    void testEqualsDifferentObjects() {
        int[] numbers = {1, 2, 3, 4};
        Vector vectorOne = new Vector(numbers);
        Object vectorTwo = new Object();
        assertNotEquals(vectorOne, vectorTwo);
    }

    @Test
    void appendToVector() {
        int[] numbersOne = {1, 2, 3, 4};
        int number = 5;
        int[] numbersTwo = {1, 2, 3, 4, 5};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.append(number);
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void appendToEmptyVector() {
        int[] numbersOne = {};
        int number = 5;
        int[] numbersTwo = {5};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.append(number);
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void removeFirstOccurrenceOneOccurrence() {
        int[] numbersOne = {1, 2, 3, 4};
        int number = 5;
        int[] numbersTwo = {1, 2, 3, 4, 5};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorTwo.removeFirstOccurrence(number);
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void removeFirstOccurrenceMultipleOccurrence() {
        int[] numbersOne = {1, 2, 3, 4, 5, 4};
        int number = 5;
        int[] numbersTwo = {1, 2, 5, 3, 4, 5, 4};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorTwo.removeFirstOccurrence(number);
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void removeFirstOccurrenceNoOccurrence() {
        int[] numbersOne = {1, 2, 3, 4};
        int number = 5;
        int[] numbersTwo = {1, 2, 3, 4};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorTwo.removeFirstOccurrence(number);
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void existsInVectorTrue() {
        int[] numbersOne = {1, 2, 3, 4};
        int number = 4;
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.existsInVector(number);
        assertTrue(result);
    }

    @Test
    void existsInVectorFalse() {
        int[] numbersOne = {1, 2, 3, 4};
        int number = 5;
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.existsInVector(number);
        assertFalse(result);
    }

    @Test
    void existsInVectorFalseEmptyVector() {
        int[] numbersOne = {};
        int number = 5;
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.existsInVector(number);
        assertFalse(result);
    }

    @Test
    void existsInVectorTrueMultipleOccurrences() {
        int[] numbersOne = {5, 5, 5, 5};
        int number = 5;
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.existsInVector(number);
        assertTrue(result);
    }

    @Test
    void getValueAtIndexZero() {
        int[] numbersOne = {1, 2, 3, 4};
        int index = 0;
        int expected = 1;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getValueAtIndex(index);
        assertEquals(expected, result);
    }

    @Test
    void getValueAtIndexThree() {
        int[] numbersOne = {1, 2, 3, 4};
        int index = 3;
        int expected = 4;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getValueAtIndex(index);
        assertEquals(expected, result);
    }

    @Test
    void getValueAtIndexFourOutOfBounds() {
        int[] numbersOne = {1, 2, 3, 4};
        int index = 4;
        Vector vectorOne = new Vector(numbersOne);
        assertThrows(IndexOutOfBoundsException.class, () -> vectorOne.getValueAtIndex(index), "Index is out of bounds");
    }

    @Test
    void getNumberOfElementsFour() {
        int[] numbersOne = {1, 2, 3, 4};
        int expected = 4;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getNumberOfElements();
        assertEquals(expected, result);
    }

    @Test
    void getNumberOfElementsEmpty() {
        int[] numbersOne = {};
        int expected = 0;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getNumberOfElements();
        assertEquals(expected, result);
    }

    @Test
    void getLargestOneLargest() {
        int[] numbersOne = {1, 2, 3, 4};
        int expected = 4;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getLargest();
        assertEquals(expected, result);
    }

    @Test
    void getLargestManyEqualLargest() {
        int[] numbersOne = {1, 2, 3, 4, 4, 3, 4};
        int expected = 4;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getLargest();
        assertEquals(expected, result);
    }

    @Test
    void getLargestNegativeValues() {
        int[] numbersOne = {-1, -2, -3, -4, -4, -3, -4};
        int expected = -1;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getLargest();
        assertEquals(expected, result);
    }

    @Test
    void getLargestEmptyVector() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        assertThrows(IllegalArgumentException.class, vectorOne::getLargest);
    }

    @Test
    void getSmallestOneSmallest() {
        int[] numbersOne = {1, 2, 3, 4};
        int expected = 1;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getSmallest();
        assertEquals(expected, result);
    }

    @Test
    void getSmallestManyEqualSmallest() {
        int[] numbersOne = {1, 2, 3, 4, 4, 3, 4, 1, 1};
        int expected = 1;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getSmallest();
        assertEquals(expected, result);
    }

    @Test
    void getSmallestNegativeValues() {
        int[] numbersOne = {-1, -2, -3, -4, -4, -3, -4};
        int expected = -4;
        Vector vectorOne = new Vector(numbersOne);
        int result = vectorOne.getSmallest();
        assertEquals(expected, result);
    }

    @Test
    void getSmallestEmptyVector() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        assertThrows(IllegalArgumentException.class, vectorOne::getSmallest);
    }

    @Test
    void getAverageTwoPointFive() {
        int[] numbersOne = {1, 2, 3, 4};
        double expected = 2.5;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverage();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOnlyOneValue() {
        int[] numbersOne = {1};
        double expected = 1;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverage();
        assertEquals(expected, result);
    }

    @Test
    void getAverageNegativeValues() {
        int[] numbersOne = {-1, -2, -3, -4};
        double expected = -2.5;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverage();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfEvensNegativeThree() {
        int[] numbersOne = {-1, -2, -3, -4};
        double expected = -3;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfEvens();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfEvensOnlyEvens() {
        int[] numbersOne = {-2, 0, 2, 4};
        double expected = 1;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfEvens();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfEvensNoEvens() {
        int[] numbersOne = {1, 3, 5};
        double expected = 0;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfEvens();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfOddsTwo() {
        int[] numbersOne = {1, 2, 3, 4};
        double expected = 2;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfOdds();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfOddsOnlyOdds() {
        int[] numbersOne = {1, 3, 5, 7, 11};
        double expected = 5.4;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfOdds();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfOddsNoOdds() {
        int[] numbersOne = {2, 4};
        double expected = 0;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfOdds();
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfMultiplesOnlyMultiples() {
        int[] numbersOne = {2, 4};
        int value = 2;
        double expected = 3;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfMultiples(value);
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfMultiplesSomeMultiples() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int value = 3;
        double expected = 6;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfMultiples(value);
        assertEquals(expected, result);
    }

    @Test
    void getAverageOfMultiplesNoMultiples() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int value = 11;
        double expected = 0;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageOfMultiples(value);
        assertEquals(expected, result);
    }

    @Test
    void sortAscendingSortedDescending() {
        int[] numbersOne = {6, 5, 4, 3, 2, 1};
        int[] numbersTwo = {1, 2, 3, 4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortAscending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortAscendingNotSorted() {
        int[] numbersOne = {4, 5, 3, 2, 6, 1};
        int[] numbersTwo = {1, 2, 3, 4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortAscending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortAscendingAlreadySorted() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6};
        int[] numbersTwo = {1, 2, 3, 4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortAscending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortAscendingEmptyVector() {
        int[] numbersOne = {};
        int[] numbersTwo = {};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortAscending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortAscendingOneElementVector() {
        int[] numbersOne = {1};
        int[] numbersTwo = {1};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortAscending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortDescendingSortedAscending() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6};
        int[] numbersTwo = {6, 5, 4, 3, 2, 1};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortDescending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortDescendingNotSorted() {
        int[] numbersOne = {4, 5, 3, 2, 6, 1};
        int[] numbersTwo = {6, 5, 4, 3, 2, 1};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortDescending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortDescendingAlreadySorted() {
        int[] numbersOne = {6, 5, 4, 3, 2, 1};
        int[] numbersTwo = {6, 5, 4, 3, 2, 1};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortDescending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortDescendingEmptyVector() {
        int[] numbersOne = {};
        int[] numbersTwo = {};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortDescending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void sortDescendingOneElementVector() {
        int[] numbersOne = {1};
        int[] numbersTwo = {1};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.sortDescending();
        assertEquals(vectorOne, vectorTwo);
    }

    @Test
    void isEmptyTrue() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.isEmpty();
        assertTrue(result);
    }

    @Test
    void isEmptyFalse() {
        int[] numbersOne = {1};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.isEmpty();
        assertFalse(result);
    }

    @Test
    void hasOnlyOneElementTrue() {
        int[] numbersOne = {1};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyOneElement();
        assertTrue(result);
    }

    @Test
    void hasOnlyOneElementFalseNoElements() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyOneElement();
        assertFalse(result);
    }

    @Test
    void hasOnlyOneElementFalseManyElements() {
        int[] numbersOne = {1, 2, 3, 4, 5};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyOneElement();
        assertFalse(result);
    }

    @Test
    void hasOnlyEvenElementsTrueOneElement() {
        int[] numbersOne = {2};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyEvenElements();
        assertTrue(result);
    }

    @Test
    void hasOnlyEvenElementsTrueManyElement() {
        int[] numbersOne = {2, 4, 6};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyEvenElements();
        assertTrue(result);
    }

    @Test
    void hasOnlyEvenElementsFalseOneElement() {
        int[] numbersOne = {1};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyEvenElements();
        assertFalse(result);
    }

    @Test
    void hasOnlyEvenElementsFalseManyElement() {
        int[] numbersOne = {2, 4, 5};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyEvenElements();
        assertFalse(result);
    }

    @Test
    void hasOnlyEvenElementsExceptionEmpty() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        assertThrows(IllegalArgumentException.class, vectorOne::hasOnlyEvenElements);
    }

    @Test
    void hasOnlyOddElementsTrueOneElement() {
        int[] numbersOne = {1};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyOddElements();
        assertTrue(result);
    }

    @Test
    void hasOnlyOddElementsTrueManyElement() {
        int[] numbersOne = {1, 3, 5, 7};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyOddElements();
        assertTrue(result);
    }

    @Test
    void hasOnlyOddElementsFalseOneElement() {
        int[] numbersOne = {2};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyOddElements();
        assertFalse(result);
    }

    @Test
    void hasOnlyOddElementsFalseManyElement() {
        int[] numbersOne = {2, 4, 5};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasOnlyEvenElements();
        assertFalse(result);
    }

    @Test
    void hasOnlyOddElementsExceptionEmpty() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        assertThrows(IllegalArgumentException.class, vectorOne::hasOnlyOddElements);
    }

    @Test
    void hasDuplicatesTrueOnlyDuplicates() {
        int[] numbersOne = {2, 2, 4, 4};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasDuplicates();
        assertTrue(result);
    }

    @Test
    void hasDuplicatesTrueOneDuplicates() {
        int[] numbersOne = {1, 2, 3, 3};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasDuplicates();
        assertTrue(result);
    }

    @Test
    void hasDuplicatesFalseNoDuplicates() {
        int[] numbersOne = {1, 2, 3};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasDuplicates();
        assertFalse(result);
    }

    @Test
    void hasDuplicatesFalseEmpty() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        boolean result = vectorOne.hasDuplicates();
        assertFalse(result);
    }

    @Test
    void countDigitsOneDigit() {
        int element = 1;
        int expected = 1;
        int result = Vector.countDigits(element);
        assertEquals(result, expected);
    }

    @Test
    void countDigitsManyDigits() {
        int element = 1234;
        int expected = 4;
        int result = Vector.countDigits(element);
        assertEquals(result, expected);
    }

    @Test
    void getAverageDigitsOne() {
        int[] numbersOne = {1, 2, 3, 3};
        double expected = 1;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageDigits();
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getAverageDigitsOnePointTwoFive() {
        int[] numbersOne = {12, 2, 3, 3};
        double expected = 1.25;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageDigits();
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getAverageDigitsEmpty() {
        int[] numbersOne = {};
        double expected = 0;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageDigits();
        assertEquals(result, expected);
    }

    @Test
    void copyVector() {
        int[] numbersOne = {1, 2, 3, 3};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = vectorOne.copyVector();
        assertEquals(vectorOne, vectorTwo);
        assertEquals(vectorOne.getNumberOfElements(), vectorTwo.getNumberOfElements());
        assertNotSame(vectorOne, vectorTwo);
    }

    @Test
    void getElementsWithDigitsAboveAverageOneElement() {
        int[] numbersOne = {12, 2, 3, 3};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {12};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithDigitsAboveAverage();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsAboveAverageNoElements() {
        int[] numbersOne = {1, 2, 3, 3};
        Vector vectorOne = new Vector(numbersOne);
        Vector expected = new Vector();
        Vector result = vectorOne.getElementsWithDigitsAboveAverage();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsAboveAverageManyElements() {
        int[] numbersOne = {12, 22, 32, 3};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {12, 22, 32};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithDigitsAboveAverage();
        assertEquals(result, expected);
    }

    @Test
    void countEvenDigitsOneEvenDigit() {
        int element = 12;
        int expected = 1;
        int result = Vector.countEvenDigits(element);
        assertEquals(result, expected);
    }

    @Test
    void countEvenDigitsManyEvenDigit() {
        int element = 2121;
        int expected = 2;
        int result = Vector.countEvenDigits(element);
        assertEquals(result, expected);
    }

    @Test
    void countEvenDigitsNoEvenDigit() {
        int element = 1;
        int expected = 0;
        int result = Vector.countEvenDigits(element);
        assertEquals(result, expected);
    }

    @Test
    void getEvenDigitPercentageOfElementResultPointFive() {
        int element = 1254;
        double expected = 0.5;
        double result = Vector.getEvenDigitPercentageOfElement(element);
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getEvenDigitPercentageOfElementResultOne() {
        int element = 222;
        double expected = 1;
        double result = Vector.getEvenDigitPercentageOfElement(element);
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getEvenDigitPercentageOfElementResultZero() {
        int element = 1111;
        double expected = 0;
        double result = Vector.getEvenDigitPercentageOfElement(element);
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getAverageEvenDigitPercentagePointThreeSevenFive() {
        int[] numbersOne = {12, 2, 3, 3};
        double expected = 0.375;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageEvenDigitPercentage();
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getAverageEvenDigitPercentageOnlyEvenDigits() {
        int[] numbersOne = {22, 2, 4, 46};
        double expected = 1;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageEvenDigitPercentage();
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getAverageEvenDigitPercentageNoEvenDigits() {
        int[] numbersOne = {35, 7, 79, 1};
        double expected = 0;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageEvenDigitPercentage();
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getAverageEvenDigitPercentageEmpty() {
        int[] numbersOne = {};
        double expected = 0;
        Vector vectorOne = new Vector(numbersOne);
        double result = vectorOne.getAverageEvenDigitPercentage();
        assertEquals(result, expected, 0.01);
    }

    @Test
    void getElementsWithEvenDigitPercentageAboveAverageSomeElements() {
        int[] numbersOne = {111, 222, 32, 3};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {222, 32};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithEvenDigitPercentageAboveAverage();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithEvenDigitPercentageAboveAverageEmpty() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithEvenDigitPercentageAboveAverage();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithEvenDigitPercentageAboveAverageNoElements() {
        int[] numbersOne = {2, 4, 6, 8, 22};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithEvenDigitPercentageAboveAverage();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithOnlyEvenDigitsOnlyEvenDigitElements() {
        int[] numbersOne = {2, 4, 6, 8, 22};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {2, 4, 6, 8, 22};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithOnlyEvenDigits();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithOnlyEvenDigitsSomeEvenDigitElements() {
        int[] numbersOne = {864, 1, 8, 3, 24};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {864, 8, 24};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithOnlyEvenDigits();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithOnlyEvenDigitsNoEvenDigitElements() {
        int[] numbersOne = {1, 3, 5314, 17, 93};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithOnlyEvenDigits();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceOnlyElementsIncreasing() {
        int[] numbersOne = {12, 34, 56, 1234, 789};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {12, 34, 56, 1234, 789};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithDigitsInIncreasingSequence();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceSomeElementsIncreasing() {
        int[] numbersOne = {12, 21, 2, 1, 34, 56, 54321, 1234, 789};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {12, 34, 56, 1234, 789};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithDigitsInIncreasingSequence();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceNoElementsIncreasing() {
        int[] numbersOne = {21, 2, 1, 54321};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getElementsWithDigitsInIncreasingSequence();
        assertEquals(result, expected);
    }

    @Test
    void getPalindromesOnlyPalindromeElements() {
        int[] numbersOne = {1, 2, 11, 131, 1001, 940049};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {1, 2, 11, 131, 1001, 940049};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getPalindromes();
        assertEquals(result, expected);
    }

    @Test
    void getPalindromesSomePalindromeElements() {
        int[] numbersOne = {1, 19, 12, 41, 2, 11, 131, 1001, 764, 12369, 940049};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {1, 2, 11, 131, 1001, 940049};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getPalindromes();
        assertEquals(result, expected);
    }

    @Test
    void getPalindromesNoPalindromeElements() {
        int[] numbersOne = {19, 12, 41, 764, 12369};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getPalindromes();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithSameDigitsOnlySameDigitElements() {
        int[] numbersOne = {11, 2, 44, 55, 3333};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {11, 2, 44, 55, 3333};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getPalindromes();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithSameDigitsSomeSameDigitElements() {
        int[] numbersOne = {13, 2, 445, 55, 23333};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {2, 55};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getPalindromes();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithSameDigitsNoSameDigitElements() {
        int[] numbersOne = {13, 24, 445, 655, 23333};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getPalindromes();
        assertEquals(result, expected);
    }

    @Test
    void getArmstrongElementsOnlyArmstrongElements() {
        int[] numbersOne = {8, 9, 153, 370, 371};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {8, 9, 153, 370, 371};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getArmstrongElements();
        assertEquals(result, expected);
    }

    @Test
    void getArmstrongElementsSomeArmstrongElements() {
        int[] numbersOne = {8, 13, 9, 153, 371, 6412};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {8, 9, 153, 371};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getArmstrongElements();
        assertEquals(result, expected);
    }

    @Test
    void getArmstrongElementsNoArmstrongElements() {
        int[] numbersOne = {54, 32, 92, 1487, 21};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        Vector result = vectorOne.getArmstrongElements();
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceOfSizeSomeElements() {
        int[] numbersOne = {12, 23, 34, 123, 234, 456, 348};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {123, 234, 456, 348};
        Vector expected = new Vector(numbersTwo);
        int size = 3;
        Vector result = vectorOne.getElementsWithDigitsInIncreasingSequenceOfSize(size);
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceOfSizeAllElements() {
        int[] numbersOne = {123, 234, 456, 348};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {123, 234, 456, 348};
        Vector expected = new Vector(numbersTwo);
        int size = 3;
        Vector result = vectorOne.getElementsWithDigitsInIncreasingSequenceOfSize(size);
        assertEquals(result, expected);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceOfSizeNoElements() {
        int[] numbersOne = {12, 23, 34, 123, 234, 456, 348};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector expected = new Vector(numbersTwo);
        int size = 4;
        Vector result = vectorOne.getElementsWithDigitsInIncreasingSequenceOfSize(size);
        assertEquals(result, expected);
    }

    @Test
    void getSumManyElements() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        int expected = 45;
        int result = vectorOne.getSum();
        assertEquals(result, expected);
    }

    @Test
    void getSumOneElement() {
        int[] numbersOne = {1};
        Vector vectorOne = new Vector(numbersOne);
        int expected = 1;
        int result = vectorOne.getSum();
        assertEquals(result, expected);
    }

    @Test
    void getSumNoElements() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        int expected = 0;
        int result = vectorOne.getSum();
        assertEquals(result, expected);
    }

    @Test
    void getIndexOfFirstOccurrenceResultMinusOneNoOccurrence() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        int value = 10;
        int expected = -1;
        int result = vectorOne.getIndexOfFirstOccurrence(value);
        assertEquals(result, expected);
    }

    @Test
    void getIndexOfFirstOccurrenceResultOneOccurrence() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        int value = 2;
        int expected = 1;
        int result = vectorOne.getIndexOfFirstOccurrence(value);
        assertEquals(result, expected);
    }

    @Test
    void getIndexOfFirstOccurrenceResultManyOccurrences() {
        int[] numbersOne = {1, 2, 3, 2, 5, 6, 2, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        int value = 2;
        int expected = 1;
        int result = vectorOne.getIndexOfFirstOccurrence(value);
        assertEquals(result, expected);
    }


    @Test
    void getNumberOfOccurrencesNoOccurrences() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        int value = 10;
        int expected = 0;
        int result = vectorOne.getNumberOfOccurrences(value);
        assertEquals(result, expected);
    }

    @Test
    void getNumberOfOccurrencesOneOccurrence() {
        int[] numbersOne = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        int value = 4;
        int expected = 1;
        int result = vectorOne.getNumberOfOccurrences(value);
        assertEquals(result, expected);
    }

    @Test
    void getNumberOfOccurrencesManyOccurrences() {
        int[] numbersOne = {1, 2, 3, 4, 2, 2, 7, 2, 2};
        Vector vectorOne = new Vector(numbersOne);
        int value = 2;
        int expected = 5;
        int result = vectorOne.getNumberOfOccurrences(value);
        assertEquals(result, expected);
    }

    @Test
    void invertDifferentVectors() {
        int[] numbersOne = {5, 4, 3, 2, 3, 4, 4};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {4, 4, 3, 2, 3, 4, 5};
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.invert();
        assertEquals(vectorOne, vectorTwo);
        assertNotSame(vectorOne, vectorTwo);
    }

    @Test
    void invertEmptyVectors() {
        int[] numbersOne = {};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {};
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.invert();
        assertEquals(vectorOne, vectorTwo);
        assertNotSame(vectorOne, vectorTwo);
    }

    @Test
    void invertPalindromeVectors() {
        int[] numbersOne = {1, 2, 3, 2, 1};
        Vector vectorOne = new Vector(numbersOne);
        int[] numbersTwo = {1, 2, 3, 2, 1};
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.invert();
        assertEquals(vectorOne, vectorTwo);
        assertNotSame(vectorOne, vectorTwo);
    }

    @Test
    void setValueSuccess() {
        int[] numbersOne = {1, 2, 3, 2, 1};
        Vector vectorOne = new Vector(numbersOne);
        int value = 5;
        int index = 3;
        int[] numbersTwo = {1, 2, 3, 5, 1};
        Vector vectorTwo = new Vector(numbersTwo);
        vectorOne.setValue(value, index);
        assertEquals(vectorOne, vectorTwo);
        assertNotSame(vectorOne, vectorTwo);
    }

    @Test
    void setValueExceptionOutOfBounds() {
        int[] numbersOne = {1, 2, 3, 2, 1};
        Vector vectorOne = new Vector(numbersOne);
        int value = 5;
        int index = 7;
        int[] numbersTwo = {1, 2, 3, 5, 1};
        Vector vectorTwo = new Vector(numbersTwo);
        assertThrows(IndexOutOfBoundsException.class, () -> {
            vectorOne.setValue(value, index);
        });
    }
}