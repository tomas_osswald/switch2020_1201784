package array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test
    void ConstructorWithNoArgumentsEmptyMatrix() {
        assertDoesNotThrow(() -> {
            Matrix newMatrix = new Matrix();
            assertTrue(newMatrix.isEmpty());
        });
    }

    @Test
    void ConstructorWithArrayNullException() {
        int[][] numbers = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Matrix newMatrix = new Matrix(numbers);
        }, "Array should not be null");
    }

    @Test
    void ConstructorWithArrayEmpty() {
        int[][] numbers = {};
        assertDoesNotThrow(() -> {
            Matrix newMatrix = new Matrix(numbers);
            assertTrue(newMatrix.isEmpty());
        });
    }

    @Test
    void ConstructorWithArray() {
        int[][] numbers = {{1, 2, 3}, {1, 2, 3}};
        assertDoesNotThrow(() -> {
            Matrix newMatrix = new Matrix(numbers);
            Matrix otherMatrix = new Matrix(numbers);
            assertEquals(newMatrix, otherMatrix);
            assertNotSame(newMatrix, otherMatrix);
        });
    }

    @Test
    void ConstructorWithVectorNullException() {
        Vector[] initialVector = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Matrix newMatrix = new Matrix(initialVector);
        }, "Array should not be null");
    }

    @Test
    void ConstructorWithVectorEmpty() {
        int[] numbers = {};
        Vector vectorOne = new Vector(numbers);
        Vector vectorTwo = new Vector(numbers);
        Vector vectorThree = new Vector(numbers);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};

        assertDoesNotThrow(() -> {
            Matrix newMatrix = new Matrix(vectors);
            assertTrue(newMatrix.isEmpty());
        });
    }

    @Test
    void ConstructorWithVector() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};

        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);

        Vector[] vectorsOne = {vectorOne, vectorTwo, vectorThree};
        Vector[] vectorsTwo = {vectorOne, vectorTwo, vectorThree};

        assertDoesNotThrow(() -> {
            Matrix matrixOne = new Matrix(vectorsOne);
            Matrix matrixTwo = new Matrix(vectorsTwo);
            assertEquals(matrixOne, matrixTwo);
            assertNotSame(matrixOne, matrixTwo);
        });
    }


    @Test
    void appendExceptionLineOutOfBounds() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int value = 1;
        int line = 2;

        assertThrows(IndexOutOfBoundsException.class, () -> {
            matrixOne.append(value, line);
        });
    }

    @Test
    void appendEmptyMatrix() {
        int[] numbersOne = {1};
        Vector vectorOne = new Vector(numbersOne);
        Vector[] vectorsOne = {vectorOne};
        Matrix matrixOne = new Matrix(vectorsOne);

        int[] numbersTwo = {};
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsTwo = {vectorTwo};
        Matrix matrixTwo = new Matrix(vectorsTwo);

        int value = 1;
        int line = 0;

        assertDoesNotThrow(() -> {
            matrixTwo.append(value, line);
        });
        assertEquals(matrixOne, matrixTwo);
        assertNotSame(matrixOne, matrixTwo);
    }

    @Test
    void appendSuccess() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersExpected = {4, 5, 6, 7};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorExpected = new Vector(numbersExpected);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Vector[] vectorsTwo = {vectorOne, vectorExpected};
        Matrix matrixOne = new Matrix(vectorsOne);
        Matrix matrixTwo = new Matrix(vectorsTwo);

        int value = 7;
        int line = 1;
        matrixOne.append(value, line);

        assertEquals(matrixOne, matrixTwo);
        assertNotSame(matrixOne, matrixTwo);
    }

    @Test
    void removeFirstOccurrenceOneOccurrence() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersExpected = {4, 5};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorExpected = new Vector(numbersExpected);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Vector[] vectorsTwo = {vectorOne, vectorExpected};
        Matrix matrixOne = new Matrix(vectorsOne);
        Matrix matrixTwo = new Matrix(vectorsTwo);

        int value = 6;
        matrixOne.removeFirstOccurrence(value);

        assertEquals(matrixOne, matrixTwo);
        assertNotSame(matrixOne, matrixTwo);
    }

    @Test
    void removeFirstOccurrenceNoOccurrences() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);
        Matrix matrixTwo = new Matrix(vectorsOne);

        int value = 7;
        matrixOne.removeFirstOccurrence(value);

        assertEquals(matrixOne, matrixTwo);
        assertNotSame(matrixOne, matrixTwo);
    }

    @Test
    void isEmptyFalse() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        boolean result = matrixOne.isEmpty();
        assertFalse(result);
    }

    @Test
    void isEmptyTrue() {
        int[] numbersOne = {};
        int[] numbersTwo = {};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        boolean result = matrixOne.isEmpty();
        assertTrue(result);
    }

    @Test
    void getLargestResultSix() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int expected = 6;

        int result = matrixOne.getLargest();
        assertEquals(result, expected);
    }

    @Test
    void getLargestExceptionEmptyVector() {
        int[] numbersOne = {};
        int[] numbersTwo = {};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        assertThrows(IllegalArgumentException.class, matrixOne::getLargest);
    }

    @Test
    void getSmallestResultOne() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int expected = 1;

        int result = matrixOne.getSmallest();
        assertEquals(result, expected);
    }

    @Test
    void getSmallestExceptionEmptyVector() {
        int[] numbersOne = {};
        int[] numbersTwo = {};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        assertThrows(IllegalArgumentException.class, matrixOne::getSmallest);
    }

    @Test
    void getAverageTenPointFive() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        double expected = 3.5;
        double result = matrixOne.getAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAverageEmptyVector() {
        int[] numbersOne = {};
        int[] numbersTwo = {};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        double expected = 0;
        double result = matrixOne.getAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumOfEachRow() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int[] sumOfRows = {6, 15};
        Vector expected = new Vector(sumOfRows);
        Vector result = matrixOne.getSumOfEachRow();

        assertEquals(expected, result);
    }

    @Test
    void getSumOfEachColumn() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int[] sumOfColumns = {5, 7, 9};
        Vector expected = new Vector(sumOfColumns);
        Vector result = matrixOne.getSumOfEachColumn();

        assertEquals(expected, result);
    }

    @Test
    void getRowWithLargestSumOfElementsResultOne() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int expected = 1;
        int result = matrixOne.getRowWithLargestSumOfElements();

        assertEquals(expected, result);
    }

    @Test
    void getRowWithLargestSumOfElementsResultZero() {
        int[] numbersOne = {124, 412, 2};
        int[] numbersTwo = {4, 51, 62};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int expected = 0;
        int result = matrixOne.getRowWithLargestSumOfElements();

        assertEquals(expected, result);
    }

    @Test
    void isSquareTrueInputSizeTwoSquare() {
        int[] numbersOne = {1, 2};
        int[] numbersTwo = {4, 5};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectors = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSquare();

        assertTrue(result);
    }

    @Test
    void isSquareTrueInputSizeThreeSquare() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSquare();

        assertTrue(result);
    }

    @Test
    void isSquareFalseInputRectangle() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 3};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectors = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSquare();

        assertFalse(result);
    }

    @Test
    void isSquareFalseInputIrregular() {
        int[] numbersOne = {1, 2};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSquare();

        assertFalse(result);
    }

    @Test
    void isSymmetricResultTrueInputSizeThreeSquare() {
        int[] numbersOne = {1, 4, 7};
        int[] numbersTwo = {4, 5, 8};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSymmetric();

        assertTrue(result);
    }

    @Test
    void isSymmetricResultTrueInputSizeTwoSquare() {
        int[] numbersOne = {1, 4};
        int[] numbersTwo = {4, 5};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectors = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSymmetric();

        assertTrue(result);
    }

    @Test
    void isSymmetricResultFalseNotSymmetric() {
        int[] numbersOne = {1, 2};
        int[] numbersTwo = {4, 5};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectors = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSymmetric();

        assertFalse(result);
    }

    @Test
    void isSymmetricResultFalseNotSquare() {
        int[] numbersOne = {1, 2};
        int[] numbersTwo = {4};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectors = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isSymmetric();

        assertFalse(result);
    }

    @Test
    void getNonNullElementsInMainDiagonalNoNullElements() {
        int[] numbersOne = {1, 4, 7};
        int[] numbersTwo = {4, 5, 8};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int expected = 3;
        int result = matrixOne.getNonNullElementsInMainDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void getNonNullElementsInMainDiagonalSomeNullElements() {
        int[] numbersOne = {0, 4, 7};
        int[] numbersTwo = {4, 0, 8};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int expected = 1;
        int result = matrixOne.getNonNullElementsInMainDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void getNonNullElementsInMainDiagonalOnlyNullElements() {
        int[] numbersOne = {0, 4, 7};
        int[] numbersTwo = {4, 0, 8};
        int[] numbersThree = {7, 8, 0};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int expected = 0;
        int result = matrixOne.getNonNullElementsInMainDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void getNonNullElementsInMainDiagonalNotSquare() {
        int[] numbersOne = {0, 4, 7};
        int[] numbersTwo = {4, 0};
        int[] numbersThree = {7, 8, 0};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int expected = 1;
        int result = matrixOne.getNonNullElementsInMainDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void isMainDiagonalEqualSecondaryDiagonalResultTrue() {
        int[] numbersOne = {1, 4, 1};
        int[] numbersTwo = {4, 5, 8};
        int[] numbersThree = {9, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isMainDiagonalEqualSecondaryDiagonal();

        assertTrue(result);
    }

    @Test
    void isMainDiagonalEqualSecondaryDiagonalResultFalse() {
        int[] numbersOne = {1, 4, 1};
        int[] numbersTwo = {4, 5, 8};
        int[] numbersThree = {15, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        boolean result = matrixOne.isMainDiagonalEqualSecondaryDiagonal();

        assertFalse(result);
    }

    @Test
    void getElementsWithDigitsAboveAverage() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 25, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int[] sumOfRows = {25};
        Vector expected = new Vector(sumOfRows);
        Vector result = matrixOne.getElementsWithDigitsAboveAverage();

        assertEquals(expected, result);
    }

    @Test
    void getElementsWithEvenDigitPercentageAboveAverage() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 25, 6};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector[] vectorsOne = {vectorOne, vectorTwo};
        Matrix matrixOne = new Matrix(vectorsOne);

        int[] sumOfRows = {2, 4, 25, 6};
        Vector expected = new Vector(sumOfRows);
        Vector result = matrixOne.getElementsWithEvenDigitPercentageAboveAverage();

        assertEquals(expected, result);
    }

    @Test
    void invertRows() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int[] numbersFour = {3, 2, 1};
        int[] numbersFive = {6, 5, 4};
        int[] numbersSix = {9, 8, 7};
        Vector vectorFour = new Vector(numbersFour);
        Vector vectorFive = new Vector(numbersFive);
        Vector vectorSix = new Vector(numbersSix);
        Vector[] vectorsTwo = {vectorFour, vectorFive, vectorSix};
        Matrix matrixTwo = new Matrix(vectorsTwo);

        matrixOne.invertRows();

        assertEquals(matrixOne, matrixTwo);
    }

    @Test
    void invertColumns() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int[] numbersFour = {7, 8, 9};
        int[] numbersFive = {4, 5, 6};
        int[] numbersSix = {1, 2, 3};
        Vector vectorFour = new Vector(numbersFour);
        Vector vectorFive = new Vector(numbersFive);
        Vector vectorSix = new Vector(numbersSix);
        Vector[] vectorsTwo = {vectorFour, vectorFive, vectorSix};
        Matrix matrixTwo = new Matrix(vectorsTwo);

        matrixOne.invertColumns();

        assertEquals(matrixOne, matrixTwo);
    }

    @Test
    void transposeMatrix() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int[] numbersFour = {1, 4, 7};
        int[] numbersFive = {2, 5, 8};
        int[] numbersSix = {3, 6, 9};
        Vector vectorFour = new Vector(numbersFour);
        Vector vectorFive = new Vector(numbersFive);
        Vector vectorSix = new Vector(numbersSix);
        Vector[] vectorsTwo = {vectorFour, vectorFive, vectorSix};
        Matrix matrixTwo = new Matrix(vectorsTwo);

        matrixOne.transposeMatrix();

        assertEquals(matrixOne, matrixTwo);
    }

    @Test
    void rotateNinetyDegrees() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int[] numbersFour = {7, 4, 1};
        int[] numbersFive = {8, 5, 2};
        int[] numbersSix = {9, 6, 3};
        Vector vectorFour = new Vector(numbersFour);
        Vector vectorFive = new Vector(numbersFive);
        Vector vectorSix = new Vector(numbersSix);
        Vector[] vectorsTwo = {vectorFour, vectorFive, vectorSix};
        Matrix matrixTwo = new Matrix(vectorsTwo);

        matrixOne.rotateNinetyDegrees();

        assertEquals(matrixOne, matrixTwo);
    }

    @Test
    void rotateOneEightyDegrees() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int[] numbersFour = {9, 8, 7};
        int[] numbersFive = {6, 5, 4};
        int[] numbersSix = {3, 2, 1};
        Vector vectorFour = new Vector(numbersFour);
        Vector vectorFive = new Vector(numbersFive);
        Vector vectorSix = new Vector(numbersSix);
        Vector[] vectorsTwo = {vectorFour, vectorFive, vectorSix};
        Matrix matrixTwo = new Matrix(vectorsTwo);

        matrixOne.rotateOneEightyDegrees();

        assertEquals(matrixOne, matrixTwo);
    }

    @Test
    void rotateMinusNinetyDegrees() {
        int[] numbersOne = {1, 2, 3};
        int[] numbersTwo = {4, 5, 6};
        int[] numbersThree = {7, 8, 9};
        Vector vectorOne = new Vector(numbersOne);
        Vector vectorTwo = new Vector(numbersTwo);
        Vector vectorThree = new Vector(numbersThree);
        Vector[] vectors = {vectorOne, vectorTwo, vectorThree};
        Matrix matrixOne = new Matrix(vectors);

        int[] numbersFour = {3, 6, 9};
        int[] numbersFive = {2, 5, 8};
        int[] numbersSix = {1, 4, 7};
        Vector vectorFour = new Vector(numbersFour);
        Vector vectorFive = new Vector(numbersFive);
        Vector vectorSix = new Vector(numbersSix);
        Vector[] vectorsTwo = {vectorFour, vectorFive, vectorSix};
        Matrix matrixTwo = new Matrix(vectorsTwo);

        matrixOne.rotateMinusNinetyDegrees();

        assertEquals(matrixOne, matrixTwo);
    }
}