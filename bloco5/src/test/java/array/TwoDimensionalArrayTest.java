package array;

import array.TwoDimensionalArray;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TwoDimensionalArrayTest {

    @Test
    void ConstructorSuccessNoArguments() {
        assertDoesNotThrow(() -> {
            TwoDimensionalArray newVector = new TwoDimensionalArray();
        });
    }

    @Test
    void ConstructorThrowExceptionNull() {
        int[][] numbers = null;
        assertThrows(IllegalArgumentException.class, () -> {
            TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        }, "Integers array should not be null");
    }

    @Test
    void ConstructorSuccessNotNullArguments() {
        int[][] numbers = {{0}, {0}};
        assertDoesNotThrow(() -> {
            TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        });
    }

    @Test
    void addNumberFourToEmptyArrayRowZero() {
        int[][] numbers = {{}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int number = 4;
        int row = 0;

        newVector.add(number, row);

        Integer[][] expected = {{4}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void addNumberFourToEmptyArrayRowTwo() {
        int[][] numbers = {{}, {}, {}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int number = 4;
        int row = 2;

        newVector.add(number, row);

        Integer[][] expected = {{}, {}, {4}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void addNumberFourToArrayRowZero() {
        int[][] numbers = {{1, 2, 3}, {1, 2}, {4, 5}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int number = 4;
        int row = 0;

        newVector.add(number, row);

        Integer[][] expected = {{1, 2, 3, 4}, {1, 2}, {4, 5}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void addNumberFourToArrayRowTwo() {
        int[][] numbers = {{1, 2, 3}, {1, 2}, {4, 5}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int number = 4;
        int row = 2;

        newVector.add(number, row);

        Integer[][] expected = {{1, 2, 3}, {1, 2}, {4, 5, 4}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void addNumberFourToArrayRowOutOfBounds() {
        int[][] numbers = {{1, 2, 3}, {1, 2}, {4, 5}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int number = 4;
        int row = 3;

        assertThrows(IndexOutOfBoundsException.class, () -> newVector.add(number, row));
    }

    @Test
    void removeFirstOccurrenceNumberTwo() {
        int[][] numbers = {{1, 2, 3}, {1, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int number = 2;

        newVector.removeFirstOccurrenceNumber(number);

        Integer[][] expected = {{1, 3}, {1, 2, 3}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(newVector.removeFirstOccurrenceNumber(number));
    }

    @Test
    void removeFirstOccurrenceNumberNotFound() {
        int[][] numbers = {{1, 2, 3}, {1, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int number = 4;

        newVector.removeFirstOccurrenceNumber(number);

        Integer[][] expected = {{1, 2, 3}, {1, 2, 3}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
        assertFalse(newVector.removeFirstOccurrenceNumber(number));
    }

    @Test
    void isEmptyResultTrue() {
        int[][] numbers = {{}, {}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isEmpty();

        assertTrue(result);
    }

    @Test
    void isEmptyResultFalse() {
        int[][] numbers = {{1}, {}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isEmpty();

        assertFalse(result);
    }

    @Test
    void getLargestElementResultThree() {
        int[][] numbers = {{1, 2, 3}, {1, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Integer expected = 3;

        Integer result = newVector.getLargestElement();

        assertEquals(expected, result);
    }

    @Test
    void getLargestElementSingleElementArray() {
        int[][] numbers = {{1}, {}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Integer expected = 1;

        Integer result = newVector.getLargestElement();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestElementResultThree() {
        int[][] numbers = {{6, 5, 3}, {7, 8, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Integer expected = 3;

        Integer result = newVector.getSmallestElement();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestElementSingleElementArray() {
        int[][] numbers = {{5}, {}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Integer expected = 5;

        Integer result = newVector.getSmallestElement();

        assertEquals(expected, result);
    }

    @Test
    void getAverageResultTwo() {
        int[][] numbers = {{1, 2, 3}, {1, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        double expected = 2;

        double result = newVector.getAverage();

        assertEquals(expected, result, 0.1);
    }

    @Test
    void getAverageSingleElementArray() {
        int[][] numbers = {{1}, {}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        double expected = 1;

        double result = newVector.getAverage();

        assertEquals(expected, result, 0.1);
    }

    @Test
    void getAverageResultThreePointFive() {
        int[][] numbers = {{1, 2, 3}, {4, 5, 6}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        double expected = 3.5;

        double result = newVector.getAverage();

        assertEquals(expected, result, 0.1);
    }

    @Test
    void getSumOfEachRowEqualRowLength() {
        int[][] numbers = {{1, 2, 3}, {4, 5, 6}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {6, 15};

        Object[] result = newVector.getSumOfEachRow().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getSumOfEachRowDifferentRowLength() {
        int[][] numbers = {{1, 2, 3, 4}, {5, 6}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {10, 11};

        Object[] result = newVector.getSumOfEachRow().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getSumOfEachColumnEqualColumnLength() {
        int[][] numbers = {{1, 2, 3}, {4, 5, 6}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {5, 7, 9};

        Object[] result = newVector.getSumOfEachColumn().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getSumOfEachColumnDifferentColumnLength() {
        int[][] numbers = {{1, 2, 3, 4}, {5, 6}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {6, 8, 3, 4};

        Object[] result = newVector.getSumOfEachColumn().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getRowWithLargestSumOfElementsInputDifferentLengthRows() {
        int[][] numbers = {{1, 2, 3, 4}, {5, 6}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int expected = 1;

        int result = newVector.getRowWithLargestSumOfElements();

        assertEquals(expected, result);
    }

    @Test
    void getRowWithLargestSumOfElementsInputOnlyOneRow() {
        int[][] numbers = {{1, 2, 3, 4}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int expected = 0;

        int result = newVector.getRowWithLargestSumOfElements();

        assertEquals(expected, result);
    }

    @Test
    void getRowWithLargestSumOfElementsInputEmptyVector() {
        int[][] numbers = {{}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int expected = 0;

        int result = newVector.getRowWithLargestSumOfElements();

        assertEquals(expected, result);
    }

    @Test
    void getRowWithLargestSumOfElementsInputEqualSumRows() {
        int[][] numbers = {{1, 2, 3, 4}, {1, 2, 3, 4}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int expected = 0;

        int result = newVector.getRowWithLargestSumOfElements();

        assertEquals(expected, result);
    }

    @Test
    void isSquareMatrixInputEmpty() {
        int[][] numbers = {{}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isSquareMatrix();

        assertFalse(result);
    }

    @Test
    void isSquareMatrixInputOnlyOneElement() {
        int[][] numbers = {{1}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isSquareMatrix();

        assertTrue(result);
    }

    @Test
    void isSquareMatrixInputNotSquare() {
        int[][] numbers = {{1, 2}, {4}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isSquareMatrix();

        assertFalse(result);
    }

    @Test
    void isSquareMatrixInputSquare() {
        int[][] numbers = {{1, 2}, {4, 5}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isSquareMatrix();

        assertTrue(result);
    }

    @Test
    void isSymmetricMatrixInputNotSymmetric() {
        int[][] numbers = {{1, 2}, {4, 5}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isSymmetricMatrix();

        assertFalse(result);
    }

    @Test
    void isSymmetricMatrixInputSymmetric() {
        int[][] numbers = {{1, 2}, {2, 5}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isSymmetricMatrix();

        assertTrue(result);
    }

    @Test
    void isSymmetricMatrixInputNotSquare() {
        int[][] numbers = {{1, 2}, {2, 5, 2}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isSymmetricMatrix();

        assertFalse(result);
    }

    @Test
    void getNonNullElementsInMainDiagonalInputNotSquare() {
        int[][] numbers = {{1, 2, 3, 4}, {1, 2, 3, 4}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int expected = 1;

        int result = newVector.getNonNullElementsInMainDiagonal();

        assertEquals(expected, result);
    }

    @Test
    void getNonNullElementsInMainDiagonalInputNoNullElements() {
        int[][] numbers = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        int expected = 3;

        int result = newVector.getNonNullElementsInMainDiagonal();

        assertEquals(expected, result);
    }

    @Test
    void isMainDiagonalEqualSecondaryDiagonalInputNotSquare() {
        int[][] numbers = {{1, 2}, {2, 5, 2}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isMainDiagonalEqualSecondaryDiagonal();

        assertFalse(result);
    }

    @Test
    void isMainDiagonalEqualSecondaryDiagonalResultTrue() {
        int[][] numbers = {{1, 2, 1}, {4, 5, 6}, {3, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isMainDiagonalEqualSecondaryDiagonal();

        assertTrue(result);
    }

    @Test
    void isMainDiagonalEqualSecondaryDiagonalResultFalse() {
        int[][] numbers = {{1, 2, 3}, {4, 5, 6}, {3, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        boolean result = newVector.isMainDiagonalEqualSecondaryDiagonal();

        assertFalse(result);
    }

    @Test
    void getElementsWithDigitsAboveAverageResultOne() {
        int[][] numbers = {{1, 2, 3}, {5, 6}, {12}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {12};

        Object[] result = newVector.getElementsWithDigitsAboveAverage().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsAboveAverageResultNone() {
        int[][] numbers = {{1, 2, 3}, {5, 6}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {};

        Object[] result = newVector.getElementsWithDigitsAboveAverage().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithEvenDigitsPercentageAboveAverageResultOne() {
        int[][] numbers = {{1, 2, 3}, {5}, {1, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {2};

        Object[] result = newVector.getElementsWithEvenDigitsPercentageAboveAverage().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithEvenDigitsPercentageAboveAverageResultNone() {
        int[][] numbers = {{1, 3}, {5}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);
        Object[] expected = {};

        Object[] result = newVector.getElementsWithEvenDigitsPercentageAboveAverage().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void invertElementsInRowEqualLengthRows() {
        int[][] numbers = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        newVector.invertElementsInRow();

        Integer[][] expected = {{3, 2, 1}, {3, 2, 1}, {3, 2, 1}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void invertElementsInRowDifferentLengthRows() {
        int[][] numbers = {{1, 2}, {1, 2, 3}, {1}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        newVector.invertElementsInRow();

        Integer[][] expected = {{2, 1}, {3, 2, 1}, {1}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void invertElementsInColumnEqualLengthRows() {
        int[][] numbers = {{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        TwoDimensionalArray newVector = new TwoDimensionalArray(numbers);

        newVector.invertElementsInColumn();

        Integer[][] expected = {{3, 3, 3}, {2, 2, 2}, {1, 1, 1}};
        Integer[][] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }
}