package array;

import array.Array;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTest {

    @Test
    void ConstructorSuccessNoArguments() {
        assertDoesNotThrow(() -> {
            Array newVector = new Array();
        });
    }

    @Test
    void ConstructorThrowExceptionNull() {
        int[] numbers = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Array newVector = new Array(numbers);
        }, "Integers array should not be null");
    }

    @Test
    void ConstructorSuccessNotNullArguments() {
        int[] numbers = {0};
        assertDoesNotThrow(() -> {
            Array newVector = new Array(numbers);
        });
    }

    @Test
    void addNumberFourToEmptyArray() {
        int[] numbers = {};
        Array newVector = new Array(numbers);
        int number = 4;

        newVector.add(number);

        Object[] expected = {4};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void addNumberFourToArray() {
        int[] numbers = {1, 2, 3};
        Array newVector = new Array(numbers);
        int number = 4;

        newVector.add(number);

        Object[] expected = {1, 2, 3, 4};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void removeFirstOccurrenceNumberOne() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        int number = 1;

        newVector.removeFirstOccurrenceNumber(number);

        Object[] expected = {2, 3, 1, 2, 3};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void removeFirstOccurrenceNumberTwo() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        int number = 2;

        newVector.removeFirstOccurrenceNumber(number);

        Object[] expected = {1, 3, 1, 2, 3};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void removeFirstOccurrenceNumberNotInArray() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        int number = 4;

        newVector.removeFirstOccurrenceNumber(number);

        Object[] expected = {1, 2, 3, 1, 2, 3};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementAtIndexFour() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        int index = 4;
        Integer expected = 2;

        Integer result = newVector.getElementAtIndex(index);

        assertEquals(expected, result);
    }

    @Test
    void getElementAtIndexZero() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        int index = 0;
        Integer expected = 1;

        Integer result = newVector.getElementAtIndex(index);

        assertEquals(expected, result);
    }

    @Test
    void getElementAtIndexOutOfBounds() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        int index = 6;

        assertThrows(IndexOutOfBoundsException.class, () -> {
            newVector.getElementAtIndex(index);
        });
    }

    @Test
    void getSizeZero() {
        int[] numbers = {};
        Array newVector = new Array(numbers);
        int expected = 0;

        int result = newVector.getSize();

        assertEquals(expected, result);
    }

    @Test
    void getSizeOne() {
        int[] numbers = {1};
        Array newVector = new Array(numbers);
        int expected = 1;

        int result = newVector.getSize();

        assertEquals(expected, result);
    }

    @Test
    void getSizeSix() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        int expected = 6;

        int result = newVector.getSize();

        assertEquals(expected, result);
    }

    @Test
    void getLargestElementThree() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        Integer expected = 3;

        Integer result = newVector.getLargestElement();

        assertEquals(expected, result);
    }

    @Test
    void getLargestElementOne() {
        int[] numbers = {1, -2, -3, -1, -2, -3};
        Array newVector = new Array(numbers);
        Integer expected = 1;

        Integer result = newVector.getLargestElement();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestElementOne() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        Integer expected = 1;

        Integer result = newVector.getSmallestElement();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestElementThree() {
        int[] numbers = {5, 24, 3, 2145, 4, 10};
        Array newVector = new Array(numbers);
        Integer expected = 3;

        Integer result = newVector.getSmallestElement();

        assertEquals(expected, result);
    }

    @Test
    void getSmallestElementNullResultIndexOutOfBounds() {
        int[] numbers = {};
        Array newVector = new Array(numbers);

        assertThrows(IndexOutOfBoundsException.class, () -> {
            newVector.getSmallestElement();
        });
    }

    @Test
    void getAverageResultTwo() {
        int[] numbers = {1, 2, 3, 1, 2, 3};
        Array newVector = new Array(numbers);
        Integer expected = 2;

        Integer result = newVector.getAverage();

        assertEquals(expected, result);
    }

    @Test
    void getAverageResultOne() {
        int[] numbers = {1};
        Array newVector = new Array(numbers);
        Integer expected = 1;

        Integer result = newVector.getAverage();

        assertEquals(expected, result);
    }

    @Test
    void getAverageResultThree() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        Integer expected = 3;

        Integer result = newVector.getAverage();

        assertEquals(expected, result);
    }

    @Test
    void getEvensAverageResultFour() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        Integer expected = 4;

        Integer result = newVector.getEvensAverage();

        assertEquals(expected, result);
    }

    @Test
    void getEvensAverageResultZeroNoEvens() {
        int[] numbers = {1,3,5};
        Array newVector = new Array(numbers);
        Integer expected = 0;

        Integer result = newVector.getEvensAverage();

        assertEquals(expected, result);
    }

    @Test
    void getOddsAverageResultThree() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        Integer expected = 3;

        Integer result = newVector.getOddsAverage();

        assertEquals(expected, result);
    }

    @Test
    void getOddsAverageResultZeroNoOdds() {
        int[] numbers = {2,4,6};
        Array newVector = new Array(numbers);
        Integer expected = 0;

        Integer result = newVector.getOddsAverage();

        assertEquals(expected, result);
    }

    @Test
    void getAverageOfMultiplesResultFourMultiplesTwo() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        int number = 2;
        Integer expected = 4;

        Integer result = newVector.getAverageOfMultiples(number);

        assertEquals(expected, result);
    }

    @Test
    void getAverageOfMultiplesResultSixMultiplesSix() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        int number = 6;
        Integer expected = 6;

        Integer result = newVector.getAverageOfMultiples(number);

        assertEquals(expected, result);
    }

    @Test
    void getAverageOfMultiplesResultZeroNoMultiples() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        int number = 7;
        Integer expected = 0;

        Integer result = newVector.getAverageOfMultiples(number);

        assertEquals(expected, result);
    }

    @Test
    void sortAscendingInputAlreadySorted() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);

        newVector.sortAscending();
        Object[] expected = {1,2,3,4,5,6};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortAscendingInputNotSorted() {
        int[] numbers = {2,4,6,5,3,1};
        Array newVector = new Array(numbers);

        newVector.sortAscending();
        Object[] expected = {1,2,3,4,5,6};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortDescendingInputAlreadySorted() {
        int[] numbers = {6,5,4,3,2,1};
        Array newVector = new Array(numbers);

        newVector.sortDescending();
        Object[] expected = {6,5,4,3,2,1};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortDescendingInputNotSorted() {
        int[] numbers = {2,4,6,5,3,1};
        Array newVector = new Array(numbers);

        newVector.sortDescending();
        Object[] expected = {6,5,4,3,2,1};
        Object[] result = newVector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void isEmptyResultTrue() {
        int[] numbers = {};
        Array newVector = new Array(numbers);

        boolean result = newVector.isEmpty();

        assertTrue(result);
    }

    @Test
    void isEmptyResultFalse() {
        int[] numbers = {1};
        Array newVector = new Array(numbers);

        boolean result = newVector.isEmpty();

        assertFalse(result);
    }

    @Test
    void hasOneElementResultTrue() {
        int[] numbers = {5};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOneElement();

        assertTrue(result);
    }

    @Test
    void hasOneElementResultFalseInputZeroElements() {
        int[] numbers = {};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOneElement();

        assertFalse(result);
    }

    @Test
    void hasOneElementResultFalseInputManyElements() {
        int[] numbers = {1,2,3,4,5,6,7};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOneElement();

        assertFalse(result);
    }

    @Test
    void hasOnlyEvenElementsResultTrue() {
        int[] numbers = {2,4,6};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOnlyEvenElements();

        assertTrue(result);
    }

    @Test
    void hasOnlyEvenElementsResultFalseInputOneOdd() {
        int[] numbers = {2,4,6,1};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOnlyEvenElements();

        assertFalse(result);
    }

    @Test
    void hasOnlyEvenElementsResultFalseInputOnlyOdds() {
        int[] numbers = {1,3,5};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOnlyEvenElements();

        assertFalse(result);
    }

    @Test
    void hasOnlyOddElementsResultTrue() {
        int[] numbers = {1,3,5};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOnlyOddElements();

        assertTrue(result);
    }

    @Test
    void hasOnlyOddElementsResultFalseInputOneEven() {
        int[] numbers = {1,3,5,2};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOnlyOddElements();

        assertFalse(result);
    }

    @Test
    void hasOnlyOddElementsResultFalseInputOnlyEvens() {
        int[] numbers = {2,4,6};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasOnlyOddElements();

        assertFalse(result);
    }

    @Test
    void hasDuplicatesResultTrueOneDuplicate() {
        int[] numbers = {2,4,6,2};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasDuplicates();

        assertTrue(result);
    }

    @Test
    void hasDuplicatesResultTrueManyDuplicate() {
        int[] numbers = {2,4,6,2,4,6,8};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasDuplicates();

        assertTrue(result);
    }

    @Test
    void hasDuplicatesResultFalseNoDuplicates() {
        int[] numbers = {2,4,6};
        Array newVector = new Array(numbers);

        boolean result = newVector.hasDuplicates();

        assertFalse(result);
    }

    @Test
    void getElementsWithDigitsAboveAverageResultTen() {
        int[] numbers = {1,2,3,4,5,6,10};
        Array newVector = new Array(numbers);
        Object[] expected = {10};

        Array aboveAverage = newVector.getElementsWithDigitsAboveAverage();

        Object[] result = aboveAverage.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsAboveAverageResultTenOneHundred() {
        int[] numbers = {1,2,3,4,5,6,10,100};
        Array newVector = new Array(numbers);
        Object[] expected = {10,100};

        Array aboveAverage = newVector.getElementsWithDigitsAboveAverage();

        Object[] result = aboveAverage.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsAboveAverageResultNoElements() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        Object[] expected = {};

        Array aboveAverage = newVector.getElementsWithDigitsAboveAverage();

        Object[] result = aboveAverage.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithEvenDigitsPercentageAboveAverageResultTwentyTwo() {
        int[] numbers = {121,343,555,22};
        Array newVector = new Array(numbers);
        Object[] expected = {22};

        Array aboveAverage = newVector.getElementsWithEvenDigitsPercentageAboveAverage();

        Object[] result = aboveAverage.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithEvenDigitsPercentageAboveAverageResultTwoFourSix() {
        int[] numbers = {121,343,555,2,4,6};
        Array newVector = new Array(numbers);
        Object[] expected = {2,4,6};

        Array aboveAverage = newVector.getElementsWithEvenDigitsPercentageAboveAverage();

        Object[] result = aboveAverage.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithEvenDigitsPercentageAboveAverageResultTwo() {
        int[] numbers = {1,3,5,7,2};
        Array newVector = new Array(numbers);
        Object[] expected = {2};

        Array aboveAverage = newVector.getElementsWithEvenDigitsPercentageAboveAverage();

        Object[] result = aboveAverage.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithOnlyEvenDigitsResultTwoFourSix() {
        int[] numbers = {1,2,3,4,5,6};
        Array newVector = new Array(numbers);
        Object[] expected = {2,4,6};

        Array onlyEvens = newVector.getElementsWithOnlyEvenDigits();

        Object[] result = onlyEvens.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithOnlyEvenDigitsInputNoEvens() {
        int[] numbers = {1,3,5,7};
        Array newVector = new Array(numbers);
        Object[] expected = {};

        Array onlyEvens = newVector.getElementsWithOnlyEvenDigits();

        Object[] result = onlyEvens.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithOnlyEvenDigitsInputAllEvens() {
        int[] numbers = {2,4,6,8};
        Array newVector = new Array(numbers);
        Object[] expected = {2,4,6,8};

        Array onlyEvens = newVector.getElementsWithOnlyEvenDigits();

        Object[] result = onlyEvens.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceResultEmpty() {
        int[] numbers = {1,2,3};
        Array newVector = new Array(numbers);
        Object[] expected = {};

        Array increasingSequence = newVector.getElementsWithDigitsInIncreasingSequence();

        Object[] result = increasingSequence.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceInputOneInIncreasingSequence() {
        int[] numbers = {1,2,3,123};
        Array newVector = new Array(numbers);
        Object[] expected = {123};

        Array increasingSequence = newVector.getElementsWithDigitsInIncreasingSequence();

        Object[] result = increasingSequence.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceInputAllNumbersInIncreasingSequence() {
        int[] numbers = {12,345,23,1234};
        Array newVector = new Array(numbers);
        Object[] expected = {12,345,23,1234};

        Array increasingSequence = newVector.getElementsWithDigitsInIncreasingSequence();

        Object[] result = increasingSequence.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getPalindromesInputNoPalindromes() {
        int[] numbers = {14,21,43};
        Array newVector = new Array(numbers);
        Object[] expected = {};

        Array palindromes = newVector.getPalindromes();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getPalindromesInputOnePalindromes() {
        int[] numbers = {1,21,43};
        Array newVector = new Array(numbers);
        Object[] expected = {1};

        Array palindromes = newVector.getPalindromes();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getPalindromesInputOnlyPalindromes() {
        int[] numbers = {1,2,3,4,11,101,121};
        Array newVector = new Array(numbers);
        Object[] expected = {1,2,3,4,11,101,121};

        Array palindromes = newVector.getPalindromes();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithSameDigitsInputNone() {
        int[] numbers = {14,21,43};
        Array newVector = new Array(numbers);
        Object[] expected = {};

        Array palindromes = newVector.getElementsWithSameDigits();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithSameDigitsInputOneNumberWithSameDigits() {
        int[] numbers = {14,21,43,22};
        Array newVector = new Array(numbers);
        Object[] expected = {22};

        Array palindromes = newVector.getElementsWithSameDigits();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithSameDigitsInputOnlyNumbersWithSameDigits() {
        int[] numbers = {1,11,111,2222};
        Array newVector = new Array(numbers);
        Object[] expected = {1,11,111,2222};

        Array palindromes = newVector.getElementsWithSameDigits();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getArmstrongElementsInputNoArmstrongElements() {
        int[] numbers = {14,21,43};
        Array newVector = new Array(numbers);
        Object[] expected = {};

        Array palindromes = newVector.getArmstrongElements();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getArmstrongElementsInputOneArmstrongElements() {
        int[] numbers = {1,2,3};
        Array newVector = new Array(numbers);
        Object[] expected = {1};

        Array palindromes = newVector.getArmstrongElements();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getArmstrongElementsInputOnlyArmstrongElements() {
        int[] numbers = {0,1,153,370,371,407};
        Array newVector = new Array(numbers);
        Object[] expected = {0,1,153,370,371,407};

        Array palindromes = newVector.getArmstrongElements();

        Object[] result = palindromes.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceInputOneInIncreasingSequenceButTooShort() {
        int[] numbers = {1,2,3,123};
        int length = 4;
        Array newVector = new Array(numbers);
        Object[] expected = {};

        Array increasingSequence = newVector.getElementsWithDigitsInIncreasingSequence(length);

        Object[] result = increasingSequence.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceInputAllNumbersInIncreasingSequenceButOnlyOneLongEnough() {
        int[] numbers = {12,345,23,1234};
        int length = 4;
        Array newVector = new Array(numbers);
        Object[] expected = {1234};

        Array increasingSequence = newVector.getElementsWithDigitsInIncreasingSequence(length);

        Object[] result = increasingSequence.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void getElementsWithDigitsInIncreasingSequenceInputAllNumbersInIncreasingSequenceAllLongEnough() {
        int[] numbers = {12,345,23,1234};
        int length = 2;
        Array newVector = new Array(numbers);
        Object[] expected = {12,345,23,1234};

        Array increasingSequence = newVector.getElementsWithDigitsInIncreasingSequence(length);

        Object[] result = increasingSequence.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void isEqualVectorResultTrue() {
        int[] numbersOne = {12,345,23,1234};
        Array vectorOne = new Array(numbersOne);
        int[] numbersTwo = {12,345,23,1234};
        Array vectorTwo = new Array(numbersTwo);

        boolean result = vectorOne.isEqualVector(vectorTwo);

        assertTrue(result);
    }

    @Test
    void isEqualVectorResultFalseDifferentLength() {
        int[] numbersOne = {12,345,23,1234,1};
        Array vectorOne = new Array(numbersOne);
        int[] numbersTwo = {12,345,23,1234};
        Array vectorTwo = new Array(numbersTwo);

        boolean result = vectorOne.isEqualVector(vectorTwo);

        assertFalse(result);
    }

    @Test
    void isEqualVectorResultFalseDifferentElements() {
        int[] numbersOne = {1,2,3,4,5};
        Array vectorOne = new Array(numbersOne);
        int[] numbersTwo = {6,7,8,9,10};
        Array vectorTwo = new Array(numbersTwo);

        boolean result = vectorOne.isEqualVector(vectorTwo);

        assertFalse(result);
    }
}