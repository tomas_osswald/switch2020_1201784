package array;

public class Vector {

    // attributes
    private int[] vector;

    // constructor

    /**
     * Constructs an empty Vector
     */
    public Vector() {
        this.vector = new int[0];
    }

    /**
     * Constructs a Vector with given array
     *
     * @param array
     */
    public Vector(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        this.vector = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            this.vector[i] = array[i];
        }
    }

    /**
     * Constructs a Vector from a given Vector
     *
     * @param givenVector
     */
    public Vector(Vector givenVector) {
        if (givenVector == null) {
            throw new IllegalArgumentException("Vector should not be null");
        }
        Vector newVector = new Vector();
        for (int i = 0; i < givenVector.getNumberOfElements(); i++) {
            newVector.append(givenVector.getValueAtIndex(i));
        }
        this.vector = newVector.vector;
    }

    // methods

    /**
     * Returns true if given value is even
     *
     * @param value
     * @return
     */
    private static boolean isEven(int value) {
        boolean isEven = false;
        if (value % 2 == 0) {
            isEven = true;
        }
        return isEven;
    }

    /**
     * Returns the number of digits in a given number
     *
     * @param element
     * @return
     */
    static public int countDigits(int element) {
        int digitCount = 0;
        do {
            digitCount++;
            element /= 10;
        } while (element > 0);
        return digitCount;
    }

    /**
     * Returns de number of even digits in a given element
     *
     * @param element
     * @return
     */
    public static int countEvenDigits(int element) {
        int evenDigitCounter = 0;
        do {
            if (isEven(element)) {
                evenDigitCounter++;
            }
            element /= 10;
        } while (element > 0);
        return evenDigitCounter;
    }

    /**
     * Returns the percentage of even digits in an element
     *
     * @param element
     * @return
     */
    public static double getEvenDigitPercentageOfElement(int element) {
        double evenDigitPercentage;
        int digits = countDigits(element);
        int evenDigits = countEvenDigits(element);
        evenDigitPercentage = evenDigits / (double) digits;
        return evenDigitPercentage;
    }

    /**
     * Returns true if the elements of this Vector are of equal value to the elements of a given Object
     */
    @Override
    public boolean equals(Object o) {
        boolean isEqual = true;
        if (o == this) {
            isEqual = true;
        } else if (o instanceof Vector) {
            Vector otherVector = (Vector) o;
            if (otherVector.vector.length == this.vector.length) {
                for (int i = 0; i < this.vector.length && isEqual; i++) {
                    if (this.vector[i] != otherVector.vector[i]) {
                        isEqual = false;
                    }
                }
            } else {
                isEqual = false;
            }
        } else {
            isEqual = false;
        }
        return isEqual;
    }

    /**
     * Appends a value to the end of this Vector
     *
     * @param value
     */
    public void append(int value) {
        int[] newVector = new int[this.vector.length + 1];
        for (int i = 0; i < newVector.length; i++) {
            if (i == newVector.length - 1) {
                newVector[i] = value;
            } else {
                newVector[i] = this.vector[i];
            }
        }
        this.vector = newVector;
    }

    /**
     * Removes the first occurrence of a given value in the Vector. If value does not exists, does nothing
     *
     * @param value
     */
    public void removeFirstOccurrence(int value) {
        if (existsInVector(value)) {
            boolean removed = false;
            int[] newVector = new int[this.vector.length - 1];
            for (int i = 0, j = 0; i < this.vector.length; i++, j++) {
                if (this.vector[i] != value || removed) {
                    newVector[j] = this.vector[i];
                } else {
                    removed = true;
                    j--;
                }
            }
            this.vector = newVector;
        }
    }

    /**
     * Returns true if value exists in the Vector
     *
     * @param value
     * @return
     */
    public boolean existsInVector(int value) {
        boolean exists = false;
        for (int i = 0; i < this.vector.length && !exists; i++) {
            if (this.vector[i] == value) {
                exists = true;
            }
        }
        return exists;
    }

    /**
     * Returns value at given index
     *
     * @param index
     * @return
     */
    public int getValueAtIndex(int index) {
        int value;
        if (index < 0 || index >= this.vector.length) {
            throw new ArrayIndexOutOfBoundsException("Index is out of bounds");
        }
        value = this.vector[index];
        return value;
    }

    /**
     * Returns the number of elements in the Vector
     *
     * @return
     */
    public int getNumberOfElements() {
        int elementCounter;
        elementCounter = this.vector.length;
        return elementCounter;
    }

    /**
     * Returns the largest element in the Vector
     *
     * @return
     */
    public int getLargest() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Vector is empty");
        }
        int largest = this.vector[0];
        for (int element : this.vector) {
            if (element > largest) {
                largest = element;
            }
        }
        return largest;
    }

    /**
     * Returns the smallest element in the Vector
     *
     * @return
     */
    public int getSmallest() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Vector is empty");
        }
        int smallest = this.vector[0];
        for (int element : this.vector) {
            if (element < smallest) {
                smallest = element;
            }
        }
        return smallest;
    }

    /**
     * Returns the average of all elements in the Vector
     *
     * @return
     */
    public double getAverage() {
        double average = 0;
        int sum = 0;
        for (int element : this.vector) {
            sum += element;
        }
        int numberOfElements = getNumberOfElements();
        if (numberOfElements != 0) {
            average = sum / (double) getNumberOfElements();
        }
        return average;
    }

    /**
     * Returns the average of all even elements in the Vector
     *
     * @return
     */
    public double getAverageOfEvens() {
        double average = 0;
        int sumEvens = 0;
        int evensCounter = 0;
        for (int element : this.vector) {
            if (isEven(element)) {
                sumEvens += element;
                evensCounter++;
            }
        }
        if (evensCounter != 0) {
            average = sumEvens / (double) evensCounter;
        }
        return average;
    }

    /**
     * Returns the average of all odd elements in the vector
     *
     * @return
     */
    public double getAverageOfOdds() {
        double average = 0;
        int sumOdds = 0;
        int oddsCounter = 0;
        for (int element : this.vector) {
            if (!isEven(element)) {
                sumOdds += element;
                oddsCounter++;
            }
        }
        if (oddsCounter != 0) {
            average = sumOdds / (double) oddsCounter;
        }
        return average;
    }

    /**
     * Returns true if an element is multiple of a given number
     *
     * @param element
     * @param number
     * @return
     */
    private boolean isElementMultipleOfNumber(int element, int number) {
        boolean isMultiple = false;
        if (element % number == 0) {
            isMultiple = true;
        }
        return isMultiple;
    }

    /**
     * Returns the average of all elements that are multiples of a given value
     *
     * @param value
     * @return
     */
    public double getAverageOfMultiples(int value) {
        double average = 0;
        int sumMultiples = 0;
        int multiplesCounter = 0;
        for (int element : this.vector) {
            if (isElementMultipleOfNumber(element, value)) {
                sumMultiples += element;
                multiplesCounter++;
            }
        }
        if (multiplesCounter != 0) {
            average = sumMultiples / (double) multiplesCounter;
        }
        return average;
    }

    /**
     * Returns 1 if number one is larger than number two, 0 if equal, -1 is number two is larger than number one
     *
     * @param numberOne
     * @param numberTwo
     * @return
     */
    private int compareNumbers(int numberOne, int numberTwo) {
        int compare = 0;
        if (numberOne > numberTwo) {
            compare = 1;
        }
        if (numberOne < numberTwo) {
            compare = -1;
        }
        return compare;
    }

    /**
     * Sorts the Vector in ascending order
     */
    public void sortAscending() {
        int temp = 0;
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (compareNumbers(this.vector[i], this.vector[j]) == 1) {
                    temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;
                }
            }
        }
    }

    /**
     * Sorts de Vector in descending order
     */
    public void sortDescending() {
        int temp = 0;
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (compareNumbers(this.vector[i], this.vector[j]) == -1) {
                    temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;
                }
            }
        }
    }

    /**
     * Returns true if Vector is empty (has no elements)
     *
     * @return
     */
    public boolean isEmpty() {
        boolean isEmpty = false;
        if (this.vector.length == 0) {
            isEmpty = true;
        }
        return isEmpty;
    }

    /**
     * Returns true if Vector has only one element
     *
     * @return
     */
    public boolean hasOnlyOneElement() {
        boolean hasOnlyOneElement = false;
        if (this.vector.length == 1) {
            hasOnlyOneElement = true;
        }
        return hasOnlyOneElement;
    }

    /**
     * Returns true if Vector has only even elements
     *
     * @return
     */
    public boolean hasOnlyEvenElements() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Vector is empty");
        }
        boolean hasOnlyEvenElements = true;
        for (int i = 0; i < this.vector.length && hasOnlyEvenElements; i++) {
            if (!isEven(this.vector[i])) {
                hasOnlyEvenElements = false;
            }
        }
        return hasOnlyEvenElements;
    }

    /**
     * Returns true if Vector has only odd elements
     *
     * @return
     */
    public boolean hasOnlyOddElements() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Vector is empty");
        }
        boolean hasOnlyOddElements = true;
        for (int i = 0; i < this.vector.length && hasOnlyOddElements; i++) {
            if (isEven(this.vector[i])) {
                hasOnlyOddElements = false;
            }
        }
        return hasOnlyOddElements;
    }

    /**
     * Returns true if Vector has any duplicate elements
     *
     * @return
     */
    public boolean hasDuplicates() {
        boolean hasDuplicates = false;
        for (int i = 0; i < this.vector.length && !hasDuplicates; i++) {
            for (int j = i + 1; j < this.vector.length && !hasDuplicates; j++) {
                if (this.vector[i] == this.vector[j]) {
                    hasDuplicates = true;
                }
            }
        }
        return hasDuplicates;
    }

    /**
     * Returns the average digits of all elements in Vector
     *
     * @return
     */
    public double getAverageDigits() {
        double averageDigits = 0;
        int sumDigits = 0;
        for (int element : this.vector) {
            sumDigits += countDigits(element);
        }
        int numberOfElements = getNumberOfElements();
        if (numberOfElements != 0) {
            averageDigits = sumDigits / (double) numberOfElements;
        }
        return averageDigits;
    }

    /**
     * Returns a copy of this Vector
     *
     * @return
     */
    public Vector copyVector() {
        int[] arrayCopy = new int[this.vector.length];
        for (int i = 0; i < arrayCopy.length; i++) {
            arrayCopy[i] = this.vector[i];
        }
        Vector copy = new Vector(arrayCopy);
        return copy;
    }

    /**
     * Returns a Vector with only the elements with digits above average in this Vector
     *
     * @return
     */
    public Vector getElementsWithDigitsAboveAverage() {
        Vector elementsWithDigitsAboveAverage = copyVector();
        double averageDigits = getAverageDigits();
        for (int i = 0; i < this.vector.length; i++) {
            if (countDigits(this.vector[i]) <= averageDigits) {
                elementsWithDigitsAboveAverage.removeFirstOccurrence(this.vector[i]);
            }
        }
        return elementsWithDigitsAboveAverage;
    }

    /**
     * Returns the percentage of even digits of all elements in this Vector
     *
     * @return
     */
    public double getAverageEvenDigitPercentage() {
        double evenDigitPercentage = 0;
        double sumEvenDigitPercentageOfElements = 0;
        for (int element : this.vector) {
            sumEvenDigitPercentageOfElements += getEvenDigitPercentageOfElement(element);
        }
        int numberOfElements = getNumberOfElements();
        if (numberOfElements != 0) {
            evenDigitPercentage = sumEvenDigitPercentageOfElements / (double) numberOfElements;
        }
        return evenDigitPercentage;
    }

    /**
     * Returns a Vector with only the elements with above average even digit percentage in this Vector
     *
     * @return
     */
    public Vector getElementsWithEvenDigitPercentageAboveAverage() {
        Vector elementsWithEvenDigitPercentageAboveAverage = copyVector();
        double averageEvenDigitPercentage = getAverageEvenDigitPercentage();
        for (int i = 0; i < this.vector.length; i++) {
            if (getEvenDigitPercentageOfElement(this.vector[i]) <= averageEvenDigitPercentage) {
                elementsWithEvenDigitPercentageAboveAverage.removeFirstOccurrence(this.vector[i]);
            }
        }
        return elementsWithEvenDigitPercentageAboveAverage;
    }

    /**
     * Returns true if a given number only has even digits
     *
     * @param element
     * @return
     */
    private boolean hasOnlyEvenDigits(int element) {
        boolean hasOnlyEvenDigits = false;
        int digits = countDigits(element);
        int evenDigits = countEvenDigits(element);
        if (digits == evenDigits) {
            hasOnlyEvenDigits = true;
        }
        return hasOnlyEvenDigits;
    }

    /**
     * Returns a Vector with only the elements with only even digits in this Vector
     *
     * @return
     */
    public Vector getElementsWithOnlyEvenDigits() {
        Vector elementsWithOnlyEvenDigits = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (hasOnlyEvenDigits(this.vector[i])) {
                elementsWithOnlyEvenDigits.append(this.vector[i]);
            }
        }
        return elementsWithOnlyEvenDigits;
    }

    /**
     * Returns an array with the digits of a number as elements
     *
     * @param number
     * @return
     */
    private int[] digitsToArray(int number) {
        int arrayLength = countDigits(number);
        int[] array = new int[arrayLength];
        for (int i = arrayLength - 1; i >= 0; i--) {
            array[i] = number % 10;
            number /= 10;
        }
        return array;
    }

    /**
     * Returns true if the number's digits are in increasing sequence
     *
     * @param number
     * @return
     */
    private boolean isNumberInIncreasingSequence(int number) {
        boolean isIncreasing = true;

        if (number < 10) {
            isIncreasing = false;
        } else {
            int[] array = digitsToArray(number);
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] >= array[i + 1]) {
                    isIncreasing = false;
                }
            }
        }
        return isIncreasing;
    }

    /**
     * Returns a Vector with only the elements with digits in increasing sequence in this Vector
     *
     * @return
     */
    public Vector getElementsWithDigitsInIncreasingSequence() {
        Vector elementsWithDigitsInIncreasingSequence = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (isNumberInIncreasingSequence(this.vector[i])) {
                elementsWithDigitsInIncreasingSequence.append(this.vector[i]);
            }
        }
        return elementsWithDigitsInIncreasingSequence;
    }

    /**
     * Returns true if a number is a numerical palindrome
     *
     * @param number
     * @return
     */
    private boolean isPalindrome(int number) {
        boolean isPalindrome = true;
        int[] numberArray = digitsToArray(number);
        for (int i = 0, j = numberArray.length - 1; i < numberArray.length && j >= 0 && isPalindrome; i++, j--) {
            if (numberArray[i] != numberArray[j]) {
                isPalindrome = false;
            }
        }
        return isPalindrome;
    }

    /**
     * Returns a Vector with only the elements that are palindromes in this Vector
     *
     * @return
     */
    public Vector getPalindromes() {
        Vector palindromes = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (isPalindrome(this.vector[i])) {
                palindromes.append(this.vector[i]);
            }
        }
        return palindromes;
    }

    /**
     * Returns true if all digits of the number are the same
     *
     * @param number
     * @return
     */
    private boolean isSameDigits(int number) {
        boolean sameDigits = true;
        int[] numberArray = digitsToArray(number);
        for (int i = 1; i < numberArray.length && sameDigits; i++) {
            if (numberArray[0] != numberArray[i]) {
                sameDigits = false;
            }
        }
        return sameDigits;
    }

    /**
     * Returns a Vector with only the elements that have the same digits in this Vector
     *
     * @return
     */
    public Vector getElementsWithSameDigits() {
        Vector elementsWithSameDigits = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (isSameDigits(this.vector[i])) {
                elementsWithSameDigits.append(this.vector[i]);
            }
        }
        return elementsWithSameDigits;
    }

    /**
     * Returns true if a number is an armstrong number
     *
     * @param number
     * @return
     */
    private boolean isArmstrongNumber(int number) {
        boolean isArmstrong = false;
        int[] numberArray = digitsToArray(number);
        double armstrongNumber = 0;
        int digits = countDigits(number);
        for (int i = 0; i < numberArray.length; i++) {
            armstrongNumber += Math.pow(numberArray[i], digits);
        }
        if (armstrongNumber == number) {
            isArmstrong = true;
        }
        return isArmstrong;
    }

    /**
     * Returns a Vector with only the elements that are armstrong numbers in this Vector
     *
     * @return
     */
    public Vector getArmstrongElements() {
        Vector armostrongELements = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (isArmstrongNumber(this.vector[i])) {
                armostrongELements.append(this.vector[i]);
            }
        }
        return armostrongELements;
    }

    /**
     * Returns true if a number has a sequence of increasing digits of at least a given size
     *
     * @param number
     * @param size
     * @return
     */
    private boolean isNumberInIncreasingSequenceOfSize(int number, int size) {
        boolean isIncreasing = false;
        if (countDigits(number) >= size) {
            int[] array = digitsToArray(number);
            for (int index = 0; index < array.length - size + 1 && !isIncreasing; index++) {
                int sequenceCounter = 0;
                boolean sequence = true;
                for (int i = index, j = i + 1; j < array.length && j < index + size && sequence; i++, j++) {
                    if (array[i] < array[j]) {
                        sequenceCounter++;
                    } else {
                        sequence = false;
                    }
                }
                if (sequenceCounter == size - 1) {
                    isIncreasing = true;
                }
            }
        }
        return isIncreasing;
    }

    /**
     * Returns a Vector with only the elements with digits in increasing sequence of a given size in this vector
     *
     * @param size
     * @return
     */
    public Vector getElementsWithDigitsInIncreasingSequenceOfSize(int size) {
        Vector increasingSequence = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (isNumberInIncreasingSequenceOfSize(this.vector[i], size)) {
                increasingSequence.append(this.vector[i]);
            }
        }
        return increasingSequence;
    }

    /**
     * Returns the sum of all elements in this vector
     *
     * @return
     */
    public int getSum() {
        int sum = 0;
        for (int element : this.vector) {
            sum += element;
        }
        return sum;
    }

    /**
     * Returns the index of the first occurrence of a given value in this Vector. Returns -1 if value is not found
     *
     * @param value
     * @return
     */
    public int getIndexOfFirstOccurrence(int value) {
        int index = -1;
        for (int i = 0; i < this.vector.length && index == -1; i++) {
            if (this.vector[i] == value) {
                index = i;
            }
        }
        return index;
    }

    /**
     * Returns the amount of occurrences of a given value in this vector
     *
     * @param value given value
     * @return amount of occurrences
     */
    public int getNumberOfOccurrences(int value) {
        int amountOfOccurrences = 0;
        for (int element : this.vector) {
            if (element == value) {
                amountOfOccurrences++;
            }
        }
        return amountOfOccurrences;
    }

    /**
     * Inverts the order of elements in this array
     */
    public void invert() {
        Vector inverted = new Vector();
        for (int i = this.vector.length - 1; i >= 0; i--) {
            inverted.append(this.vector[i]);
        }
        this.vector = inverted.vector;
    }

    /**
     * Sets given value at given index
     *
     * @param value given value
     * @param index given index
     */
    public void setValue(int value, int index) {
        if (index > this.vector.length - 1) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        this.vector[index] = value;
    }

}
