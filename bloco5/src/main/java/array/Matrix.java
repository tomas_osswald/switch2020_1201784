package array;

public class Matrix {

    // attributes
    private Vector[] matrix;

    // constructor

    public Matrix() {
        this.matrix = new Vector[0];
    }

    public Matrix(int[][] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        Vector[] newMatrix = new Vector[array.length];
        for (int i = 0; i < array.length; i++) {
            Vector arr = new Vector(array[i]);
            newMatrix[i] = arr.copyVector();
        }
        this.matrix = newMatrix;
    }

    public Matrix(Vector[] givenMatrix) {
        if (givenMatrix == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        Vector[] newMatrix = new Vector[givenMatrix.length];
        for (int i = 0; i < givenMatrix.length; i++) {
            newMatrix[i] = givenMatrix[i].copyVector();
        }
        this.matrix = newMatrix;
    }

    /**
     * Returns true if the elements of this Matrix are equal to those of a given Matrix
     * @param o given Matrix
     * @return true if equal
     */
    @Override
    public boolean equals(Object o) {
        boolean isEqual = true;
        if (o == this) {
            isEqual = true;
        } else if (o instanceof Matrix) {
            Matrix otherMatrix = (Matrix) o;
            if (otherMatrix.matrix.length == this.matrix.length) {
                for (int i = 0; i < this.matrix.length && isEqual; i++) {
                    if (!this.matrix[i].equals(otherMatrix.matrix[i])) {
                        isEqual = false;
                    }
                }
            } else {
                isEqual = false;
            }
        } else {
            isEqual = false;
        }
        return isEqual;
    }


    /**
     * Appends a given value to the end of a given line of this Matrix
     *
     * @param number given number
     * @param line   given line
     */
    public void append(int number, int line) {
        if (line >= this.matrix.length) {
            throw new IndexOutOfBoundsException("Line is out of bounds");
        }
        this.matrix[line].append(number);
    }

    /**
     * Removes the first occurrence of a given value in the Matrix. If value does not exists, does nothing
     *
     * @param value given value
     */
    public void removeFirstOccurrence(int value) {
        int line = getLineOfValue(value);
        if (line != -1) {
            this.matrix[line].removeFirstOccurrence(value);
        }
    }

    /**
     * Returns the line where value exists, else returns -1 if value does not exist.
     *
     * @param value given value
     * @return line of value
     */
    private int getLineOfValue(int value) {
        int exists = -1;
        for (int i = 0; i < this.matrix.length && exists == -1; i++) {
            if (this.matrix[i].existsInVector(value)) {
                exists = i;
            }
        }
        return exists;
    }

    /**
     * Returns true if the Matrix is empty (has no elements)
     *
     * @return true if empty
     */
    public boolean isEmpty() {
        boolean isEmpty = true;
        for (int i = 0; i < this.matrix.length && isEmpty; i++) {
            if (!this.matrix[i].isEmpty()) {
                isEmpty = false;
            }
        }
        return isEmpty;
    }

    /**
     * Returns the largest element in the Matrix
     *
     * @return largest element
     */
    public int getLargest() {
        int largest = this.matrix[0].getLargest();
        for (int i = 1; i < this.matrix.length; i++) {
            int vectorLargest = this.matrix[i].getLargest();
            if (largest < vectorLargest) {
                largest = vectorLargest;
            }
        }
        return largest;
    }

    /**
     * Returns the smallest element in the Matrix
     *
     * @return smallest value
     */
    public int getSmallest() {
        int smallest = this.matrix[0].getSmallest();
        for (int i = 1; i < this.matrix.length; i++) {
            int vectorSmallest = this.matrix[i].getSmallest();
            if (smallest > vectorSmallest) {
                smallest = vectorSmallest;
            }
        }
        return smallest;
    }

    /**
     * Returns the average of all elements in the Matrix
     *
     * @return average value
     */
    public double getAverage() {
        double average = 0;
        int sumAverageVectors = 0;
        for (Vector vector : this.matrix) {
            sumAverageVectors += vector.getAverage();
        }
        average = sumAverageVectors / (double) this.matrix.length;
        return average;
    }

    /**
     * Returns a Vector where each element is the sum of each row in this Matrix
     *
     * @return Sum of each row as Vector
     */
    public Vector getSumOfEachRow() {
        int[] sumOfEachRow = new int[this.matrix.length];
        for (int i = 0; i < sumOfEachRow.length; i++) {
            sumOfEachRow[i] = this.matrix[i].getSum();
        }
        return new Vector(sumOfEachRow);
    }

    /**
     * Returns a Vector where each element is the sum of each column in this Matrix
     *
     * @return Sum of each column as Vector
     */
    public Vector getSumOfEachColumn() {
        int[] sumOfEachColumn = new int[getLargestColumn()];
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].getNumberOfElements(); j++) {
                sumOfEachColumn[j] += this.matrix[i].getValueAtIndex(j);
            }
        }
        return new Vector(sumOfEachColumn);
    }

    /**
     * Returns the largest index of the columns in this Matrix
     *
     * @return index of largest column
     */
    private int getLargestColumn() {
        int largestColumn = this.matrix[0].getNumberOfElements();
        for (int i = 1; i < this.matrix.length; i++) {
            if (largestColumn < this.matrix[i].getNumberOfElements()) {
                largestColumn = this.matrix[i].getNumberOfElements();
            }
        }
        return largestColumn;
    }

    /**
     * Returns the index of the row with the largest sum of elements in this Matrix
     *
     * @return index of row with largest sum of elements
     */
    public int getRowWithLargestSumOfElements() {
        Vector sumOfEachRow = getSumOfEachRow();
        int largest = sumOfEachRow.getLargest();
        return sumOfEachRow.getIndexOfFirstOccurrence(largest);
    }

    /**
     * Returns true if this Matrix is a square
     *
     * @return true if square
     */
    public boolean isSquare() {
        boolean isSquare = true;
        int height = this.matrix.length;
        for (int i = 0; i < this.matrix.length && isSquare; i++) {
            if (this.matrix[i].getNumberOfElements() != height) {
                isSquare = false;
            }
        }
        return isSquare;
    }

    /**
     * Returns true if this Matrix is symmetric and a square. If matrix is not symmetric or not a square, returns false
     *
     * @return true if symmetric
     */
    public boolean isSymmetric() {
        boolean isSymmetric = true;
        if (!isSquare()) {
            isSymmetric = false;
        } else {
            for (int i = 0; i < this.matrix.length && isSymmetric; i++) {
                for (int j = 0; j < this.matrix.length && isSymmetric; j++) {
                    if (this.matrix[i].getValueAtIndex(j) != this.matrix[j].getValueAtIndex(i)) {
                        isSymmetric = false;
                    }
                }
            }
        }
        return isSymmetric;
    }

    /**
     * Returns a Vector with the main diagonal of this Matrix
     *
     * @return main diagonal
     */
    private Vector getMainDiagonal() {
        Vector mainDiagonal = new Vector();
        for (int i = 0; i < this.matrix.length; i++) {
            mainDiagonal.append(this.matrix[i].getValueAtIndex(i));
        }
        return mainDiagonal;
    }

    /**
     * Returns a Vector with the secondary diagonal of this Matrix
     *
     * @return secondary diagonal
     */
    private Vector getSecondaryDiagonal() {
        Vector secondaryDiagonal = new Vector();
        for (int i = 0, j = this.matrix.length - 1; i < this.matrix.length && j >= 0; i++, j--) {
            secondaryDiagonal.append(this.matrix[i].getValueAtIndex(j));
        }
        return secondaryDiagonal;
    }

    /**
     * Returns the number of non zero values in the main diagonal
     *
     * @return number of non zero values
     */
    public int getNonNullElementsInMainDiagonal() {
        int nonNullElements = 0;
        if (!this.isSquare()) {
            nonNullElements = 1;
        } else {
            Vector mainDiagonal = getMainDiagonal();
            nonNullElements = mainDiagonal.getNumberOfElements() - mainDiagonal.getNumberOfOccurrences(0);
        }
        return nonNullElements;
    }

    /**
     * Returns true if the main diagonal is equal to the secondary diagonal (have the same elements in the same order)
     *
     * @return true if equal
     */
    public boolean isMainDiagonalEqualSecondaryDiagonal() {
        boolean isEqual;
        Vector mainDiagonal = getMainDiagonal();
        Vector secondaryDiagonal = getSecondaryDiagonal();
        isEqual = mainDiagonal.equals(secondaryDiagonal);
        return isEqual;
    }

    /**
     * Returns the average number of digits of all elements in this matrix
     *
     * @return average digits
     */
    private double getAverageDigits() {
        double averageDigits;
        double sumAverageRows = 0;
        for (Vector vector : this.matrix) {
            sumAverageRows += vector.getAverageDigits();
        }
        averageDigits = sumAverageRows / (double) this.matrix.length;
        return averageDigits;
    }

    /**
     * Returns a Vector with the elements of this Matrix that have above average number of digits
     *
     * @return Vector of elements with above average digits
     */
    public Vector getElementsWithDigitsAboveAverage() {
        Vector elementsWithDigitsAboveAverage = new Vector();
        double averageDigits = getAverageDigits();
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].getNumberOfElements(); j++) {
                if (Vector.countDigits(this.matrix[i].getValueAtIndex(j)) > averageDigits) {
                    elementsWithDigitsAboveAverage.append(this.matrix[i].getValueAtIndex(j));
                }
            }

        }
        return elementsWithDigitsAboveAverage;
    }

    /**
     * Returns the average even digit percentage of all elements in this matrix
     *
     * @return average even digit percentage
     */
    private double getAverageEvenDigitPercentage() {
        double averageDigits;
        double sumAverageRows = 0;
        for (Vector vector : this.matrix) {
            sumAverageRows = vector.getAverageEvenDigitPercentage();
        }
        averageDigits = sumAverageRows / (double) this.matrix.length;
        return averageDigits;
    }

    /**
     * Returns a Vector with the elements of this Matrix that have above average percentage of even digits
     *
     * @return Vector of elements with above average even digit percentage
     */
    public Vector getElementsWithEvenDigitPercentageAboveAverage() {
        Vector elementsWithEvenDigitPercentageAboveAverage = new Vector();
        double averageEvenDigitPercentage = getAverageEvenDigitPercentage();
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].getNumberOfElements(); j++) {
                if (Vector.getEvenDigitPercentageOfElement(this.matrix[i].getValueAtIndex(j)) > averageEvenDigitPercentage) {
                    elementsWithEvenDigitPercentageAboveAverage.append(this.matrix[i].getValueAtIndex(j));
                }
            }

        }
        return elementsWithEvenDigitPercentageAboveAverage;
    }

    /**
     * Inverts all rows of this matrix
     */
    public void invertRows() {
        for (Vector vector : this.matrix) {
            vector.invert();
        }
    }

    /**
     * Inverts all columns of this matrix
     */
    public void invertColumns() {
        for (int i = 0, j = this.matrix.length - 1; i < this.matrix.length / 2; i++, j--) {
            Vector temp = this.matrix[i];
            this.matrix[i] = this.matrix[j];
            this.matrix[j] = temp;
        }
    }

    /**
     * Transposes this matrix
     */
    public void transposeMatrix() {
        Matrix transposed = new Matrix(this.matrix);
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].getNumberOfElements(); j++) {
                transposed.matrix[i].setValue(this.matrix[j].getValueAtIndex(i), j);
            }
        }
        this.matrix = transposed.matrix;
    }

    /**
     * Rotates this matrix 90 degrees clockwise
     */
    public void rotateNinetyDegrees() {
        transposeMatrix();
        invertRows();
    }

    /**
     * Rotates this matrix 180 degrees
     */
    public void rotateOneEightyDegrees() {
        invertColumns();
        invertRows();
    }

    /**
     * Rotates this matrix 90 degrees counter clockwise
     */
    public void rotateMinusNinetyDegrees() {
        transposeMatrix();
        invertColumns();
    }
}
