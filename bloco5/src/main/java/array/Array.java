package array;

import java.util.ArrayList;

public class Array {

    // attributes
    private final ArrayList<Integer> integers;

    // constructors
    public Array() {
        this.integers = new ArrayList<Integer>();
    }

    public Array(int[] numbers) {
        if (numbers == null) {
            throw new IllegalArgumentException("Integers array should not be null");
        }
        int[] copy = createCopy(numbers);
        this.integers = new ArrayList<Integer>();
        for (int number : copy) {
            this.integers.add(number);
        }
    }

    // methods

    /**
     * Returns a copy of a given array
     * @param array
     * @return
     */
    public int[] createCopy(int[] array) {
        int[] copy = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            copy[i] = array[i];
        }
        return copy;
    }

    /**
     * Returns true if numbers is even
     *
     * @param number
     * @return
     */
    private boolean isEven(Integer number) {
        return (number % 2 == 0);
    }

    /**
     * Returns 1 if number one is larger than number two, 0 if equal, -1 is number two is larger than number one
     *
     * @param numberOne
     * @param numberTwo
     * @return
     */
    private int compareNumbers(int numberOne, int numberTwo) {
        int compare = 0;
        if (numberOne > numberTwo) {
            compare = 1;
        }
        if (numberOne < numberTwo) {
            compare = -1;
        }
        return compare;
    }

    /**
     * Returns list in array of object type
     *
     * @return
     */
    public Object[] toArray() {
        this.integers.trimToSize();
        return this.integers.toArray();
    }

    /**
     * Adds number to the ArrayList integers
     *
     * @param number
     * @return
     */
    public boolean add(int number) {
        return this.integers.add(number);
    }

    /**
     * Remove first occurrence of the number in ArrayList
     *
     * @param number
     */
    public boolean removeFirstOccurrenceNumber(int number) {
        boolean found = false;

        for (int i = 0; i < integers.size() && !found; i++) {
            if (this.integers.get(i) == number) {
                this.integers.remove(i);
                found = true;
            }
        }

        return found;
    }

    /**
     * Returns element at specified index in list
     *
     * @param index
     * @return
     */
    public Integer getElementAtIndex(int index) {
        return this.integers.get(index);
    }

    /**
     * Returns the number of elements in list
     *
     * @return
     */
    public int getSize() {
        return this.integers.size();
    }

    /**
     * Returns the largest element in list
     *
     * @return
     */
    public Integer getLargestElement() {
        Integer largest = this.integers.get(0);

        for (Integer element : integers) {
            if (element > largest) {
                largest = element;
            }
        }

        return largest;
    }

    /**
     * Returns the smallest element in list
     *
     * @return
     */
    public Integer getSmallestElement() {
        Integer smallest = this.integers.get(0);

        for (Integer element : integers) {
            if (element < smallest) {
                smallest = element;
            }
        }

        return smallest;
    }

    /**
     * Returns the average of all elements in the list
     *
     * @return
     */
    public Integer getAverage() {
        Integer sum = 0;
        for (Integer element : integers) {
            sum += element;
        }
        Integer average = sum / integers.size();
        return average;
    }

    /**
     * Returns the average of all even elements in the list
     *
     * @return
     */
    public Integer getEvensAverage() {
        Integer sum = 0;
        Integer counter = 0;
        for (Integer element : integers) {
            if (isEven(element)) {
                sum += element;
                counter++;
            }
        }
        if (counter == 0) {
            return counter;
        }
        Integer average = sum / counter;
        return average;
    }

    /**
     * Returns the average of all odd elements in the list
     *
     * @return
     */
    public Integer getOddsAverage() {
        Integer sum = 0;
        Integer counter = 0;
        for (Integer element : integers) {
            if (!isEven(element)) {
                sum += element;
                counter++;
            }
        }
        if (counter == 0) {
            return counter;
        }
        Integer average = sum / counter;
        return average;
    }

    /**
     * Returns the average of all multiples of a number in the list
     *
     * @param number
     * @return
     */
    public Integer getAverageOfMultiples(int number) {
        Integer sum = 0;
        Integer counter = 0;
        for (Integer element : integers) {
            if (element % number == 0) {
                sum += element;
                counter++;
            }
        }
        if (counter == 0) {
            return counter;
        }
        Integer average = sum / counter;
        return average;
    }

    /**
     * Sorts the list in ascending order
     */
    public void sortAscending() {
        Integer tempInteger = null;
        for (int i = 0; i < this.integers.size(); i++) {
            for (int j = i + 1; j < this.integers.size(); j++) {
                if (compareNumbers(this.integers.get(i), this.integers.get(j)) == 1) {
                    tempInteger = this.integers.get(i);
                    this.integers.set(i, this.integers.get(j));
                    this.integers.set(j, tempInteger);
                }
            }
        }
    }

    /**
     * Sorts the list in descending order
     */
    public void sortDescending() {
        Integer tempInteger = null;
        for (int i = 0; i < this.integers.size(); i++) {
            for (int j = i + 1; j < this.integers.size(); j++) {
                if (compareNumbers(this.integers.get(i), this.integers.get(j)) == -1) {
                    tempInteger = this.integers.get(i);
                    this.integers.set(i, this.integers.get(j));
                    this.integers.set(j, tempInteger);
                }
            }
        }
    }

    /**
     * Returns true if the list contains no elements
     *
     * @return
     */
    public boolean isEmpty() {
        return this.integers.isEmpty();
    }

    /**
     * Returns true if the list contains only one element
     *
     * @return
     */
    public boolean hasOneElement() {
        return (this.integers.size() == 1);
    }

    /**
     * Returns true if the list contains only even elements
     *
     * @return
     */
    public boolean hasOnlyEvenElements() {
        boolean onlyEvens = false;
        Integer counter = 0;
        for (Integer element : integers) {
            if (isEven(element)) {
                counter++;
            }
        }
        if (counter == this.integers.size()) {
            onlyEvens = true;
        }
        return onlyEvens;
    }

    /**
     * Returns true if the list contains only even odd
     *
     * @return
     */
    public boolean hasOnlyOddElements() {
        boolean onlyOdds = false;
        Integer counter = 0;
        for (Integer element : integers) {
            if (!isEven(element)) {
                counter++;
            }
        }
        if (counter == this.integers.size()) {
            onlyOdds = true;
        }
        return onlyOdds;
    }

    /**
     * Returns true if the list has any duplicate elements
     *
     * @return
     */
    public boolean hasDuplicates() {
        boolean hasDuplicates = false;
        Integer counter = 0;
        for (int i = 0; i < this.integers.size(); i++) {
            for (int j = i + 1; j < this.integers.size(); j++) {
                if (compareNumbers(this.integers.get(i), this.integers.get(j)) == 0) {
                    counter++;
                }
            }
        }
        if (counter > 0) {
            hasDuplicates = true;
        }
        return hasDuplicates;
    }

    /**
     * Returns average digits of elements in list
     *
     * @return
     */
    private Integer getAverageDigitsOfElements() {
        Integer sum = 0;
        for (Integer element : integers) {
            sum += element.toString().length();
        }
        Integer average = sum / integers.size();
        return average;
    }

    /**
     * Returns list with elements with digits above the average digits of elements in a list
     *
     * @return
     */
    public Array getElementsWithDigitsAboveAverage() {
        Array elementsWithDigitsAboveAverage = new Array();
        int averageDigits = this.getAverageDigitsOfElements();
        for (Integer element : integers) {
            if (element.toString().length() > averageDigits) {
                elementsWithDigitsAboveAverage.add(element);
            }
        }
        return elementsWithDigitsAboveAverage;
    }

    /**
     * Returns de percentage of even digits in a number
     *
     * @param number
     * @return
     */
    private double getPercentageEvenDigits(int number) {
        double percentageEvenDigits;
        int digit;
        int digitCounter = 0;
        int evenDigits = 0;
        while (number > 0) {
            digit = number % 10;
            if (isEven(digit)) {
                evenDigits++;
            }
            digitCounter++;
            number /= 10;
        }
        percentageEvenDigits = evenDigits / digitCounter;
        return percentageEvenDigits;
    }

    /**
     * Returns average even digits of elements in list
     *
     * @return
     */
    private double getAverageEvenDigitsPercentageOfElements() {
        double percentageEvenDigits = 0;
        for (Integer element : integers) {
            percentageEvenDigits += getPercentageEvenDigits(element);
        }
        percentageEvenDigits /= integers.size();
        return percentageEvenDigits;
    }

    /**
     * Returns list with elements with more even digits that the average of elements in a list
     *
     * @return
     */
    public Array getElementsWithEvenDigitsPercentageAboveAverage() {
        Array elementsWithEvenDigitPercentageAboveAverage = new Array();
        double averageEvenDigitsPercentage = this.getAverageEvenDigitsPercentageOfElements();
        for (Integer element : integers) {
            if (getPercentageEvenDigits(element) > averageEvenDigitsPercentage) {
                elementsWithEvenDigitPercentageAboveAverage.add(element);
            }
        }
        return elementsWithEvenDigitPercentageAboveAverage;
    }

    /**
     * Returns true if all digits in a number are even
     *
     * @param number
     * @return
     */
    private boolean hasOnlyEvenDigits(int number) {
        boolean hasOnlyEvenDigits = false;
        int digit;
        int evenDigits = 0;
        int digitCounter = 0;
        while (number > 0) {
            digit = number % 10;
            if (isEven(digit)) {
                evenDigits++;
            }
            digitCounter++;
            number /= 10;
        }
        if (evenDigits == digitCounter) {
            hasOnlyEvenDigits = true;
        }
        return hasOnlyEvenDigits;
    }

    /**
     * Returns a list with the elements of a list that only have even digits
     *
     * @return
     */
    public Array getElementsWithOnlyEvenDigits() {
        Array elementsWithOnlyEvenDigits = new Array();
        for (Integer element : integers) {
            if (hasOnlyEvenDigits(element)) {
                elementsWithOnlyEvenDigits.add(element);
            }
        }
        return elementsWithOnlyEvenDigits;
    }

    /**
     * Returns the number of digits in a number
     *
     * @param number
     * @return
     */
    private int countDigits(int number) {
        int digitCounter = 0;
        while (number > 0) {
            digitCounter++;
            number /= 10;
        }
        return digitCounter;
    }

    /**
     * Returns an array with the digits of a number as elements
     *
     * @param number
     * @return
     */
    private int[] digitsToArray(int number) {
        int arrayLength = countDigits(number);
        int[] array = new int[arrayLength];
        for (int i = arrayLength - 1; i >= 0; i--) {
            array[i] = number % 10;
            number /= 10;
        }
        return array;
    }

    /**
     * Returns true if the number's digits are in increasing sequence
     *
     * @param number
     * @return
     */
    private boolean isNumberInIncreasingSequence(int number) {
        boolean isIncreasing = true;

        if (number < 10) {
            isIncreasing = false;
        } else {
            int[] array = digitsToArray(number);
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] >= array[i + 1]) {
                    isIncreasing = false;
                }
            }
        }
        return isIncreasing;
    }

    /**
     * Returns a list with the elements of a list that have digits in an increasing sequence
     *
     * @return
     */
    public Array getElementsWithDigitsInIncreasingSequence() {
        Array elementsWithDigitsInIncreasingSequence = new Array();
        for (Integer element : integers) {
            if (isNumberInIncreasingSequence(element)) {
                elementsWithDigitsInIncreasingSequence.add(element);
            }
        }
        return elementsWithDigitsInIncreasingSequence;
    }

    /**
     * Returns true if a number is a numerical palindrome
     *
     * @param number
     * @return
     */
    private boolean isPalindrome(int number) {
        boolean isPalindrome = true;
        int[] numberArray = digitsToArray(number);
        for (int i = 0, j = numberArray.length - 1; i < numberArray.length && j >= 0 && isPalindrome; i++, j--) {
            if (numberArray[i] != numberArray[j]) {
                isPalindrome = false;
            }
        }
        return isPalindrome;
    }

    /**
     * Returns a list with the elements of a list that are palindromes
     *
     * @return
     */
    public Array getPalindromes() {
        Array palindromes = new Array();
        for (Integer element : integers) {
            if (isPalindrome(element)) {
                palindromes.add(element);
            }
        }
        return palindromes;
    }

    /**
     * Returns true if all digits of the number are the same
     *
     * @param number
     * @return
     */
    private boolean isSameDigits(int number) {
        boolean sameDigits = true;
        int[] numberArray = digitsToArray(number);
        for (int i = 1; i < numberArray.length && sameDigits; i++) {
            if (numberArray[0] != numberArray[i]) {
                sameDigits = false;
            }
        }
        return sameDigits;
    }

    /**
     * Returns a list with the elements of a list that consist of the same digits
     *
     * @return
     */
    public Array getElementsWithSameDigits() {
        Array elementsWithSameDigits = new Array();
        for (Integer element : integers) {
            if (isSameDigits(element)) {
                elementsWithSameDigits.add(element);
            }
        }
        return elementsWithSameDigits;
    }

    /**
     * Returns true if a number is an armstrong number
     * @param number
     * @return
     */
    private boolean isArmstrongNumber(int number) {
        boolean isArmstrong = false;
        int[] numberArray = digitsToArray(number);
        double armstrongNumber = 0;
        for (int i = 0; i < numberArray.length; i++) {
            armstrongNumber += Math.pow(numberArray[i], 3);
        }
        if (armstrongNumber == number) {
            isArmstrong = true;
        }
        return isArmstrong;
    }

    /**
     * Returns a list with the elements of a list that are armstrong numbers
     * @return
     */
    public Array getArmstrongElements() {
        Array ArmstrongElements = new Array();
        for (Integer element : integers) {
            if (isArmstrongNumber(element)) {
                ArmstrongElements.add(element);
            }
        }
        return ArmstrongElements;
    }

    /**
     * Returns a list with the elements of a list that have digits in an increasing sequence
     * and are at least 'length' long
     * @return
     */
    public Array getElementsWithDigitsInIncreasingSequence(int length) {
        Array elementsWithDigitsInIncreasingSequence = new Array();
        for (Integer element : integers) {
            if (isNumberInIncreasingSequence(element) && countDigits(element) >= length) {
                elementsWithDigitsInIncreasingSequence.add(element);
            }
        }
        return elementsWithDigitsInIncreasingSequence;
    }

    /**
     * Returns true if the elements of a given array.Array are equal to the elements of this array.Array
     * @param newVector
     * @return
     */
    public boolean isEqualVector(Array newVector) {
        return this.integers.equals(newVector.integers);
    }
}
