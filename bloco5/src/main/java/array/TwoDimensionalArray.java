package array;

import java.util.ArrayList;

public class TwoDimensionalArray {

    // attributes
    private ArrayList<ArrayList<Integer>> integers;

    // constructor

    public TwoDimensionalArray() {

    }

    public TwoDimensionalArray(int[][] numbers) {
        if (numbers == null) {
            throw new IllegalArgumentException("Integers array should not be null");
        }
        this.integers = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < numbers.length; i++) {
            ArrayList row = new ArrayList();
            for (int j = 0; j < numbers[i].length; j++) {
                row.add(numbers[i][j]);
            }
            integers.add(row);
        }
    }

    /*public static void main(String[] args) {
        int[][] numbers = {
                {0, 2, 0, 0, 1, 7, 0, 3, 0},
                {8, 5, 1, 0, 9, 4, 2, 0, 0},
                {4, 7, 0, 2, 6, 0, 9, 0, 0},
                {3, 4, 0, 0, 5, 0, 0, 0, 7},
                {0, 0, 8, 0, 0, 2, 1, 0, 3},
                {0, 0, 0, 6, 0, 8, 5, 0, 0},
                {0, 0, 4, 0, 0, 9, 6, 1, 0},
                {6, 0, 0, 0, 0, 3, 7, 0, 9},
                {0, 1, 0, 0, 4, 6, 0, 5, 0}};
        array.TwoDimensionalArray novo = new array.TwoDimensionalArray(numbers);
        System.out.println(novo.integers);
    }*/

    // methods

    /**
     * Returns list in array of object type
     *
     * @return
     */
    public Integer[][] toArray() {
        Integer[][] array = new Integer[this.integers.size()][];
        for (int i = 0; i < this.integers.size(); i++) {
            ArrayList<Integer> row = this.integers.get(i);
            array[i] = row.toArray(new Integer[row.size()]);
        }
        return array;
    }

    /**
     * Returns true if successful in adding a number in a given row to the array.TwoDimensionalArray
     *
     * @param number
     * @param row
     * @return
     */
    public boolean add(int number, int row) {
        return this.integers.get(row).add(number);
    }

    /**
     * Returns true if successful in removing the first occurrence of a given number in the array.TwoDimensionalArray
     *
     * @param number
     * @return
     */
    public boolean removeFirstOccurrenceNumber(int number) {
        boolean found = false;
        for (int i = 0; i < integers.size() && !found; i++) {
            for (int j = 0; j < integers.get(i).size() && !found; j++) {
                if (this.integers.get(i).get(j) == number) {
                    this.integers.get(i).remove(j);
                    found = true;
                }
            }
        }
        return found;
    }

    /**
     * Returns true if the array.TwoDimensionalArray is empty
     *
     * @return
     */
    public boolean isEmpty() {
        boolean isEmpty = true;
        for (int i = 0; i < this.integers.size() && isEmpty; i++) {
            if (!this.integers.get(i).isEmpty()) {
                isEmpty = false;
            }
        }
        return isEmpty;
    }

    /**
     * Returns the largest element in the array.TwoDimensionalArray
     *
     * @return
     */
    public Integer getLargestElement() {
        Integer largest = this.integers.get(0).get(0);
        for (ArrayList<Integer> row : this.integers) {
            for (Integer element : row) {
                if (element > largest) {
                    largest = element;
                }
            }
        }
        return largest;
    }

    /**
     * Returns the smallest element in the array.TwoDimensionalArray
     *
     * @return
     */
    public Integer getSmallestElement() {
        Integer smallest = this.integers.get(0).get(0);
        for (ArrayList<Integer> row : this.integers) {
            for (Integer element : row) {
                if (element < smallest) {
                    smallest = element;
                }
            }
        }
        return smallest;
    }

    /**
     * Returns the average of all elements in the array.TwoDimensionalArray
     *
     * @return
     */
    public double getAverage() {
        double sum = 0;
        double elementCounter = 0;
        for (ArrayList<Integer> row : this.integers) {
            for (Integer element : row) {
                sum += element;
                elementCounter++;
            }
        }
        return sum / elementCounter;
    }

    /**
     * Returns an ArrayList where each element is the sum of elements in that row in the array.TwoDimensionalArray
     *
     * @return
     */
    public ArrayList<Integer> getSumOfEachRow() {
        ArrayList sumOfEachRow = new ArrayList();
        for (ArrayList<Integer> row : this.integers) {
            int sum = 0;
            for (Integer element : row) {
                sum += element;
            }
            sumOfEachRow.add(sum);
        }
        return sumOfEachRow;
    }

    /**
     * Returns the largest index of the columns of the array.TwoDimensionalArray
     *
     * @return
     */
    private int getLargestColumn() {
        int largestColumn = 0;
        for (int i = 0; i < this.integers.size(); i++) {
            if (largestColumn < this.integers.get(i).size()) {
                largestColumn = this.integers.get(i).size();
            }
        }
        return largestColumn;
    }

    /**
     * Returns an ArrayList with number capacity
     * @param number
     * @return
     */
    private ArrayList<Integer> setArrayListCapacity(int number) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            arrayList.add(0);
        }
        return arrayList;
    }

    /**
     * Returns an ArrayList where each element is the sum of the elements in that column in the array.TwoDimensionalArray
     *
     * @return
     */
    public ArrayList<Integer> getSumOfEachColumn() {
        ArrayList<Integer> sumOfEachColumn = setArrayListCapacity(getLargestColumn());
        for (int row = 0; row < this.integers.size(); row++) {
            for (int column = 0; column < this.integers.get(row).size(); column++) {
                sumOfEachColumn.set(column, sumOfEachColumn.get(column) + this.integers.get(row).get(column));
            }
        }
        return sumOfEachColumn;
    }

    /**
     * Returns the index of the row of the array.TwoDimensionalArray with the largest sum of elements
     *
     * @return
     */
    public int getRowWithLargestSumOfElements() {
        ArrayList<Integer> getSumOfEachRow = getSumOfEachRow();
        Integer largest = getSumOfEachRow.get(0);
        for (Integer element : getSumOfEachRow) {
            if (element > largest) {
                largest = element;
            }
        }
        return getSumOfEachRow.indexOf(largest);
    }

    /**
     * Returns true if the array.TwoDimensionalArray is a square matrix
     *
     * @return
     */
    public boolean isSquareMatrix() {
        boolean isSquared = true;
        for (ArrayList<Integer> row : this.integers) {
            if (this.integers.size() != row.size()) {
                isSquared = false;
            }
        }
        return isSquared;
    }

    /**
     * Returns true if the array.TwoDimensionalArray is a symmetric matrix
     *
     * @return
     */
    public boolean isSymmetricMatrix() {
        boolean isSymmetricMatrix = true;
        if (!isSquareMatrix()) {
            isSymmetricMatrix = false;
        } else {
            for (int i = 0; i < this.integers.size() && isSymmetricMatrix; i++) {
                for (int j = 0; j < this.integers.get(i).size() && isSymmetricMatrix; j++) {
                    if (!this.integers.get(i).get(j).equals(this.integers.get(j).get(i))) {
                        isSymmetricMatrix = false;
                    }
                }
            }
        }
        return isSymmetricMatrix;
    }

    /**
     * Returns the main diagonal of the array.TwoDimensionalArray
     *
     * @return
     */
    private ArrayList<Integer> getMainDiagonal() {
        ArrayList<Integer> mainDiagonal = new ArrayList<>();
        for (int i = 0; i < this.integers.size(); i++) {
            mainDiagonal.add(this.integers.get(i).get(i));
        }
        return mainDiagonal;
    }

    /**
     * Returns the secondary diagonal of the array.TwoDimensionalArray
     *
     * @return
     */
    private ArrayList<Integer> getSecondaryDiagonal() {
        ArrayList<Integer> secondaryDiagonal = new ArrayList<Integer>(this.integers.size());
        for (int i = 0, j = this.integers.size() - 1; i < this.integers.size() && j >= 0; i++, j--) {
            secondaryDiagonal.add(this.integers.get(i).get(j));
        }
        return secondaryDiagonal;
    }

    /**
     * Returns the number of non-null elements in the main diagonal of the array.TwoDimensionalArray
     *
     * @return
     */
    public int getNonNullElementsInMainDiagonal() {
        int nonNullElements = 1;
        if (isSquareMatrix()) {
            nonNullElements = 0;
            ArrayList<Integer> mainDiagonal = getMainDiagonal();
            for (Integer element : mainDiagonal) {
                if (element != null) {
                    nonNullElements++;
                }
            }
        }
        return nonNullElements;
    }

    /**
     * Returns true if the main diagonal is equal to the secondary diagonal
     *
     * @return
     */
    public boolean isMainDiagonalEqualSecondaryDiagonal() {
        boolean isEqual = false;
        if (isSquareMatrix()) {
            ArrayList<Integer> mainDiagonal = getMainDiagonal();
            ArrayList<Integer> secondaryDiagonal = getSecondaryDiagonal();
            if (mainDiagonal.equals(secondaryDiagonal)) {
                isEqual = true;
            }
        }
        return isEqual;
    }

    /**
     * Returns the number of digits in a number
     *
     * @param number
     * @return
     */
    private int countDigits(int number) {
        int digitCounter = 0;
        while (number > 0) {
            digitCounter++;
            number /= 10;
        }
        return digitCounter;
    }

    /**
     * Returns the average of digits in all elements in the array.TwoDimensionalArray
     *
     * @return
     */
    private double getAverageDigitsofElements() {
        double sumDigits = 0;
        double elementCounter = 0;
        for (ArrayList<Integer> row : this.integers) {
            for (Integer element : row) {
                sumDigits += countDigits(element);
                elementCounter++;
            }
        }
        return sumDigits / elementCounter;
    }

    /**
     * Returns an ArrayList with all digits in the array.TwoDimensionalArray with above average digit count
     *
     * @return
     */
    public ArrayList<Integer> getElementsWithDigitsAboveAverage() {
        ArrayList<Integer> elementsWithDigitsAboveAverage = new ArrayList<>();
        double averageDigits = this.getAverageDigitsofElements();
        for (ArrayList<Integer> row : this.integers) {
            for (Integer element : row) {
                if (countDigits(element) > averageDigits) {
                    elementsWithDigitsAboveAverage.add(element);
                }
            }
        }
        return elementsWithDigitsAboveAverage;
    }

    /**
     * Returns true if numbers is even
     *
     * @param number
     * @return
     */
    private boolean isEven(Integer number) {
        return (number % 2 == 0);
    }

    /**
     * Returns the percentage of even digits in a number
     *
     * @param number
     * @return
     */
    private double getPercentageEvenDigits(int number) {
        double percentageEvenDigits;
        int digit;
        double digitCounter = countDigits(number);
        double evenDigits = 0;
        while (number > 0) {
            digit = number % 10;
            if (isEven(digit)) {
                evenDigits++;
            }
            number /= 10;
        }
        percentageEvenDigits = evenDigits / digitCounter;
        return percentageEvenDigits;
    }

    /**
     * Returns the percentage of even digits in elements in the array.TwoDimensionalArray
     *
     * @return
     */
    private double getAverageEvenDigitsPercentageOfElements() {
        double percentageEvenDigits = 0;
        double elementCounter = 0;
        for (ArrayList<Integer> row : this.integers) {
            for (Integer element : row) {
                percentageEvenDigits += getPercentageEvenDigits(element);
                elementCounter++;
            }
        }
        return percentageEvenDigits / elementCounter;
    }

    /**
     * Returns ArrayList with elements from the array.TwoDimensionalArray with above average percentage of even digits
     *
     * @return
     */
    public ArrayList<Integer> getElementsWithEvenDigitsPercentageAboveAverage() {
        ArrayList<Integer> elementsWithEvenDigitPercentageAboveAverage = new ArrayList<>();
        double averageEvenDigitsPercentage = this.getAverageEvenDigitsPercentageOfElements();
        for (ArrayList<Integer> row : this.integers) {
            for (Integer element : row) {
                if (getPercentageEvenDigits(element) > averageEvenDigitsPercentage) {
                    elementsWithEvenDigitPercentageAboveAverage.add(element);
                }
            }
        }
        return elementsWithEvenDigitPercentageAboveAverage;
    }

    /**
     * Inverts the order of elements in each row of the array.TwoDimensionalArray
     */
    public void invertElementsInRow() {
        Integer temp = null;
        for (ArrayList<Integer> row : this.integers) {
            for (int begin = 0, end = row.size() - 1; begin < end; begin++, end--) {
                temp = row.get(begin);
                row.set(begin, row.get(end));
                row.set(end, temp);
            }
        }
    }

    /**
     * Inverts the order of elements in each column of the array.TwoDimensionalArray
     */
    public void invertElementsInColumn() {
        Integer temp = null;
        for (int row = 0; row < this.integers.size()/2; row++) {
            for (int column = 0; column < this.integers.get(row).size(); column++) {
                temp = this.integers.get(row).get(column);
                this.integers.get(row).set(column, this.integers.get(this.integers.size() - 1 - row).get(column));
                this.integers.get(this.integers.size() - 1 - row).set(column, temp);
            }
        }
    }

    /**
     * Returns a 90 degrees clockwise rotated array.TwoDimensionalArray
     */
    public void rotateNinetyDegrees() {
        TwoDimensionalArray rotated = this;
        for (int row = 0; row < this.integers.size(); row++) {
            for (int column = 0; column < this.integers.size(); column++) {
                rotated.integers.get(row).set(column, this.integers.get(integers.size() - column - 1).get(row));
            }
        }
        this.integers = rotated.integers;
    }

    /**
     * Returns a 180 degrees rotated array.TwoDimensionalArray
     */
    public void rotateOneHundredEightyDegrees() {
        TwoDimensionalArray rotated = this;
        for (int row = 0; row < this.integers.size(); row++) {
            for (int column = 0; column < this.integers.size(); column++) {
                rotated.integers.get(row).set(column, this.integers.get(integers.size() - row - 1).get(integers.size() - column - 1));
            }
        }
        this.integers = rotated.integers;
    }

    /**
     * Returns a 90 degrees counter-clockwise rotated array.TwoDimensionalArray
     */
    public void rotateMinusNinetyDegrees() {
        TwoDimensionalArray rotated = this;
        for (int row = 0; row < this.integers.size(); row++) {
            for (int column = 0; column < this.integers.size(); column++) {
                rotated.integers.get(row).set(column, this.integers.get(column).get(integers.size() - row - 1));
            }
        }
        this.integers = rotated.integers;
    }

}