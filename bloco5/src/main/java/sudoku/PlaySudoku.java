package sudoku;

import java.util.Scanner;

public class PlaySudoku {

    /**
     * Play
     *
     * @param args
     */
    public static void main(String[] args) {

        SudokuGrid grid = new SudokuGrid(chooseDifficulty());
        do {
            grid.printSudoku();
            int number = readNumberBetweenZeroAndNine();
            int row = readRowBetweenZeroAndEight();
            int column = readColumnBetweenZeroAndEight();
            grid.writeNumber(number, row, column);
        } while (!grid.checkIfGridIsFull());

        grid.printSudoku();
        System.out.println("Parabéns, terminou o jogo");
    }

    /**
     * Método que lê e retorna a dificuldade escolhida pelo utilizador
     *
     * @return
     */
    private static int[][] chooseDifficulty() {
        Scanner ler = new Scanner(System.in);
        int chosenDifficulty = -1;
        int[][] difficulty = new int[0][];
        do {
            System.out.println("Esolha a dificuldade\n1 - fácil \n2 - médio \n3 - difícil");
            chosenDifficulty = ler.nextInt();
        } while (chosenDifficulty < 0 || chosenDifficulty > 4);
        switch (chosenDifficulty) {
            case 1:
                difficulty = easy;
                break;
            case 2:
                difficulty = medium;
                break;
            case 3:
                difficulty = hard;
                break;
        }
        return difficulty;
    }

    /**
     * Método que lê e retorna um número a jogar entre 0 e 9 inserido pelo utilizador
     *
     * @return número inserido
     */
    private static int readNumberBetweenZeroAndNine() {
        Scanner ler = new Scanner(System.in);
        int number = -1;

        do {
            System.out.println("Introduza o número entre 1 a 9 escrever (ou 0 para apagar um número)");
            number = ler.nextInt();
        } while (number < 0 || number > 9);

        return number;
    }

    /**
     * Método que lê e retorna a linha inserida pelo utilizador entre 0 e 8
     *
     * @return linha inserida
     */
    private static int readRowBetweenZeroAndEight() {
        Scanner ler = new Scanner(System.in);
        int row = -1;

        do {
            System.out.println("Introduza a linha entre 1 a 9");
            row = ler.nextInt() - 1;
        } while (row < 0 || row > 8);

        return row;
    }

    /**
     * Método que lê e retorna a coluna inserida pelo utilizador entre 0 e 8
     *
     * @return coluna inserida
     */
    private static int readColumnBetweenZeroAndEight() {
        Scanner ler = new Scanner(System.in);
        int column = -1;

        do {
            System.out.println("Introduza a coluna entre 1 a 9");
            column = ler.nextInt() - 1;
        } while (column < 0 || column > 8);

        return column;
    }

    static final int[][] easy = {
            {0, 2, 0, 0, 1, 7, 0, 3, 0},
            {8, 5, 1, 0, 9, 4, 2, 0, 0},
            {4, 7, 0, 2, 6, 0, 9, 0, 0},
            {3, 4, 0, 0, 5, 0, 0, 0, 7},
            {0, 0, 8, 0, 0, 2, 1, 0, 3},
            {0, 0, 0, 6, 0, 8, 5, 0, 0},
            {0, 0, 4, 0, 0, 9, 6, 1, 0},
            {6, 0, 0, 0, 0, 3, 7, 0, 9},
            {0, 1, 0, 0, 4, 6, 0, 5, 0}};

    static final int[][] medium = {
            {0, 8, 0, 9, 0, 0, 0, 5, 0},
            {4, 0, 0, 6, 5, 0, 9, 1, 3},
            {0, 0, 5, 4, 0, 0, 0, 0, 0},
            {1, 0, 2, 0, 6, 5, 0, 0, 0},
            {7, 0, 4, 0, 0, 0, 5, 0, 8},
            {0, 0, 0, 0, 0, 0, 0, 0, 2},
            {3, 0, 0, 0, 0, 1, 0, 0, 6},
            {8, 7, 0, 0, 0, 0, 0, 4, 0},
            {0, 5, 0, 0, 4, 0, 8, 0, 1}};

    static final int[][] hard = {
            {6, 0, 0, 0, 0, 0, 9, 0, 4},
            {0, 3, 0, 5, 0, 0, 0, 6, 0},
            {0, 0, 9, 7, 0, 0, 0, 0, 0},
            {0, 0, 3, 0, 0, 0, 1, 0, 0},
            {0, 0, 0, 3, 0, 4, 0, 2, 0},
            {0, 2, 0, 0, 0, 0, 5, 4, 0},
            {0, 9, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 9, 0, 1, 7, 0, 0},
            {0, 0, 8, 0, 7, 2, 3, 1, 0}};

    public static final String ANSI_RED = "\u001B[31m";


}