package sudoku;

public class SudokuCell {

    // attributes
    int number;
    int row;
    int column;

    // constructor
    public SudokuCell(int number, int row, int column) {
        if (isValidNumber(number) && isValidColumnOrRow(row) && isValidColumnOrRow(column)) {
            this.number = number;
            this.row = row;
            this.column = column;
        }
    }

    // methods

    /**
     * Returns true if row or column is between 0 and 8
     *
     * @param number
     * @return
     */
    private boolean isValidColumnOrRow(int number) {
        if (!(number >= 0 && number <= 8)) {
            throw new IllegalArgumentException("Rows and Columns must be between 1 and 9");
        }
        return true;
    }

    /**
     * Returns true if number is between 0 and 9
     *
     * @param number
     * @return
     */
    private boolean isValidNumber(int number) {
        if (!(number >= 0 && number <= 9)) {
            throw new IllegalArgumentException("Number must be between 0 and 9");
        }
        return true;
    }

    /**
     * Returns true if this number is equal to another cell's number
     *
     * @param number
     * @return
     */
    public boolean isNumberEqual(int number) {
        boolean isEqualNumber = false;
        if (this.number == number) {
            isEqualNumber = true;
        }
        return isEqualNumber;
    }

    /**
     * Set number value of this sudoku.SudokuCell to given number
     * @param number
     */
    public void setNumber(int number) {
        if (isValidNumber(number)){
            this.number = number;
        }
    }

    /**
     * Returns true if a given object is equal to this sudoku.SudokuCell
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        if (obj==this) {
            isEqual = true;
        } else if (obj instanceof SudokuCell) {
            SudokuCell otherCell = (SudokuCell) obj;
            if (this.number == otherCell.number && this.row == otherCell.row && this.column == otherCell.column) {
                isEqual = true;
            }
        }
        return isEqual;
    }
}
