package sudoku;

public class SudokuGrid {

    // attributes
    SudokuCell[][] grid = new SudokuCell[9][9];
    SudokuCell[][] baseGrid = new SudokuCell[9][9];

    // constructors
    public SudokuGrid(int[][] grid) {
        if (!isValidGrid(grid)) {
            throw new IllegalArgumentException("Grid must be 9x9 square");
        }
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[row].length; column++) {
                this.grid[row][column] = new SudokuCell(grid[row][column], row, column);
            }
        }
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[row].length; column++) {
                this.baseGrid[row][column] = new SudokuCell(grid[row][column], row, column);
            }
        }
    }

    // methods

    /**
     * Returns true if a 9x9 length grid is given
     *
     * @param grid
     * @return
     */
    private boolean isValidGrid(int[][] grid) {
        boolean isValidGrid = false;
        if (grid.length == 9) {
            isValidGrid = true;
            for (int i = 0; i < grid.length && isValidGrid; i++) {
                if (grid[i].length != 9) {
                    isValidGrid = false;
                }
            }
        }
        return isValidGrid;
    }

    /**
     * Returns true if a given object is equal to this sudoku.SudokuGrid
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        boolean isEqual = true;
        if (obj == this) {
            isEqual = true;
        } else if (obj instanceof SudokuGrid) {
            SudokuGrid otherGrid = (SudokuGrid) obj;
            for (int i = 0; i < this.grid.length && isEqual; i++) {
                for (int j = 0; j < this.grid.length && isEqual; j++) {
                    if (this.grid[i][j].number != otherGrid.grid[i][j].number) {
                        isEqual = false;
                    }
                }
            }
        } else {
            isEqual = false;
        }
        return isEqual;
    }

    /**
     * Returns true if successful in writing number
     *
     * @param number
     * @param row
     * @param column
     * @return
     */
    public boolean writeNumber(int number, int row, int column) {
        boolean success = false;
        if (this.baseGrid[row][column].isNumberEqual(0)) {
            if (number == 0) {
                this.grid[row][column].setNumber(number);
                success = true;
            }
            if (checkIfNumberIsValidInRow(number, row) && checkIfNumberIsValidInColumn(number, column) && checkIfNumberIsValidInGrid(number, row, column)) {
                this.grid[row][column].setNumber(number);
                success = true;
            }
            if (!success) {
                System.out.println("Não pode repetir um número na mesma linha, coluna ou quadrado 3x3");
            }
        } else {
            System.out.println("Não pode alterar esse número");
        }
        return success;
    }

    /**
     * Returns true if no other sudoku.SudokuCell in given row has given number
     *
     * @param number
     * @param row
     * @return
     */
    private boolean checkIfNumberIsValidInRow(int number, int row) {
        boolean isValid = true;
        for (int column = 0; column < this.grid.length && isValid; column++) {
            if (this.grid[row][column].isNumberEqual(number)) {
                isValid = false;
            }
        }
        return isValid;
    }

    /**
     * Returns true if no other sudoku.SudokuCell in given column has given number
     *
     * @param number
     * @param column
     * @return
     */
    private boolean checkIfNumberIsValidInColumn(int number, int column) {
        boolean isValid = true;
        for (int row = 0; row < this.grid.length && isValid; row++) {
            if (this.grid[row][column].isNumberEqual(number)) {
                isValid = false;
            }
        }
        return isValid;
    }

    /**
     * Returns true if no other sudoku.SudokuCell in the smaller 3x3 grid has given number
     *
     * @param number
     * @param row
     * @param column
     * @return
     */
    private boolean checkIfNumberIsValidInGrid(int number, int row, int column) {
        boolean isValid = true;
        int rowMinimum = (row / 3) * 3;
        int columnMinimum = (column / 3) * 3;
        for (int i = rowMinimum ; i <= rowMinimum + 3 && isValid; i++) {
            for (int j = columnMinimum; j <= columnMinimum + 3 && isValid; j++) {
                if (this.grid[i][j].isNumberEqual(number)) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }

    /**
     * Returns true if the grid is completed
     *
     * @return
     */
    public boolean checkIfGridIsFull() {
        boolean isFull = true;
        for (int row = 0; row < this.grid.length && isFull; row++) {
            for (int column = 0; column < this.grid[row].length && isFull; column++) {
                if (this.grid[row][column].isNumberEqual(0)) {
                    isFull = false;
                }
            }
        }
        return isFull;
    }

    /**
     * Prints the attribute grid
     */
    public void printSudoku() {
        System.out.println(ANSI_RED + "      1  2  3   4  5  6   7  8  9" + ANSI_RESET );
        System.out.println("     -----------------------------");
        for (int i = 0; i < this.grid.length; i++) {
            System.out.print(" " + ANSI_RED + (i + 1) + ANSI_RESET + " | ");
            for (int j = 0; j < this.grid[i].length; j++) {
                if (this.grid[i][j].isNumberEqual(0)) {
                    System.out.print("   ");
                } else {
                    System.out.print(" " + this.grid[i][j].number + " ");
                }
                if (j == 2 || j == 5) {
                    System.out.print("|");
                }
            }
            System.out.println();
            if (i == 2 || i == 5) {
                System.out.println("   | -----------------------------");
            }
        }
    }

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";
}